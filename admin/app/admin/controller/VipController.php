<?php
/**
 * Created by PhpStorm.
 * User: weipeng
 * Date: 2018/8/17
 * Time: 01:31
 */

namespace app\admin\controller;


use cmf\controller\AdminBaseController;
use think\Db;

class VipController extends AdminBaseController
{
    public function index(){
        $list = db('vip_rule') -> select();

        $this->assign('list',$list);
        return $this->fetch();
    }

    public function add(){
        $id = input('param.id');
        if ($id) {
            $name = Db::name("vip_rule")->where("id=$id")->find();
            $this->assign('data', $name);
        }
        return $this->fetch();

    }

    public function add_post(){

        $param = $this->request->param();
        $id = $param['id'];
        $data = $param;
        $data['create_time'] = time();
        if($id){
            $result = Db::name("vip_rule")->where("id=$id")->update($data);
        }else{
            $result = Db::name("vip_rule")->insert($data);
        }
        if($result){
            $this->success("保存成功",url('vip/index'));
        }else{
            $this->error("保存失败");
        }
    }

    public function del(){

        $id = input('param.id');
        $result = db('vip_rule') -> delete($id);

        return $result ? '1' : '0';
        exit;
    }

}