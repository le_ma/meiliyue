<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:128:"/Users/weipeng/Desktop/work/buguniao/bogo_video_line_service_v_2_5/mapi/public/../application/api/view/invitation_api/index.html";i:1537236724;}*/ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>邀请奖励</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/mapi/public/static/css/invitation.css" rel="stylesheet">
    <link href="/mapi/public/static/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="col-xs-12 invitation">
  <div class="col-xs-12 invitation-type" >
      <div class="col-xs-12 pad invitation-type-hei">
          <div class="col-xs-3 pad invitation-type-name"><span>1</span> 重中奖</div>
          <div class="col-xs-9 invitation-type-coin"><span>男性好友<?php echo $data['invite_reg_reward_man']; ?>女性好友<?php echo $data['invite_reg_reward_female']; ?></span></div>
      </div>
      <div class="col-xs-12 pad invitation-type-hei">
          <div class="col-xs-3 pad invitation-type-name"><span>2</span> 重中奖</div>
          <div class="col-xs-9 invitation-type-coin">好友每次 <span>消费的<?php echo $data['invite_income_ratio']; ?>%</span></div>
      </div>
      <div class="col-xs-12 pad invitation-type-hei">
          <div class="col-xs-3 pad invitation-type-name"><span>3</span> 重中奖</div>
          <div class="col-xs-9 invitation-type-coin">好友每次 <span>收入的<?php echo $data['invite_income_ratio_female']; ?>%</span></div>
      </div>
      <div class="col-xs-12 invitation-type-rules">
          注：邀请奖励可提现，<span class="href" data-href="/mapi/public/index.php/api/invitation_api/details/uid/<?php echo $data['uid']; ?>">规则详情</span>
      </div>
  </div>
   <div class="col-xs-12 pad invitation-cumulative">
      <div class="invitation-cumulative-coin">
          <div class="col-xs-12 pad invitation-cumulative-coin-name"></div>
          <div class="invitation-cumulative-coin-names href" data-href="/mapi/public/index.php/api/invitation_api/rewards/uid/<?php echo $data['uid']; ?>"><span>累计奖励</span>(金额)</div>
          <div class="col-xs-12 invitation-cumulative-coin-name-sum">
             <?php echo (isset($data['reward_total']) && ($data['reward_total'] !== '')?$data['reward_total']:'0'); ?>
          </div>
      </div>
       <div class="invitation-cumulative-coin">
           <div class="col-xs-12 pad invitation-cumulative-coin-name"></div>
           <div class="invitation-cumulative-coin-names"><span>累计邀请</span>(人)</div>
           <div class="col-xs-12 invitation-cumulative-coin-name-sum">
                <?php echo (isset($data['sum']) && ($data['sum'] !== '')?$data['sum']:'0'); ?>
           </div>
       </div>
   </div>
    <div class="col-xs-8 col-xs-offset-2 invitation-btn-div" data-code="<?php echo $data['invite_url']; ?>">立即邀请好友</div>
    <div class="col-xs-12 invitation-my-inviter href" data-href="/mapi/public/index.php/api/invitation_api/inviter/uid/<?php echo $data['uid']; ?>">我邀请的人</div>
</div>
<script src="/mapi/public/static/js/jquery-1.10.2.min.js"></script>

<script>
    $(".href").click(function(){
        var url=$(this).attr("data-href");
        window.location.href=url;
    })
    //分享
    $(".invitation-btn-div").click(function(){
        //var url = $(this).attr("data-code");
        window.location.href = "cuckoo://invite_share"
    })

</script>
</body>
</html>