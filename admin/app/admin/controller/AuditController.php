<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/2/8 0008
 * Time: 上午 9:32
 */
namespace app\admin\controller;
use cmf\controller\AdminBaseController;
use QcloudApi;
use think\Db;


class AuditController extends AdminBaseController
{

    //用户封面图审核
    public function user_thumb(){

        $where   = [];
        $request = input('request.');

        if (!empty($request['uid'])) {
            $where['uid'] = intval($request['uid']);
        }

        if (isset($request['status']) && intval($request['status']) != -1) {
            $where['status'] = intval($request['status']);
        }else{
            $request['status'] = '-1';
        }

        $usersQuery = Db::name('user_img');

        $list = $usersQuery->where($where)->order("addtime DESC")->paginate(20);
        $lists=$list->toArray();
        // print_r($lists);exit;
        foreach($lists['data'] as &$v){

            $find=Db::name("user")->where("id=".$v['uid'])->find();
            if($find){
                $v['user_nickname'] = $find['user_nickname'];
            }else{
                $v['user_nickname'] = '';
            }
        }
        // 获取分页显示
        $page = $list->render();
        $this->assign('list', $lists['data']);
        $this->assign('request', $request);
        $this->assign('page', $page);
        // 渲染模板输出
        return $this->fetch();
    }

    //审核用户上传的封面
    public function user_thumb_upd(){

        $id = input('param.id');
        $uid = input('param.uid');
        $type = input('param.type');

        $result = Db::name("user_img") -> where("id=$id and uid=$uid") -> update(array("status"=>$type));

        if($result){
            if($type == 1){
                push_msg_user(9,$uid,1);
            }else{
                push_msg_user(10,$uid,1);
            }
            $this->success('操作成功！');
        }else{
            $this->success("操作失败");
        }

    }

    //小视频审核列表
    public function index()
    {
        if(request()->post()){
            session('audit',request()->post());
        }
        if(!request()->get('page') and !request()->post()){
            session('audit',null);
        }
        $where=session('audit.type') ? "a.type=".session('audit.type') : "a.type=0";
        $where.=session('audit.uid') ? " and a.uid=".session('audit.uid') : '';
        $where.=session('audit.status') ? " and a.status=".session('audit.status') : "";

        $user = Db::name("user_video")
            ->alias("a")
            ->join("user b","b.id=a.uid")
            ->field("a.*,b.user_nickname,b.avatar")
            ->where($where)
            ->order('addtime desc')
            ->paginate(20);
        $lists = $user->toArray();

        $config = load_cache('config');
        $key = $config['tencent_video_sign_key'];

        foreach ($lists['data'] as &$v){
            //获取视频地址
            $video_url = $v['video_url'];

            $parse_url_arr = parse_url($video_url);
            $url_dir = substr($parse_url_arr['path'],0,strrpos($parse_url_arr['path'],'/') + 1);
            $t = dechex(time() +  60 * 60 * 24);
            $us = rand_str();
            $sign = md5($key.$url_dir.$t.$us);

            $sign_video_url = $video_url . '?t=' . $t . '&us=' . $us . '&sign=' . $sign;
            $v['video_url'] = $sign_video_url;
        }

        $this->assign('user',$lists['data']);
        $this->assign('request', session('audit'));
        $this->assign('page', $user->render());
        return $this->fetch();
    }

    //小视频审核验证类型
    public function upd(){

        $id = input('param.id', 0, 'intval');
        $uid = input('param.uid', 0, 'intval');
        $type = input('param.type', 0, 'intval');
        //$sta = $type =='1' ? "审核通过" : "审核不通过，请重新上传";

        $name=Db::name("user_video")->where("id=$id and uid=$uid")->update(array("type"=>$type));
        $usera['id']=$uid;
        if($name){
            //$message=Db::name("user_message")->where("id=3")->find();
            if($type == 1){
                push_msg_user(9,$uid,1);
            }else{
                push_msg_user(10,$uid,1);
            }
            $this->success('操作成功！');
        }else{
            $this->success("操作失败");
        }
    }

    //推荐视频
    public function recommend(){
        $id = input('param.id', 0, 'intval');
        $is_recommend = input('param.is_recommend', 0, 'intval');

        db('user_video') -> where(['id' => $id]) -> setField('is_recommend',$is_recommend);

        $this->success('操作成功');
    }

    //私照审核列表
    public function photos()
    {
        if(request()->post()){
            session('audit1',request()->post());
        }
        if(!request()->get('page') and !request()->post()){
            session('audit1',null);
        }

        $where=session('audit1.status') ? "a.status=".session('audit1.status') : "a.status=0";
        $where.=session('audit1.uid') ? " and a.uid=".session('audit1.uid') : '';


        $user = Db::name("user_pictures")
            ->alias("a")
            ->join("user b","b.id=a.uid")
            ->field("a.*,b.user_nickname,b.avatar")
            ->where($where)
            ->order('addtime desc')
            ->paginate(20);

        $lists=$user->toArray();

        $this->assign('user',$lists['data']);
        $this->assign('request', session('identity'));
        $this->assign('page', $user->render());
        return $this->fetch();
    }

    //私照审核验证类型
    public function photos_upd(){

        $id = input('param.id', 0, 'intval');
        $uid = input('param.uid', 0, 'intval');
        $type = input('param.type', 0, 'intval');

        //$sta = $type =='1' ? "审核通过" : "审核不通过，请重新上传";

        $user = Db::name("user_pictures")->where("id=$id and uid=$uid")->update(array("status"=>$type));
        //$usera['id']=$uid;
        if($user){
            //$message = Db::name("user_message")->where("id=4")->find();
            if($type == 1){
                push_msg_user(7,$uid,1);
            }else{
                push_msg_user(8,$uid,1);
            }
            $this->success('操作成功！');
        }else{
            $this->success("操作失败");
        }
    }

    //删除视频
    public function del_video(){

        $id = input('param.id');
        $video = db('user_video') -> find($id);
        if(!$video){
           $this->error('视频记录不存在');
           exit;
        }

        require_once DOCUMENT_ROOT .'/system/qcloudapi_sdk/src/QcloudApi/QcloudApi.php';

        $puc_config = load_cache('config');
        $config = array('SecretId'       => $puc_config['tencent_api_secret_id'],
            'SecretKey'      => $puc_config['tencent_api_secret_key'],
            'RequestMethod'  => 'GET',
            'DefaultRegion'  => 'gz');

        $cvm = QcloudApi::load(QcloudApi::MODULE_VOD, $config);

        $package = array('fileId' => $video['video_id'], 'priority' => 0);

        $a = $cvm->DeleteVodFile($package);
        // $a = $cvm->generateUrl('DescribeInstances', $package);

        if ($a === false) {
            $error = $cvm->getError();
            //echo "Error code:" . $error->getCode() . ".\n";
            //echo "message:" . $error->getMessage() . ".\n";
            //echo "ext:" . var_export($error->getExt(), true) . ".\n";
            $this->error('Error message' . $error->getMessage());
            exit;
        }else{

            //删除视频
            db('user_video') -> delete($id);
        }
        $this->success('操作成功');

    }
}
