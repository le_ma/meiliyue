<?php
/**
 * Created by PhpStorm.
 * User: weipeng
 * Date: 2018/2/22
 * Time: 23:33
 */

namespace app\api\controller;


use think\Db;

class UserApi extends Base
{

    //获取认证信息和评价信息
    public function get_user_page_user_info()
    {

        $result = array('code' => 1, 'msg' => '');

        $to_user_id = intval(input('param.to_user_id'));
        $uid = intval(input('param.uid'));
        $token = trim(input(('param.token')));
        $page = intval(input(('param.page')));

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token);

        if (!$user_info) {
            $result['code'] = 0;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        if ($page == 1) {
            //查询认证资料
            $auth_info = db('auth_form_record')->field('height,weight,constellation,city,image_label,introduce,sign,status')->where('user_id', '=', $to_user_id)->find();

            $result['is_auth'] = $auth_info ? $auth_info['status'] : 0;

            $result['auth_info'] = [];
            if ($auth_info) {

                $auth_info['evaluate_list'] = [];

                if (!empty($auth_info['image_label'])) {
                    $self_label_array = explode('-', $auth_info['image_label']);
                    foreach ($self_label_array as $k => $v2) {
                        if (empty($v2)) {
                            unset($self_label_array[$k]);
                        }
                    }
                    $auth_info['evaluate_list'] = $self_label_array;
                }

                $result['auth_info'] = $auth_info;
            }
        }

        //获取评价列表
        $result['evaluate_list'] = db('user_evaluate_record')->alias('e')
            ->join('user u', 'e.user_id=u.id')
            ->field('u.user_nickname,u.avatar,e.label_name')
            ->where('e.to_user_id', '=', $to_user_id)
            ->order('e.create_time desc')
            ->page($page)
            ->select();

        foreach ($result['evaluate_list'] as &$v) {

            $v['label_list'] = [];
            if (!empty($v['label_name'])) {
                $label_array = explode('-', $v['label_name']);
                foreach ($label_array as $k => $v2) {
                    if (empty($v2)) {
                        unset($label_array[$k]);
                    }
                }
                $v['label_list'] = $label_array;
            }
        }

        return_json_encode($result);
    }


    //获取新用户主页信息
    public function get_user_page_info()
    {

        $result = array('code' => 1, 'msg' => '', 'data' => array());

        $to_user_id = intval(input('param.to_user_id'));
        $uid = intval(input('param.uid'));
        $token = trim(input(('param.token')));

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token);

        if (!$user_info) {
            $result['code'] = 0;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        $user = get_user_base_info($to_user_id, ['address', 'custom_video_charging_coin']);
        $level = get_level($to_user_id);

        $data = array(
            'id' => $to_user_id,
            'sex' => $user['sex'],
            'user_nickname' => $user['user_nickname'],
            'avatar' => $user['avatar'],
            'address' => $user['address'],
            'user_status' => get_user_auth_status($to_user_id),
            'level' => $level,
        );

        $data['attention'] = 1;
        if ($to_user_id != $uid) {
            $data['attention'] = get_attention($uid, $to_user_id);         //获取是否关注
        }

        //是否拉黑
        $data['is_black'] = get_is_black($uid, $to_user_id);

        $config = load_cache('config');

        $gift_list = db('user_gift_log')
            ->alias('l')
            ->join('gift g', 'l.gift_id=g.id')
            ->field('g.*')
            ->where('l.to_user_id', '=', $to_user_id)
            ->group('gift_id')
            ->select();

        //获取主播私照
        $private_photo_list = db('user_pictures')->where("uid=$to_user_id and status=1")->field("img,id")->limit(0, 15)->select();

        //处理图片模糊状态
        foreach ($private_photo_list as &$v) {
            //获取查询私照是否支付观看过
            $buy_record = db("user_photo_buy")->where("p_id=" . $v['id'] . " and user_id=$uid")->find();
            if (!$buy_record) {
                $v['img'] = $v['img'] . "?imageMogr2/auto-orient/blur/40x50";    //私照加密
                $v['watch'] = 1;
            } else {
                $v['watch'] = 0;
            }
        }

        if ($to_user_id == $uid) {
            $gift_count = db('user_gift_log')->where('user_id', '=', $to_user_id)->sum('gift_count');
            //统计收到的礼物
            $data['gift_count'] = $gift_count;
        } else {

            $gift_count = db('user_gift_log')->where('to_user_id', '=', $to_user_id)->sum('gift_count');
            //统计收到的礼物
            $data['gift_count'] = $gift_count;
        }
        //统计收到的礼物
        $data['gift'] = $gift_list;
        //统计主播私照
        $data['pictures_count'] = count($private_photo_list);
        //统计主播私照
        $data['pictures'] = $private_photo_list;
        //是否在线0不在1在
        $data['online'] = is_online($to_user_id, $config['heartbeat_interval']);
        $data['is_online'] = $data['online'];

        $attention_fans_count = db('user_attention')->where("attention_uid=$to_user_id")->count();
        $attention_count = db('user_attention')->where("uid=$to_user_id")->count();
        //通话时长
        $call_time = db('video_call_record_log')
            ->where('user_id', '=', $to_user_id)
            ->whereOr('call_be_user_id', '=', $to_user_id)
            ->sum('call_time');

        if ($call_time) {
            $call_time = secToTime(abs($call_time));
        } else {
            $call_time = '0';
        }

        //好评比
        $evaluation = db('video_call_record_log')->where('is_fabulous', '=', 1)->where('anchor_id', '=', $to_user_id)->count();   //获取评价总数

        //主页轮播图
        $user_image = db('user_img')->where("uid=$to_user_id")->where("status=1")->field("id,img")->order("addtime desc")->limit(6)->select();

        //点赞总数
        $fabulous_count = db('user_fabulous_record')->where('to_user_id')->count();


        $data['video_deduction'] = $config['video_deduction'];
        if (defined('OPEN_CUSTOM_VIDEO_CHARGE_COIN') && OPEN_CUSTOM_VIDEO_CHARGE_COIN == 1) {
            //判断用户等级是否符合规定
            if ($level >= $config['custom_video_money_level'] && $user['custom_video_charging_coin'] != 0) {
                $data['video_deduction'] = $user['custom_video_charging_coin'];
            }
        }

        $data['attention_fans'] = $attention_fans_count;    //获取关注人数
        $data['attention_all'] = $attention_count;      //获取粉丝人数
        $data['call'] = $call_time;                          //通话总时长
        $data['evaluation'] = $evaluation;             //好评百分比
        $data['img'] = $user_image;                      //主播轮播图
        $data['give_like'] = $fabulous_count;       //获取点赞数

        $result['data'] = $data;
        return_json_encode($result);
    }

    //提交认证信息
    public function request_submit_auth_info()
    {
        $result = array('code' => 1, 'msg' => '', 'data' => array());

        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token);

        if (!$user_info) {
            $result['code'] = 10001;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        $is_auth = db('auth_form_record')->where('user_id', '=', $uid)->find();
        if ($is_auth) {
            $result['code'] = 0;
            $result['msg'] = '已经提交过认证信息，请勿重复提交!';
            return_json_encode($result);
        }

        $nickname = trim(input('param.nickname'));
        $phone = trim((input(('param.phone'))));
        $height = intval((input(('param.height'))));
        $weight = trim((input(('param.weight'))));
        $constellation = trim((input(('param.constellation'))));
        $city = trim((input(('param.city'))));
        $introduce = trim((input(('param.introduce'))));
        $image_label = trim((input(('param.image_label'))));
        $sign = trim((input(('param.self_label'))));
        $auth_id_card_img_url1 = trim((input(('param.auth_id_card_img_url1'))));
        $auth_id_card_img_url2 = trim((input(('param.auth_id_card_img_url2'))));

        if (empty($nickname)) {
            $result['code'] = 0;
            $result['msg'] = '请输入昵称';
            return_json_encode($result);
        }

        if (strlen($phone) != 11) {
            $result['code'] = 0;
            $result['msg'] = '请输入手机号码';
            return_json_encode($result);
        }

        if ($height == 0) {
            $result['code'] = 0;
            $result['msg'] = '请选择身高';
            return_json_encode($result);
        }

        if ($weight == 0) {
            $result['code'] = 0;
            $result['msg'] = '请选择体重';
            return_json_encode($result);
        }

        if (empty($constellation)) {
            $result['code'] = 0;
            $result['msg'] = '请选择星座';
            return_json_encode($result);
        }

        if (empty($city)) {
            $result['code'] = 0;
            $result['msg'] = '请选择所在城市';
            return_json_encode($result);
        }

        if (empty($image_label)) {
            $result['code'] = 0;
            $result['msg'] = '请选择形象标签';
            return_json_encode($result);
        }

        if (empty($introduce)) {
            $result['code'] = 0;
            $result['msg'] = '请填写自我介绍';
            return_json_encode($result);
        }

        if (empty($sign)) {
            $result['code'] = 0;
            $result['msg'] = '请输入个性签名';
            return_json_encode($result);
        }

        if (empty($auth_id_card_img_url1)) {
            $result['code'] = 0;
            $result['msg'] = '请上传身份证正面';
            return_json_encode($result);
        }

        if (empty($auth_id_card_img_url2)) {
            $result['code'] = 0;
            $result['msg'] = '请上传身份证反面';
            return_json_encode($result);
        }

        $insert_data = [
            'user_nickname' => $nickname,
            'phone' => $phone,
            'height' => $height,
            'weight' => $weight,
            'constellation' => $constellation,
            'city' => $city,
            'image_label' => $image_label,
            'introduce' => $introduce,
            'sign' => $sign,
            'user_id' => $uid,
            'status' => 0,
            'auth_id_card_img_url1' => $auth_id_card_img_url1,
            'auth_id_card_img_url2' => $auth_id_card_img_url2,
            'create_time' => NOW_TIME,
        ];

        $res = db('auth_form_record')->insert($insert_data);
        if (!$res) {
            $result['code'] = 0;
            $result['msg'] = '提交失败，请稍后重试！';
        }

        return_json_encode($result);
    }

    //评价
    public function request_submit_evaluate()
    {

        $result = array('code' => 1, 'msg' => '', 'data' => array());

        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));
        $to_user_id = intval(input('param.to_user_id'));
        $label_str = trim(input('param.label_str'));
        $channel_id = trim(input('param.channel_id'));

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token);

        if (!$user_info) {
            $result['code'] = 10001;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        $data = ['user_id' => $uid, 'to_user_id' => $to_user_id, 'label_name' => $label_str, 'create_time' => NOW_TIME, 'channel_id' => $channel_id];

        db('user_evaluate_record')->insert($data);

        return_json_encode($result);
    }


    /*
     * 更新用户城市
     * */
    public function refresh_city()
    {
        $result = array('code' => 1, 'msg' => '');

        $uid = intval(input('param.uid'));
        $city = trim(input('param.city'));

        db('user')->where('id', '=', $uid)->setField('address', $city);
        return_json_encode($result);
    }


    /**
     * 会话列表根据id返回用户信息
     * */
    public function get_conversation_user_info()
    {

        $result = array('code' => 1, 'msg' => '');

        $ids = input("param.ids");

        $id_array = explode(',', $ids);
        if (count($id_array) > 0) {
            $list = db('user')
                ->whereIn('id', $id_array)
                ->field('id,avatar,user_nickname')
                ->select();

            $result['list'] = $list;
        }

        return_json_encode($result);
    }

    //对用户点赞
    public function fabulous()
    {

        $result = array('code' => 1, 'msg' => '', 'data' => array());
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));
        $to_user_id = intval(input('param.to_user_id'));

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token);

        if (!$user_info) {
            $result['code'] = 10001;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        $record = db('user_fabulous_record')
            ->where('user_id', '=', $uid)
            ->where('to_user_id', '=', $to_user_id)
            ->find();

        if (!$record) {
            $data = [
                'user_id' => $uid,
                'to_user_id' => $to_user_id,
                'create_time' => time(),
            ];
            db('user_fabulous_record')->insert($data);
        }

        return_json_encode($result);
    }


    //获取用户中心数据
    public function get_user_center_info()
    {

        $result = array('code' => 1, 'msg' => '', 'data' => array());
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token, ['is_open_do_not_disturb']);

        if (!$user_info) {
            $result['code'] = 10001;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        $level = get_level($user_info['id']);
        $data = array(
            'sex' => $user_info['sex'],
            'user_nickname' => $user_info['user_nickname'],
            'avatar' => $user_info['avatar'],
            'coin' => $user_info['coin'],
            'user_status' => $user_info['user_status'],             //2未验证1验证
            'level' => $level,
            'is_open_do_not_disturb' => $user_info['is_open_do_not_disturb'],
        );

        $config = load_cache('config');
        //获取认证状态
        $data['user_auth_status'] = get_user_auth_status($uid);

        $data['split'] = ($config['invite_income_ratio'] * 100) . '%';                    //获取分成比例

        $fans_count = Db::name('user_attention')->where("attention_uid=$uid")->count();
        $follow_count = Db::name('user_attention')->where("uid=$uid")->count();
        $data['attention_fans'] = $fans_count;    //获取关注人数
        $data['attention_all'] = $follow_count;    //获取粉丝人数

        //充值规则
        $charging_rule = Db::name("user_charge_rule")->field("id,money,coin")->order("orderno asc")->limit(3)->select();

        $data['pay_coin'] = $charging_rule;//获取购买聊币类型
        //  var_dump($data['coin']);exit;
        $result['data'] = $data;
        return_json_encode($result);
    }


    //获取粉丝列表
    public function get_fans_list()
    {

        $result = array('code' => 1, 'msg' => '', 'data' => array());
        $uid = intval(input('param.uid'));
        $page = intval(input('param.page'));

        $page = $page * 20;

        //统计用户粉丝总数
        $result['count'] = Db::name('user_attention')->where("attention_uid=$uid")->count();
        $attention = Db::name('user_attention')->alias("a")
            ->join("user u", "a.uid=u.id")
            ->where("a.attention_uid=$uid")
            ->field("u.id,u.avatar,u.user_nickname,u.sex")
            ->order("addtime desc")
            ->limit($page, 20)
            ->select();

        foreach ($attention as &$v) {

            $level = get_level($v['id']);
            $v['level'] = $level;
            $v['focus'] = 0;
            $focus = db('user_attention')->where("uid=$uid and attention_uid=" . $v['id'])->find();
            if ($focus) {
                $v['focus'] = 1;
            }
        }
        $result['data'] = $attention;
        return_json_encode($result);
    }


    //获取关注用户列表
    public function get_follow_list()
    {

        $result = array('code' => 1, 'msg' => '用户关注', 'data' => array());
        $uid = intval(input('param.uid'));

        $page = intval(input('param.page'));
        if (empty($page)) {
            $page = 0;
        } else {
            $page = $page * 10;
        }

        //统计用户关注
        $result['count'] = db('user_attention')->where("uid=$uid")->count();
        $attention = db('user_attention')->alias("a")
            ->join("user u", "a.attention_uid=u.id")
            ->where("a.uid=$uid")
            ->field("u.id,u.avatar,u.user_nickname,u.sex")
            ->order("addtime desc")
            ->limit($page, 20)
            ->select();

        foreach ($attention as &$v) {

            $level = get_level($v['id']);
            $v['level'] = $level;
            $v['focus'] = 0;
            $focus = db('user_attention')->where("uid=$uid and attention_uid=" . $v['id'])->find();
            if ($focus) {
                $v['focus'] = 1;
            }
        }

        $result['data'] = $attention;
        return_json_encode($result);
    }

    //显示修改的用户信息
    public function edit_user_info()
    {
        $result = array('code' => 1, 'msg' => '获取用户信息', 'data' => array());
        $uid = input('param.uid');
        $token = input('param.token');
        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token, ['last_change_name_time']);

        if (!$user_info) {
            $result['code'] = 10001;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }
        $data = array(
            'sex' => $user_info['sex'],
            'user_nickname' => $user_info['user_nickname'],
            'avatar' => $user_info['avatar'],
            'is_change_name' => 0,
        );

        //是否可以修改昵称
        if ((NOW_TIME - $user_info['last_change_name_time']) > (30 * 24 * 60 * 60)) {
            $data['is_change_name'] = 1;
        }
        $data['img'] = db('user_img')->where('uid', '=', $uid)->select();   //获取轮播图
        $result['data'] = $data;
        return_json_encode($result);
    }


    //修改用户信息
    public function update_user_info()
    {

        $result = array('code' => 0, 'msg' => '', 'data' => array());
        $id = intval(input('param.uid'));
        $token = trim(input('param.token'));
        $del = input("param.del");
        $sex = intval(input("param.sex"));
        $user_nickname = trim(input('param.user_nickname'));

        $delete_ids = explode('|', $del);
        if (count($delete_ids) > 0) {

            $delete_img_list = Db::name('user_img')->where('uid', '=', $id)->where('id', 'in', $delete_ids)->find();

            if ($delete_img_list) {
                $delete_img_path_list = [];
                foreach ($delete_img_list as $k => $v) {
                    $file_name = parse_url($v['img'])['path'];
                    $delete_img_path_list[] = substr($file_name, 1, strlen($file_name));

                }
                $oss_delete_res = oss_del_list($delete_img_path_list);
                if ($oss_delete_res) {
                    //删除轮播图
                    Db::name('user_img')->where('uid', '=', $id)->where('id', 'in', $delete_ids)->delete();
                }
            }
        }

        //修改昵称
        if (!empty($user_nickname)) {
            $all_name = Db::name('user')->where("user_nickname='$user_nickname' and id!=$id")->find();
            if ($all_name) {
                $result['code'] = 0;
                $result['msg'] = "用户名重复，请重新输入用户名";
                return_json_encode($result);
            }
            $data['user_nickname'] = $user_nickname;
        }

        $data['sex'] = $sex;

        //上传头像
        $avatar = request()->file('avatar');   //获取头像
        if ($avatar) {
            $upload_one = oss_upload($avatar);      //单图片上传
            $data['avatar'] = $upload_one;
        }

        $new_image = array();
        for ($i = 0; $i < 6; $i++) {
            $img = request()->file('img' . $i);
            if ($img) {
                $new_image[$i] = $img;
            }
        }

        if ($new_image) {
            $all_img = Db::name('user_img')->where("uid=$id")->count();
            $all_type = count($new_image) + $all_img;
            if ($all_type > 6) {
                //$log = array('code' => 0, 'msg' => "多图片添加失败,请删除后在添加", 'error' => $all_img);
                //save_log($log);
                $result['code'] = 0;
                $result['msg'] = "多图片添加失败,请删除后在添加";
                return_json_encode($result);
            } else {
                foreach ($new_image as $v) {
                    $uploads = oss_upload($v);      //单图片上传
                    $upload_all['img'][]['img'] = $uploads;
                }
            }
        }

        //更新修改信息
        db('user')->where("id=$id and token='$token'")->update($data);
        //echo db('user') -> getLastSql();exit;

        if ($new_image) {
            $now_time = time();
            foreach ($upload_all['img'] as &$v) {
                $v['uid'] = $id;
                $v['addtime'] = $now_time;
                $data['img'][]['img'] = $v['img'];
            }
            $all_img = Db::name('user_img')->insertAll($upload_all['img']);  //添加轮播图
            if (!$all_img) {
                $result['code'] = "0";
                $result['msg'] = "用户信息保存失败";
                return_json_encode($result);
            }
        }

        require_once DOCUMENT_ROOT . '/system/im_common.php';
        update_im_user_info($id);

        $result['code'] = 1;
        $result['msg'] = "修改成功";
        $result['data'] = $data;
        return_json_encode($result);

    }

    //删除形象图片
    public function del_image()
    {

        $result = array('code' => 0, 'msg' => '', 'data' => array());
        $img_id = intval(input('param.id'));
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token);

        if (!$user_info) {
            $result['code'] = 10001;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        $ser = db('user_img')->where("uid=$uid and id=$img_id")->find();
        $file_name = parse_url($ser['img'])['path'];
        $file_name = substr($file_name, 1, strlen($file_name));
        //var_dump($file_name);exit;
        $set = oss_del_file($file_name);

        if ($set) {
            $ser = Db::name('user_img')->where("uid=$uid and id=$img_id")->delete();    //删除轮播图
            if ($ser) {
                $result['code'] = 1;
            }
        }

        return_json_encode($result);
    }

    //视频认证
    public function video_auth()
    {

        $result = array('code' => 1, 'msg' => '', 'data' => array());
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));
        $video_id = intval(input('param.video_id'));
        $video_url = trim(input('param.video_url'));
        $cover_url = trim(input('param.cover_url'));

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token);

        if (!$user_info) {
            $result['code'] = 10001;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        $auth_status = get_user_auth_status($uid);
        if ($auth_status != 2) {
            $result['code'] = 0;
            $result['msg'] = '已经提交认证,请勿重复提交';
            return_json_encode($result);
        } else if ($auth_status == 2) {

            //添加视频认证记录
            $auth_video_data = ['status' => 0, 'video_id' => $video_id, 'video_url' => $video_url, 'cover_url' => $cover_url, 'create_time' => time()];
            db('user_auth_video')->where(['user_id' => $uid])->update($auth_video_data);
        } else {
            //添加视频认证记录
            $auth_video_data = ['user_id' => $uid, 'video_id' => $video_id, 'video_url' => $video_url, 'cover_url' => $cover_url, 'create_time' => time()];
            db('user_auth_video')->insert($auth_video_data);
        }

        return_json_encode($result);
    }

    //提现
    public function cash_income()
    {

        $result = array('code' => 1, 'msg' => '', 'data' => array());
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));
        $name = trim(input('param.name'));
        $number = trim(input('param.number'));

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token, ['income']);

        if (!$user_info) {
            $result['code'] = 10001;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        //账号是否被禁用
        if ($user_info['user_status'] == 0) {
            $result['code'] = 0;
            $result['msg'] = "您因涉嫌违规，账号受限，请联系管理员!";
            return_json_encode($result);
        }

        //检查是否认证
        $auth_status = get_user_auth_status($uid);
        if ($auth_status != 1) {
            $result['code'] = 0;
            $result['msg'] = '认证并审核通过才可以提现！';
            return_json_encode($result);
        }

        $config = load_cache('config');
        if ($user_info['income'] < $config['min_cash_income']) {
            $result['code'] = 0;
            $result['msg'] = '最低提现额度为' . $config['min_cash_income'] . '积分';
            return_json_encode($result);
        }

        //扣除剩余提现额度
        $inc_income = db('user')->where('id', '=', $uid)->setField('income', 0);
        if ($inc_income) {

            //添加提现记录
            $record = ['gathering_name' => $name, 'gathering_number' => $number, 'user_id' => $uid, 'income' => $user_info['income'], 'create_time' => NOW_TIME];
            db('user_cash_record')->insert($record);
        }

        return_json_encode($result);
    }

    //举报用户
    public function do_report_user()
    {

        $result = array('code' => 1, 'msg' => '');
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));
        $to_user_id = intval(input('param.to_user_id'));
        $type = trim(input('param.type'));
        $content = trim(input('param.content'));

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token, ['income']);

        if (!$user_info) {
            $result['code'] = 10001;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        //添加记录
        $report_record = [
            'uid' => $uid,
            'reportid' => $to_user_id,
            'reporttype' => $type,
            'content' => $content,
            'addtime' => NOW_TIME,
        ];

        $log_id = db('user_report')->insertGetId($report_record);

        $img = request()->file();     //获取举报图
        if (count($img) > 3) {
            $result['code'] = 0;
            $result['msg'] = '图片数量最多3张';
            return_json_encode($result);
        }

        $data = [];
        foreach ($img as $k => $v) {
            $uploads = oss_upload($v);      //单图片上传
            if ($uploads) {
                $data[$k]['report'] = $log_id;
                $data[$k]['addtime'] = NOW_TIME;
                $data[$k]['img'] = $uploads;
            }
        }
        //举报截图
        db('user_report_img')->insertAll($data);

        return_json_encode($result);

    }

    //修改按时付费价格
    public function change_video_line_money()
    {

        $result = array('code' => 1, 'msg' => '');
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));
        $coin = intval(input('param.coin'));

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }
        $user_info = check_token($uid, $token, ['income']);

        if (!$user_info) {
            $result['code'] = 10001;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        if ($user_info['sex'] != 2) {
            $result['code'] = 0;
            $result['msg'] = '女性用户才可以自定义金额!';
            return_json_encode($result);
        }

        if ($coin <= 0) {
            $result['code'] = 0;
            $result['msg'] = '数量必须大于0!';
            return_json_encode($result);
        }

        $user_level = get_level($uid);

        $config = load_cache('config');
        //判断用户等级是否符合规定
        if ($user_level < $config['custom_video_money_level']) {
            $result['code'] = 0;
            $result['msg'] = '等级不符合要求，最低为:' . $config['custom_video_money_level'];
            return_json_encode($result);
        }

        //是否在合理范围内
        $range = explode('-', $config['video_call_coin_range']);
        if (count($range) == 2) {

            if ($coin < $range[0] || $coin > $range[1]) {
                $result['code'] = 0;
                $result['msg'] = '视频聊收费范围为' . $config['video_call_coin_range'] . '，请重新设置';
                return_json_encode($result);
            }
        }

        //修改自定义扣费金额
        db('user')->where('id', '=', $uid)->setField('custom_video_charging_coin', $coin);

        return_json_encode($result);
    }

    /*
     * 免打扰设置
     * */
    public function request_set_do_not_disturb()
    {

        $result = array('code' => 1, 'msg' => '');
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));
        $type = intval(input('param.type'));

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }
        $user_info = check_token($uid, $token, ['income']);

        if (!$user_info) {
            $result['code'] = 10001;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        if ($type == 1) {
            db('user')->where('id', '=', $uid)->setField('is_open_do_not_disturb', 1);
        } else {

            db('user')->where('id', '=', $uid)->setField('is_open_do_not_disturb', 0);
        }
        return_json_encode($result);

    }

}