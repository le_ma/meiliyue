<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:46:"themes/admin_simpleboot3/admin/agent/link.html";i:1532317528;s:43:"themes/admin_simpleboot3/public/header.html";i:1519953092;}*/ ?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="__TMPL__/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="__TMPL__/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="__STATIC__/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "__ROOT__/",
            WEB_ROOT: "__WEB_ROOT__/",
            JS_ROOT: "static/js/",
            APP: '<?php echo \think\Request::instance()->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="__TMPL__/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="__STATIC__/js/layer/layer.js" rel="stylesheet"></script>
    <script src="__STATIC__/js/wind.js"></script>
    <script src="__TMPL__/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip();
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
</head>
<body>
<div class="wrap js-check-wrap">
    <ul class="nav nav-tabs">
        <li class="active"><a href="<?php echo url('agent/link'); ?>">渠道管理</a></li>
        <li><a href="<?php echo url('agent/addlink'); ?>">添加渠道</a></li>
    </ul>
    <form class="well form-inline margin-top-20" method="post" action="<?php echo url('agent/link'); ?>">

        代理id:
        <input type="text" class="form-control" name="agent_id" style="width: 120px;" value="<?php echo (isset($data['agent_id']) && ($data['agent_id'] !== '')?$data['agent_id']:''); ?>" placeholder="请输入代理id">
        渠道号:
        <input type="text" class="form-control" name="channel" style="width: 120px;" value="<?php echo (isset($data['channel']) && ($data['channel'] !== '')?$data['channel']:''); ?>" placeholder="请输入代理id">
        状态：
        <select class="form-control" name="status" style="width: 140px;">
            <option value="0"> 封号</option>
            <option value="1" <?php if($data['status'] == 1): ?> selected='selected' <?php endif; ?>>正常</option>
        </select>
        <input type="submit" class="btn btn-primary" value="搜索" />
        <a class="btn btn-danger" href="<?php echo url('agent/link'); ?>">清空</a>
    </form>
    <table class="table table-hover table-bordered">
        <thead>
        <tr>
            <th width="50">ID</th>
            <th>渠道号</th>
            <th>下载链接地址</th>
            <th>代理登录名 (ID)</th>
            <th>分成比例（%）</th>
            <th><?php echo lang('STATUS'); ?></th>
            <th>上级代理（ID）</th>
            <th width="130"><?php echo lang('ACTIONS'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php $user_statuses=array("0"=>'账号已封',"1"=>'正常'); if(is_array($users) || $users instanceof \think\Collection || $users instanceof \think\Paginator): if( count($users)==0 ) : echo "" ;else: foreach($users as $key=>$vo): ?>
            <tr>
                <td><?php echo $vo['id']; ?></td>
                <td><?php echo $vo['channel']; ?></td>
                <td><?php echo $vo['link']; ?></td>
                <td><?php echo $vo['agent_login']; ?>(<?php echo $vo['agent_id1']; ?>)</td>
                <td><?php echo (isset($vo['divide_into1'] ) && ($vo['divide_into1']  !== '')?$vo['divide_into1'] :'0'); ?> </td>
                <td><?php echo $user_statuses[$vo['status']]; ?></td>
                <td><?php echo $vo['agent_user']; ?> (<?php echo $vo['agent_id']; ?>)</td>
                <td>
                    <a href='<?php echo url("agent/addlink",array("id"=>$vo["id"])); ?>'><?php echo lang('EDIT'); ?></a>
                    <a class="js-ajax-delete" href="<?php echo url('agent/deletelink',array('id'=>$vo['id'])); ?>"><?php echo lang('DELETE'); ?></a>
                    <a  href="<?php echo url('agent/index',array('superior_id'=>$vo['id'])); ?>">下级详情</a>

                </td>
            </tr>
        <?php endforeach; endif; else: echo "" ;endif; ?>
        </tbody>
    </table>
    <div class="pagination"><?php echo $page; ?></div>
</div>
<script src="__STATIC__/js/admin.js"></script>
</body>
</html>