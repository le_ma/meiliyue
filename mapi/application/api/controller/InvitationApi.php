<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/7/12 0012
 * Time: 上午 11:59
 */

namespace app\api\controller;

use app\api\controller\Base;
use think\Db;

class InvitationApi extends Base
{
    //邀请奖励规则

    public function index()
    {
        $uid = input("param.uid");
        //获取邀请码
        $invite_code = create_invite_code_0910($uid);

        $config = load_cache('config');

        //邀请男性好友注册赠送收益
        $invite_reg_reward_man = $config['invite_reg_reward_man'];
        //邀请女性好友注册赠送收益
        $invite_reg_reward_female = $config['invite_reg_reward_female'];
        //赚取积分额外奖励（单位%）
        $invite_income_ratio = $config['invite_income_ratio'] * 100;
        //消费聊币额外奖励(%)
        $invite_income_ratio_female = $config['invite_income_ratio_female'] * 100;

        //获取用户人数
        $invite_user_list = Db::name('invite_record')->where('user_id', '=', $uid)->select();

        $sum = 0;

        foreach ($invite_user_list as &$v) {
            //用户奖励总数
            $v['income_total'] = db('invite_profit_record')->where('user_id', '=', $uid)->where('invite_user_id', '=', $v['invite_user_id'])->sum('income');

            $sum += $v['income_total'];
        }

        $data = array(
            'sum' => count($invite_user_list),
            'reward_total' => number_format($sum, 2),
            'invite_income_ratio' => $invite_income_ratio,
            'invite_income_ratio_female' => $invite_income_ratio_female,
            'invite_reg_reward_man' => $invite_reg_reward_man,
            'invite_reg_reward_female' => $invite_reg_reward_female,
            'code' => $invite_code,
            'uid' => $uid,
            'invite_url' => 'http://' . $_SERVER['HTTP_HOST'] . '/mapi/public/index.php/api/download_api/index?invite_code=' . $invite_code,
        );


        $this->assign("data", $data);
        return $this->fetch();
    }

    //邀请规则
    public function details()
    {

        $uid = input("param.uid");

        $category = db('portal_category')->where("name='邀请奖励规则'")->find();

        $sex_type = db('portal_category_post')->where("category_id=" . $category['id'])->find();
        $oldTagIds = db('portal_post')->where("id=" . $sex_type['post_id'])->find();

        $this->assign("rules", html_entity_decode($oldTagIds['post_content']));

        return $this->fetch();
    }

    //我的邀请人
    public function inviter()
    {
        $uid = input("param.uid");

        $invite_user_list = Db::name('invite_record')->alias('i')
            ->field('u.avatar,u.id,u.user_nickname,u.create_time')
            ->join(config('database.prefix') . 'user u', 'i.invite_user_id=u.id')
            ->where('i.user_id', '=', $uid)
            ->limit(15)
            ->select();

        $this->assign("list", $invite_user_list);
        $this->assign('uid', $uid);
        return $this->fetch();
    }

    //邀请人分页
    public function inviter_page()
    {
        $uid = input("param.uid");
        $page = input("param.page") * 15;

        $invite_user_list = Db::name('invite_record')->alias('i')
            ->field('u.avatar,u.id,u.user_nickname,u.create_time')
            ->join(config('database.prefix') . 'user u', 'i.invite_user_id=u.id')
            ->where('i.user_id', '=', $uid)
            ->limit($page, 15)
            ->select();

        echo json_encode($invite_user_list);
    }

    /*
     * 邀请收益
     * */
    public function rewards()
    {
        $uid = input("param.uid");

        $user = db("user") -> where("id=$uid") -> field('invitation_coin') -> find();
        $result = db("invite_cash_record")->where("uid=$uid")->order("addtime desc")->select();
        $invitation_coin = $user['invitation_coin'];
        if (!$user || empty($invitation_coin)) {
            $invitation_coin = 0;
        }

        $this->assign("invitation_coin", number_format($invitation_coin, 2));
        $this->assign('list', $result);
        $this->assign('uid', $uid);
        return $this->fetch();
    }

    public function add_rewards()
    {
        $val = input("param.val");
        $pay = input("param.pay");
        $uid = input("param.uid");
        $root = array('msg' => '参数错误', 'status' => '0');
//        if ($val < 50) {
//            $root['msg'] = '提现金额大于50元';
//            echo json_encode($root);
//            exit;
//        }
        if ($pay < 0) {
            $root['msg'] = '请填写支付宝账号';
            echo json_encode($root);
            exit;
        }
        if (!$uid) {
            $root['msg'] = '用户过期,请重新登录';
            echo json_encode($root);
            exit;
        }
        $data = array(
            'uid' => $uid,
            'coin' => $val,
            'pay' => $pay,
            'addtime' => NOW_TIME,
            'status' => 0,
        );
        db('user')->where('id=' . $uid) -> dec('invitation_coin', $val)->update();
        $result = db("invite_cash_record")->insert($data);
        if ($result) {
            $root['status'] = 1;
            $root['msg'] = '提现成功，等待审核';
        } else {
            $root['msg'] = '提现失败';
        }
        echo json_encode($root);
    }
}