<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/1/23 0023
 * Time: 上午 11:04
 */

namespace app\user\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class IdentityController extends AdminBaseController
{
    public function change_status_auth_info(){

        $id = input('param.id', 0, 'intval');
        $uid = input('param.uid', 0, 'intval');
        $type = input('param.type', 0, 'intval');

        if ($type == '1') {
            push_msg_user(5, $uid, 1);
        } else {
            push_msg_user(6, $uid, 1);
        }
        $user = Db::name("auth_form_record")->where('id', '=', $id)->update(array("status" => $type));

        if ($user) {
            $this->success("操作成功");
        } else {
            $this->success("操作失败");
        }
    }

    //信息认证列表
    public function auth_info_list(){


        $where = [];
        if(isset($_REQUEST['uid']) && $_REQUEST['uid'] != ''){
            $where['u.id'] = $_REQUEST['uid'];
        }else{
            $_REQUEST['uid'] = '';
        }

        if(isset($_REQUEST['status']) && $_REQUEST['status'] != ''){
            $where['a.status'] = $_REQUEST['status'];
        }else{
            $_REQUEST['status'] = -1;
        }


        $auth_list = db('user') -> alias('u') -> join('auth_form_record a','u.id=a.user_id') -> field('u.user_nickname,u.sex,a.*') -> where($where) ->order('create_time desc')->paginate(20);
        $lists = $auth_list->toArray();

        $this->assign('list', $lists['data']);
        $this->assign('page', $auth_list->render());
        $this->assign('request',$_REQUEST);
        return $this->fetch();
    }

    //身份验证
    public function index()
    {
        if (request()->post()) {
            session('identity', request()->post());
        }
        if (!request()->get('page') and !request()->post()) {
            session('identity', null);
        }

        $where = session('identity.status') ? "status=" . session('identity.status') : "status=0";
        $where .= session('identity.uid') ? " and uid=" . session('identity.uid') : '';

        $user = Db::name("user_auth_video")->where($where)->order('create_time desc')->paginate(20);
        $lists = $user->toArray();

        $key = db('config')->where('code', '=', 'tencent_video_sign_key')->find();
        $key = $key['val'];
        foreach ($lists['data'] as &$v) {
            $uid = $v['user_id'];
            $users = Db::name("user")->where("id=$uid")->find();
            $v['user_nickname'] = $users['user_nickname'];
            $v['avatar'] = $users['avatar'];

            $parse_url_arr = parse_url($v['video_url']);
            $url_dir = substr($parse_url_arr['path'], 0, strrpos($parse_url_arr['path'], '/') + 1);
            $t = dechex(time() + 60 * 60 * 24);
            $us = cmf_random_string();
            $sign = md5($key . $url_dir . $t . $us);

            $sign_video_url = $v['video_url'] . '?t=' . $t . '&us=' . $us . '&sign=' . $sign;
            //echo $sign_video_url;exit;
            $v['video_url'] = $sign_video_url;
        }

        $this->assign('user', $lists['data']);
        $this->assign('request', session('identity'));
        $this->assign('page', $user->render());
        return $this->fetch();
    }

    //审核验证类型
    public function upd()
    {
        $id = input('param.id', 0, 'intval');
        $uid = input('param.uid', 0, 'intval');
        $type = input('param.type', 0, 'intval');

        if ($type == '1') {
            push_msg_user(5, $uid, 1);
        } else {
            push_msg_user(6, $uid, 1);
        }
        $user = Db::name("user_auth_video")->where('id', '=', $id)->update(array("status" => $type));

        if ($user) {
            $this->success("操作成功");
        } else {
            $this->success("操作失败");
        }
    }

}
