<?php
/**
 * Created by PhpStorm.
 * User: weipeng
 * Date: 2018/3/2
 * Time: 9:21
 */


define('DOCUMENT_ROOT',$_SERVER['DOCUMENT_ROOT']);

define('NOW_TIME',time());

define("SITE_URL",'http://' . $_SERVER['HTTP_HOST']);

define('OPEN_CUSTOM_VIDEO_CHARGE_COIN',1);//是否开启自定义分钟扣费金额

define("IP_REG_MAX_COUNT",100); //每个IP只能注册的数量

define("IS_TEST",0); //是否是测试模式

define('OPEN_STAR',0);//是否开启星级模式

define('OPEN_INVITE',0);//是否开启邀请模块

define('OPEN_VIDEO_CHAT',0);//是否开启视频聊