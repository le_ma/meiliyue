<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:56:"themes/admin_simpleboot3/admin/refill/edit_pay_menu.html";i:1522229316;s:43:"themes/admin_simpleboot3/public/header.html";i:1519953092;}*/ ?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="__TMPL__/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="__TMPL__/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="__STATIC__/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "__ROOT__/",
            WEB_ROOT: "__WEB_ROOT__/",
            JS_ROOT: "static/js/",
            APP: '<?php echo \think\Request::instance()->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="__TMPL__/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="__STATIC__/js/layer/layer.js" rel="stylesheet"></script>
    <script src="__STATIC__/js/wind.js"></script>
    <script src="__TMPL__/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip();
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
<style type="text/css">
    .pic-list li {
        margin-bottom: 5px;
    }
</style>

</head>
<body>
<div class="wrap js-check-wrap">
    <ul class="nav nav-tabs">
        <li><a href="<?php echo url('refill/pay_menu'); ?>">支付渠道列表</a></li>
        <li class="active"><a href="javascript:;">添加支付渠道</a></li>
    </ul>
    <form action="<?php echo url('refill/edit_pay_menu_post'); ?>" method="post" >
        <div class="row">
            <div class="col-md-9">
                <table class="table table-bordered">
                    <tr>
                        <th>名称<span class="form-required">*</span></th>
                        <td>
                            <input class="form-control" type="text" name="post[pay_name]"
                                   id="title" required value="<?php echo (isset($data['pay_name'] ) && ($data['pay_name']  !== '')?$data['pay_name'] :''); ?>" placeholder="请输入名称"/>
                        </td>
                    </tr>

                    <tr>
                        <th>支付图标<span class="form-required">*</span></th>
                        <td>
                            <div style="text-align: center;">
                                <input type="hidden" name="post[icon]" id="thumbnail" value="<?php echo (isset($data['icon'] ) && ($data['icon']  !== '')?$data['icon'] :''); ?>">
                                <a href="javascript:uploadOneImage('图片上传','#thumbnail');">
                                    <?php if($data['icon']): ?>
                                        <img src="<?php echo $data['icon']; ?>"
                                             id="thumbnail-preview"
                                             width="135" style="cursor: pointer"/>
                                        <?php else: ?>
                                        <img src="__TMPL__/public/assets/images/default-thumbnail.png"
                                             id="thumbnail-preview"
                                             width="135" style="cursor: pointer"/>
                                    <?php endif; ?>

                                </a>
                                <input type="button" class="btn btn-sm btn-cancel-thumbnail" value="取消图片">
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <th>商户号 <span class="form-required">*</span></th>
                        <td><input class="form-control" type="text" name="post[merchant_id]" id="merchant_id" value="<?php echo (isset($data['merchant_id'] ) && ($data['merchant_id']  !== '')?$data['merchant_id'] :''); ?>"
                                   placeholder="请输入商户号"></td>
                    </tr>

                    <tr>
                        <th>APP_ID <span class="form-required">*</span></th>
                        <td><input class="form-control" type="text" name="post[app_id]" id="app_id" value="<?php echo (isset($data['app_id'] ) && ($data['app_id']  !== '')?$data['app_id'] :''); ?>"
                                   placeholder="请输入APP_ID"></td>
                    </tr>


                    <tr>
                        <th>类名 <span class="form-required">*</span></th>
                        <td><input class="form-control" type="text" name="post[class_name]" id="class_name" value="<?php echo (isset($data['class_name'] ) && ($data['class_name']  !== '')?$data['class_name'] :''); ?>"
                                   placeholder="请输入类名称"></td>
                    </tr>
                    <tr>
                        <th>状态<span class="form-required">*</span></th>
                        <td>
                            <input  type="radio" name="post[status]" <?php if($data['status'] == 1): ?>checked="checked"<?php endif; ?>  value="1"/>&nbsp;&nbsp;开启 &nbsp;&nbsp;&nbsp;&nbsp;
                            <input  type="radio" name="post[status]" <?php if($data['status'] == 0): ?>checked="checked"<?php endif; ?> value="0"/>&nbsp;&nbsp;关闭
                        </td>
                    </tr>
                    <tr>
                        <th>公钥 <span class="form-required">*</span></th>
                        <td><input class="form-control" type="text" name="post[public_key]" id="public_key" value="<?php echo (isset($data['public_key'] ) && ($data['public_key']  !== '')?$data['public_key'] :''); ?>"
                                   placeholder="公钥"></td>
                    </tr>

                    <tr>
                        <th>私钥<span class="form-required">*</span></th>

                        <td><input class="form-control" type="text" name="post[private_key]" id="private_key" value="<?php echo (isset($data['private_key'] ) && ($data['private_key']  !== '')?$data['private_key'] :''); ?>"
                                   placeholder="私钥"></td>
                    </tr>
                </table>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary js-ajax-submit"><?php echo lang('ADD'); ?></button>
                        <a class="btn btn-default" href="<?php echo url('refill/pay_menu'); ?>"><?php echo lang('BACK'); ?></a>
                    </div>
                </div>


                <input type="hidden" name="id" value="<?php echo (isset($data['id'] ) && ($data['id']  !== '')?$data['id'] :''); ?>"/>
            </div>

        </div>
    </form>
</div>
<script type="text/javascript" src="__STATIC__/js/admin.js"></script>


</body>
</html>
