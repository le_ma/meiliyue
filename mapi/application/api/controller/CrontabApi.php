<?php
/**
 * Created by PhpStorm.
 * User: weipeng
 * Date: 2018/3/19
 * Time: 11:33
 */

namespace app\api\controller;


class CrontabApi extends Base
{

    public function service_crontab(){
        crontab_do_end_live();
        crontab_do_end_call();

        //清除所有过期的心跳

        $config = load_cache('config');
        //时间
        $time = NOW_TIME - $config['heartbeat_interval'] - 60;//偏移量5秒
        db('monitor') -> where('monitor_time','<',$time) -> delete();
    }
}