<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/7/21 0021
 * Time: 上午 10:25
 */
namespace app\admin\controller;
use cmf\controller\AdminBaseController;
use QcloudApi;
use think\Db;

class SettlementController extends AdminBaseController
{
     //结算记录
    public function index(){
        $p=$this->request->param('page');
        if(empty($p) and !$this->request->param('start_time') and !$this->request->param('end_time')){
            session("Settlement",null);
        }else if(empty($p)){

            $data['start_time'] = $this->request->param('start_time');
            $data['end_time'] = $this->request->param('end_time');

            session("Settlement",$data);
        }

        $start_times=session("Settlement.start_time");
        $end_times=session("Settlement.end_time");


        $start_time=$start_times ?  strtotime($start_times) :'0';

        $end_time=$end_times ? strtotime($end_times) : time();

        $id       = cmf_get_current_admin_id();

        $agent=Db::name('agent')->where("id=$id")->find();

        if($agent['agent_level'] =='1'){

            $where['agent_id1']=$id;

            $type='divide_into1';

        }elseif($agent['agent_level'] =='2'){

            $where['agent_id2']=$id;

            $type='divide_into2';

        }else{

            $where['agent_id3']=$id;

            $type='divide_into3';
        }

        $where['addtime'] = array('between', array($start_time,$end_time));

        $list=Db::name('agent_settlement')
                    ->field("*,sum(money) sum")
                    ->group("beforetime")
                    ->order("beforetime desc")
                    ->where($where)
                    ->paginate(10);
        // 获取分页显示
        $page = $list->render();

        $user=$list->toArray();

        $count='0';   //统计获取的总金额

        foreach ($user['data'] as &$v){

            $v['divided']=round($v['sum']*$v[$type]/100,2);

            $count+=$v['divided'];

        }
        $data=array(

            'type'=>$type,

            'count'=>$count,

            'end_time' =>$end_times,

            'start_time' =>$start_times,
        );

        $this->assign("page", $page);

        $this->assign("data", $data);

        $this->assign("users", $user['data']);

        return $this->fetch();

    }
    //转化统计
    public function conversion(){

        $p=$this->request->param('page');
        if(empty($p) and !$this->request->param('channel')){
            session("conversion",null);
        }else if(empty($p)){

            $data['channel'] = $this->request->param('channel');


            session("conversion",$data);
        }

        $channel=session("conversion.channel");



        $id       = cmf_get_current_admin_id();

        if ($channel) {

            $where="(agent_id1 =$id or agent_id2 =$id or agent_id3 =$id) and channel=".$channel;

        }else{

            $where="agent_id1 =$id or agent_id2 =$id or agent_id3 =$id";

        }

        $agent=Db::name('agent_link')->where($where)->paginate(10);

        // 获取分页显示
        $page = $agent->render();

        $user=$agent->toArray();

        foreach ($user['data'] as &$v){

            $v['registered']=Db::name('user')->where("link_id=".$v['channel'])->count();  //注册数

            $v['sum']=Db::name('agent_settlement')->where("channel=".$v['channel'])->sum("money");               //充值总数
        }

        $this->assign("page", $page);

        $this->assign("users", $user['data']);

        return $this->fetch();
    }

    //申请提现
    public function withdrawal(){

        $id       = cmf_get_current_admin_id();
        $user=Db::name('agent')->where("id=".$id)->find();
        if($user['agent_level'] =='1'){
            $type='divide_into1';
        }elseif($user['agent_level'] =='2'){
            $type='divide_into2';
        }else{
            $type='divide_into3';
        }
        //获取代理的总收益
       $name= Db::name('agent_settlement')->where("agent_id1=$id or agent_id2=$id or agent_id3=$id")->select();
       $sum='0';
        foreach ($name as $v){
            $sum+=round($v['money']*$v[$type]/100,2);
        }

        //获取提现金额

        $tix= Db::name('agent_withdrawal')->where("agent_id=$id and status !=2")->sum("money");

        //获取剩余金额
        $residue= $sum - $tix;

        $data=array(
            'sum' =>$sum,
            'residue' =>$residue,
            'withdrawal'  =>$tix
        );
        $this->assign("data", $data);
        return $this->fetch();
    }
        //提现
    public function addwithdrawal(){
        $id       = cmf_get_current_admin_id();

        $money=$this->request->param('money');
        $data=array(
            'money' =>$money,
            'agent_id' =>$id,
            'addtime'  =>time(),

        );
        $user=Db::name("agent_withdrawal")->insertGetId($data);
        if($user){
            echo json_encode("1");
        }else{
            echo json_encode("0");
        }

    }
    //显示提现记录
    public function withdrawalrecord(){
        $p=$this->request->param('page');
        if(empty($p) and !$this->request->param('start_time') and !$this->request->param('end_time') and !$this->request->param('type')){
            session("withdrawalrecord",null);
        }else if(empty($p)){

            $data['start_time'] = $this->request->param('start_time');
            $data['end_time'] = $this->request->param('end_time');
            $data['type'] = $this->request->param('type');

            session("withdrawalrecord",$data);
        }

        $start_times=session("withdrawalrecord.start_time");
        $end_times=session("withdrawalrecord.end_time");
        $type=session("withdrawalrecord.type");


        $start_time=$start_times ?  strtotime($start_times) :'0';

        $end_time=$end_times ? strtotime($end_times) : time();


        $id       = cmf_get_current_admin_id();

        $where['agent_id']=$id;

        $where['addtime'] = array('between', array($start_time,$end_time));

        $type >='0' ? $where['status']=$type : '';

        $user=Db::name("agent_withdrawal")->where($where)->order("addtime desc")->paginate(10);

        $money=Db::name("agent_withdrawal")->where($where)->sum("money");    //统计总提现金额

        $page = $user->render();

        $user=$user->toArray();

        $this->assign("page", $page);

        $this->assign("data", session("withdrawalrecord"));

        $this->assign("money", $money);

        $this->assign("users", $user['data']);

        return $this->fetch();
    }

    //显示代理提现记录
    public function agentwithdrawal(){
        $p=$this->request->param('page');
        if(empty($p) and !$this->request->param('start_time') and !$this->request->param('end_time') and !$this->request->param('type')){
            session("agentwithdrawal",null);
        }else if(empty($p)){

            $data['start_time'] = $this->request->param('start_time');
            $data['end_time'] = $this->request->param('end_time');
            $data['type'] = $this->request->param('type');

            session("agentwithdrawal",$data);
        }

        $start_times=session("agentwithdrawal.start_time");
        $end_times=session("agentwithdrawal.end_time");
        $type=session("agentwithdrawal.type");


        $start_time=$start_times ?  strtotime($start_times) :'0';

        $end_time=$end_times ? strtotime($end_times) : time();


        $id       = cmf_get_current_admin_id();

        $where['a.superior_id']=$id;

        $where['s.addtime'] = array('between', array($start_time,$end_time));

        $type >='0' ? $where['s.status']=$type : '';

        $user=Db::name("agent_withdrawal")->alias("s")
                    ->where($where)
                    ->join("agent a","a.id=s.agent_id")
                    ->field("s.*")
                    ->order("s.addtime desc")
                    ->paginate(10);

        $money=Db::name("agent_withdrawal")->alias("s")
                        ->where($where)
                        ->join("agent a","a.id=s.agent_id")->sum("money");    //统计总提现金额

        $page = $user->render();

        $user=$user->toArray();

        $this->assign("page", $page);

        $this->assign("data", session("agentwithdrawal"));

        $this->assign("money", $money);

        $this->assign("users", $user['data']);

        return $this->fetch();
    }
    //修改提现状态
    public function agenttype(){
        $id=$this->request->param('id');
        $status=$this->request->param('status');
        $data=array(
            'status' =>$status,
        );
        $name=Db::name("agent_withdrawal")->where("id=$id")->update($data);
        if($name){
            $this->success(lang('操作成功'));
        }else{
            $this->error(lang('操作失败'));
        }
    }

}