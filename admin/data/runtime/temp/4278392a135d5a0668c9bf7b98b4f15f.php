<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:49:"themes/admin_simpleboot3/admin/consume/index.html";i:1532911628;s:43:"themes/admin_simpleboot3/public/header.html";i:1519953092;}*/ ?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="__TMPL__/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="__TMPL__/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="__STATIC__/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "__ROOT__/",
            WEB_ROOT: "__WEB_ROOT__/",
            JS_ROOT: "static/js/",
            APP: '<?php echo \think\Request::instance()->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="__TMPL__/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="__STATIC__/js/layer/layer.js" rel="stylesheet"></script>
    <script src="__STATIC__/js/wind.js"></script>
    <script src="__TMPL__/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip();
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
<style>
    .gift-img{width:50px;height:50px}
    .gift-img img{width:100%;height:100%;}
    .gift-in input{width:25px;}
    .js-ajax-form{margin-top:30px;}
    .identity img{width:30px;height:30px;border-radius: 50%;}
    .details{cursor: pointer;}
    .layui-layer-demo .layui-layer-title{
        background: #e0e0e0!important;
    }
    .form-control{width:110px!important;}
    #status,#type{    width: 100px;
        height: 32px;
        border-color: #dce4ec;
        color: #aeb5bb;}
    .table-list{font-size:14px!important;}
    .consume-col{color:#ff41ee;}
</style>
</head>
<body>
<div class="wrap js-check-wrap">
    <ul class="nav nav-tabs">
        <li class="active"><a href="javascript:;">消费记录表</a></li>
    </ul>
    <form class="well form-inline margin-top-20" method="post" action="<?php echo url('Consume/index'); ?>">
        用户消费ID：
        <input class="form-control" type="text" name="uid" style="width: 200px;" value="<?php echo input('request.uid'); ?>"
               placeholder="请输入用户ID">
        用户收益ID：
        <input class="form-control" type="text" name="touid" style="width: 200px;" value="<?php echo input('request.touid'); ?>"
               placeholder="请输入用户ID">
        消费类型：
        <select name="type" id="type">
            <option value="-1">全部消费</option>
            <option value="1" <?php if($request['type'] == 1): ?> selected="selected" <?php endif; ?>>短视频消费</option>
            <option value="2" <?php if($request['type'] == 2): ?> selected="selected" <?php endif; ?>>私照消费</option>
            <option value="3" <?php if($request['type'] == 3): ?> selected="selected" <?php endif; ?>>礼物消费</option>
            <option value="4" <?php if($request['type'] == 4): ?> selected="selected" <?php endif; ?>>一对一视频消费</option>
        </select>
        <input type="submit" class="btn btn-primary" value="搜索"/>
        <a class="btn btn-danger" href="<?php echo url('Consume/index'); ?>">清空</a>
    </form>

    <?php 

        $type = array(0 => '其他消费',1 => '视频消费',2 => '私照消费' ,3 => '礼物消费', 4 => '一对一视频消费',5 => '私信消息付费')

     ?>
    <form class="js-ajax-form" action="<?php echo url('Consume/upd'); ?>" method="post">
        <h4>总消费<?php echo $total; ?>(币)</h4>
        <table class="table table-hover table-bordered table-list">
            <thead>
            <tr>
                <th>ID</th>
                <th>消费用户（ID）</th>
                <th>收益用户（ID）</th>
                <th>消费数量（金币）</th>
                <th>消费说明</th>
                <th>消费类型</th>
                <th>消费时间</th>

            </tr>
            </thead>
            <tfoot>

            <?php if(is_array($data) || $data instanceof \think\Collection || $data instanceof \think\Paginator): if( count($data)==0 ) : echo "" ;else: foreach($data as $key=>$vo): ?>
                <tr>
                    <td><?php echo $vo['id']; ?></td>
                    <td><?php echo $vo['uname']; ?>(<?php echo $vo['user_id']; ?>)</td>
                    <td><?php echo $vo['toname']; ?>(<?php echo $vo['to_user_id']; ?>)</td>
                    <td><?php echo $vo['coin']; ?></td>
                    <th><?php echo $vo['content']; ?></th>
                    <th><?php echo $type[$vo['type']]; ?></th>
                    <th><?php echo date("Y-m-d H:i:s",$vo['create_time'] ); ?></th>
                </tr>
            <?php endforeach; endif; else: echo "" ;endif; ?>
            </tfoot>
        </table>
        <ul class="pagination"><?php echo $page; ?></ul>

    </form>

</div>
<script src="__STATIC__/js/layer/layer.js" rel="stylesheet"></script>
<script>
    $(".details").click(function(){
        var url=$(this).attr("data-url");
        //自定页
        layer.open({
            title: '视频认证',
            type: 1,
            skin: 'layui-layer-demo', //样式类名
            closeBtn: 0, //不显示关闭按钮
            anim: 2,
            shadeClose: true, //开启遮罩关闭
            content:"<video src='"+url+"' controls='controls'></video>"
        });
    })


</script>
</body>
</html>