<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/7/20 0020
 * Time: 上午 10:38
 */

namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use QcloudApi;
use think\Db;

class AgentController extends AdminBaseController
{
    //代理管理
    public function index()
    {
        $where = '';
        /**搜索条件**/


        $p = $this->request->param('page');
        if (empty($p) and !$this->request->param('agent_login') and !$this->request->param('superior_id') and !$this->request->param('agent_id') and !$this->request->param('agent_level')) {
            session("admin_agent", null);
        } else if (empty($p)) {

            $data['agent_login'] = $this->request->param('agent_login');
            $data['superior_id'] = $this->request->param('superior_id');
            $data['agent_id'] = $this->request->param('agent_id');

            $data['agent_level'] = $this->request->param('agent_level');
            $data['status'] = $this->request->param('status');

            session("admin_agent", $data);
        }

        $user_login = session("admin_agent.agent_login");
        $superior_id = session("admin_agent.superior_id");
        $agentid = session("admin_agent.agent_id");

        $agent_level = session("admin_agent.agent_level");

        if ($user_login) {
            $where['agent_login'] = ['like', "%$user_login%"];
        }
        if ($agent_level) {
            $where['agent_level'] = $agent_level;
        }
        if ($superior_id) {
            $where['superior_id'] = $superior_id;
        }
        if ($agentid) {
            $where['id'] = $agentid;
        }

        $users = Db::name('agent')
            ->where($where)
            ->order("id DESC")
            ->paginate(10);

        // 获取分页显示
        $page = $users->render();
        $name = $users->toArray();

        foreach ($name['data'] as &$v) {

            $vid = $v['superior_id'];
            $users = Db::name('agent')->where("id=$vid")->find(); //上级代理
            if ($users) {
                $v['agent_user'] = $users['agent_login'];
                $v['agent_id'] = $users['id'];
            } else {
                $v['agent_user'] = '超级管理员';
                $v['agent_id'] = '1';
            }
            //获取注册总数
            $sid = $v['id'];
            $v['registered'] = Db::name('agent_link')->alias("a")
                ->join("user u", "u.link_id=a.channel")
                ->where("a.agent_id1=$sid or a.agent_id2=$sid or a.agent_id3=$sid")
                ->count("u.id");
            $v['money'] = Db::name('agent_settlement')->where("agent_id1=$sid or agent_id2=$sid or agent_id3=$sid")->sum("money");

        }

        $this->assign("page", $page);

        $this->assign("users", $name['data']);
        $this->assign("data", session("admin_agent"));
        return $this->fetch();
    }

    //添加代理
    public function add()
    {
        $roles = Db::name('role')->where(['status' => 1])->order("id DESC")->select();
        $this->assign("roles", $roles);
        return $this->fetch();
    }

    //提交代理
    public function addPost()
    {
        if ($this->request->isPost()) {
            $login = $_POST['agent_login'];
            $user = DB::name('agent')->where("agent_login ='$login'")->find();
            if ($user) {
                $this->error("添加失败,账号已存在！");
            }
            $_POST['agent_pass'] = cmf_password($_POST['agent_pass']);
            $_POST['superior_id'] = $_POST['superior_id'] ? $_POST['superior_id'] : '0';

            $_POST['agent_level'] = $_POST['superior_id'] ? $this->agent_level($_POST['superior_id']) : '1';
            if ($_POST['agent_level'] > 3) {
                $this->error("添加失败,上级代理是3级，不能成为下级代理！");
            }
            $_POST['addtime'] = time();
            $result = DB::name('agent')->insertGetId($_POST);
            $data = array(
                'agent_id' => $result
            );
            DB::name('agent_information')->insertGetId($data);
            if ($result !== false) {
                $this->success("添加成功！", url("agent/index"));
            } else {
                $this->error("添加失败！");
            }
        }
    }

    public function agent_level($id)
    {
        $result = DB::name('agent')->where("id=" . $id)->find();
        if ($result) {
            $level = $result['agent_level'] + 1;

        } else {
            $level = '1';
        }
        return $level;
    }

    /**
     * 账号编辑
     */
    public function edit()
    {
        $id = $this->request->param('id', 0, 'intval');

        $user = DB::name('agent')->where(["id" => $id])->find();
        $this->assign($user);
        return $this->fetch();
    }

    /**
     * 账号编辑提交
     */
    public function editPost()
    {
        if ($this->request->isPost()) {

            if (empty($_POST['agent_pass'])) {
                unset($_POST['agent_pass']);
            } else {
                $_POST['agent_pass'] = cmf_password($_POST['agent_pass']);
            }
            $login = $_POST['agent_login'];
            $user = DB::name('agent')->where("agent_login ='$login' and id !=" . $_POST['id'])->find();
            if ($user) {
                $this->error("修改失败,账号已存在！");
            }
            $_POST['agent_level'] = $_POST['superior_id'] ? $this->agent_level($_POST['superior_id']) : '1';
            if ($_POST['agent_level'] > 3) {
                $this->error("添加失败,上级代理是3级，不能成为下级代理！");
            }
            $result = DB::name('agent')->update($_POST);
            if ($result !== false) {
                $this->success("保存成功！");
            } else {
                $this->error("保存失败！");
            }
        }
    }

    /**
     * 删除账号
     */
    public function delete()
    {
        $id = $this->request->param('id', 0, 'intval');

        if (Db::name('agent')->delete($id) !== false) {
            $this->success("删除成功！");
        } else {
            $this->error("删除失败！");
        }
    }

    /**
     * 停用账号
     */
    public function ban()
    {
        $id = $this->request->param('id', 0, 'intval');
        if (!empty($id)) {
            $result = Db::name('agent')->where(["id" => $id])->setField('status', '0');
            if ($result !== false) {
                $this->success("封号成功！", url("agent/index"));
            } else {
                $this->error('封号失败！');
            }
        } else {
            $this->error('数据传入失败！');
        }
    }

    /**
     * 启用账号
     */
    public function cancelBan()
    {
        $id = $this->request->param('id', 0, 'intval');
        if (!empty($id)) {
            $result = Db::name('agent')->where(["id" => $id])->setField('status', '1');
            if ($result !== false) {
                $this->success("解封成功！", url("agent/index"));
            } else {
                $this->error('解封失败！');
            }
        } else {
            $this->error('数据传入失败！');
        }
    }

    /*
     * 代理提现记录
     *
     * */
    public function withdrawal()
    {
        $where = '';
        /**搜索条件**/

        $p = $this->request->param('page');
        if (empty($p) and !$this->request->param('start_time') and !$this->request->param('end_time') and !$this->request->param('agent_id') and !$this->request->param('agent_level') and !$this->request->param('status')) {
            session("admin_withdrawal", null);
        } else if (empty($p)) {

            $data['start_time'] = $this->request->param('start_time');
            $data['end_time'] = $this->request->param('end_time');
            $data['agent_id'] = $this->request->param('agent_id');

            $data['agent_level'] = $this->request->param('agent_level');
            $data['status'] = $this->request->param('status');

            session("admin_withdrawal", $data);
        }

        $start_times = session("admin_withdrawal.start_time");
        $end_times = session("admin_withdrawal.end_time");
        $agentid = session("admin_withdrawal.agent_id");

        $agent_level = session("admin_withdrawal.agent_level");
        $status = session("admin_withdrawal.status");


        $start_time = $start_times ? strtotime($start_times) : '0';

        $end_time = $end_times ? strtotime($end_times) : time();

        $where['a.addtime'] = array('between', array($start_time, $end_time));

        if ($agent_level) {
            $where['u.agent_level'] = $agent_level;
        } else {
            $where['u.agent_level'] = 1;
        }
        if ($status != '-1' and $status != null) {
            $where['a.status'] = $status;
        }

        if ($agentid) {
            $where['u.id'] = $agentid;
        }

        $withdrawal = Db::name('agent_withdrawal')->alias("a")
            ->join("agent u", "u.id=a.agent_id")
            ->where($where)
            ->field("u.agent_login,u.agent_level,a.*")
            ->order("a.addtime DESC")
            ->paginate(10);
        $sum = Db::name('agent_withdrawal')->alias("a")
            ->join("agent u", "u.id=a.agent_id")
            ->where($where)
            ->order("a.addtime DESC")
            ->sum("money");
        $page = $withdrawal->render();
        $name = $withdrawal->toArray();

        $this->assign("sum", $sum);
        $this->assign("page", $page);
        $this->assign("list", $name['data']);
        $this->assign("data", session("admin_withdrawal"));
        return $this->fetch();
    }

    //修改提现状态
    public function addwithdrawal()
    {
        $id = $this->request->param('id');
        $status = $this->request->param('status');
        $data = array(
            'status' => $status
        );
        $result = Db::name('agent_withdrawal')->where("id=$id")->update($data);
        if ($result) {
            $this->success("操作成功！");
        } else {
            $this->success("操作失败！");
        }
    }

    //代理推广链接
    public function link()
    {
        $where = '';
        /**搜索条件**/


        $p = $this->request->param('page');
        if (empty($p) and !$this->request->param('agent_id') and !$this->request->param('channel') and $this->request->param('status') == null) {
            session("admin_link", null);
            session("admin_link.status", 1);
        } else if (empty($p)) {


            $data['agent_id'] = $this->request->param('agent_id');
            $data['channel'] = $this->request->param('channel');
            $data['status'] = $this->request->param('status');

            session("admin_link", $data);

        }


        $agentid = session("admin_link.agent_id");
        $channel = session("admin_link.channel");
        $status = session("admin_link.status");

        if ($agentid) {
            $where['a.id'] = $agentid;
        }
        if ($channel) {
            $where['l.channel'] = $channel;
        }
        if ($status >= 0) {
            $where['a.status'] = $status;
        }
        $users = Db::name('agent_link')->alias("l")
            ->join("agent a", "a.id=l.agent_id1")
            ->field("l.*,a.agent_login,a.superior_id,a.status")
            ->where($where)
            ->order("id DESC")
            ->paginate(10);

        // 获取分页显示
        $page = $users->render();
        $name = $users->toArray();

        foreach ($name['data'] as &$v) {

            $vid = $v['superior_id'];
            $users = Db::name('agent')->where("id=$vid")->find(); //上级代理
            if ($users) {
                $v['agent_user'] = $users['agent_login'];
                $v['agent_id'] = $users['id'];
            } else {
                $v['agent_user'] = '超级管理员';
                $v['agent_id'] = '1';
            }

        }

        $this->assign("page", $page);

        $this->assign("users", $name['data']);
        $this->assign("data", session("admin_link"));
        return $this->fetch();
    }

    //添加代理渠道
    public function addlink()
    {
        $id = $this->request->param('id');
        if ($id) {
            $name = Db::name('agent_link')->alias("a")
                ->join("agent u", "u.id=a.agent_id1")
                ->field("u.agent_login,a.*")
                ->where("a.id=$id")
                ->find();
        } else {
            $name = array();
            $link = Db::name('agent_link')->order("id desc")->find();   //获取渠道号
            if ($link) {
                $name['channel'] = $link['channel'] + rand(10, 99);
            } else {
                $name['channel'] = '5001';
            }
            $name['link'] = $_SERVER['HTTP_HOST'] . "/agent/public/admin/download/index/agent/" . $name['channel'];

            $name['agent_id1'] = '0';
        }

        $user = Db::name('agent')->where("status =1 and agent_level=1")->select();  //获取一级代理


        $this->assign("users", $name);

        $this->assign("type", $user);

        return $this->fetch();

    }

    //修改和添加渠道号
    public function link_upd()
    {

        $id = $this->request->param('id');
        $data = array(
            'channel' => $this->request->param('channel'),
            'link' => $this->request->param('link'),
            'agent_id1' => $this->request->param('agent_id1'),
            'divide_into1' => $this->request->param('divide_into1'),
        );

        if ($id) {

            $usertype = Db::name('agent_link')->where("id=$id")->update($data);

        } else {
            $data['addtime'] = time();
            $usertype = Db::name('agent_link')->insert($data);
        }
        if ($usertype) {

            $this->success("保存成功！");

        } else {

            $this->error("保存失败！");
        }
    }

    //删除渠道号
    public function deletelink()
    {
        $id = $this->request->param('id');
        if ($id) {
            $usertype = Db::name('agent_link')->where("id=$id")->delete();
            if ($usertype) {
                $this->success("删除成功！");
            } else {
                $this->error("删除失败！");
            }
        } else {
            $this->error("参数错误！");
        }
    }

    //注册详情
    public function registered()
    {
        /**搜索条件**/

        $p = $this->request->param('page');
        if (empty($p) and !$this->request->param('channel') and !$this->request->param('userid')) {
            session("admin_registered", null);
        } else if (empty($p)) {
            $data['channel'] = $this->request->param('channel');
            $data['userid'] = $this->request->param('userid');

            session("admin_registered", $data);
        }


        $userid = session("admin_registered.userid");
        $channel = session("admin_registered.channel");
        $where = "u.link_id !=0";
        if ($userid) {

            $where .= " and (a.agent_id1=$userid or a.agent_id2=$userid or a.agent_id3=$userid)";

        }

        if ($channel) {

            $where .= " and channel=$channel";

        }

        //渠道号
        $list = Db::name('agent_link')->alias("a")
            ->join("user u", "u.link_id=a.channel")
            ->where($where)
            ->field("u.*")
            ->order("u.create_time desc")
            ->paginate(10);

        // 获取分页显示
        $page = $list->render();
        $user = $list->toArray();

        foreach ($user['data'] as &$v) {
            $uid = $v['id'];
            $v['summoney'] = Db::name("agent_settlement")->where("uid=$uid")->sum("money");

        }
        $this->assign("page", $page);
        $this->assign("data", session("admin_registered"));
        $this->assign("users", $user['data']);
        return $this->fetch();
    }
}