<?php
/**
 * Created by PhpStorm.
 * User: weipeng
 * Date: 2018/3/19
 * Time: 8:59
 */

/*
 * 推送用户私照审核认证结果消息
 * */
function push_private_photo_auth_result_msg(){


}

/*
 * 推送所有人可以收到的系统消息
 * */
function push_msg($msg_id,$user_id,$type){

    $msg = db("user_message_all") -> find($msg_id);
    if($msg){
        $data = [
            'uid' => 0,
            'touid' => $user_id,
            'messageid' => $msg['id'],
            'messagetype' => $msg['centent'],
            'type' => $type,
            'status' => 1,
            'addtime' => NOW_TIME,
        ];
        return db('user_message_log') -> insert($data);
    }
    return false;
}


/*
 * 推送单用户系统消息
 * */
function push_msg_user($msg_id,$user_id,$type){

    $msg = db("user_message") -> find($msg_id);
    if($msg){
        $data = [
            'uid' => 0,
            'touid' => $user_id,
            'messageid' => $msg['id'],
            'messagetype' => $msg['centent'],
            'type' => $type,
            'status' => 1,
            'addtime' => NOW_TIME,
        ];
        return db('user_message_log') -> insert($data);
    }
    return false;
}

