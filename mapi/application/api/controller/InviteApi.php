<?php

namespace app\api\controller;
use app\api\controller\Base;
use think\helper\Time;

/**
 * Created by PhpStorm.
 * User: weipeng
 * Date: 2018/3/12
 * Time: 23:09
 */

class InviteApi extends Base{

    //获取邀请码和邀请记录
    public function get_my_invite_page(){

        $result = array('code' => 1,'msg' => '');
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));
        $page = intval(input('param.page'));

        if($uid == 0 || empty($token)){
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid,$token);

        if(!$user_info){
            $result['code'] = 10001;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        //查询邀请码
        $invite_code = db('invite_code') -> where('user_id','=',$uid) -> find();
        $invite_code = $invite_code['invite_code'];
        if(!$invite_code) {
            //生成邀请码
            $invite_code = create_invite_code();
            db('invite_code') -> insert(['user_id' => $uid,'invite_code' => $invite_code]);
        }

        //邀请码
        $result['invite_code'] = $invite_code;

        $result['invite_user_count'] = 0;
        $result['income_total'] = 0;
        $result['day_income_total'] = 0;

        //邀请记录
        $invite_user_list = db('invite_record') -> alias('i')
            -> field('u.avatar,u.id,u.user_nickname')
            -> join(config('database.prefix') . 'user u','i.invite_user_id=u.id')
            -> where('i.user_id','=',$uid)
            -> page($page,20)
            -> select();
        //echo db('invite_record') ->getLastSql();exit;

        foreach ($invite_user_list as &$v){
            //用户奖励总数
            $v['income_total'] = db('invite_profit_record') -> where('user_id','=',$uid) -> where('invite_user_id','=',$v['id']) -> sum('income');

        }

        //首页或者刷新进行统计数据查询
        if($page == 1){
            //总收益
            $income_total = db('invite_profit_record') -> where('user_id','=',$uid) -> sum('income');

            //今日收益
            $day_time = Time::today();
            $day_income_total = db('invite_profit_record')
                -> where('user_id','=',$uid)
                -> where('create_time','>',$day_time[0])
                -> where('create_time','<',$day_time[1])
                -> sum('income');

            //总邀请人数
            $invite_user_count = db('invite_record') -> where('user_id','=',$uid) -> count();
            $result['invite_user_count'] = $invite_user_count;
            $result['income_total'] = $income_total;
            $result['day_income_total'] = $day_income_total;
        }

        $result['invite_user_list'] = $invite_user_list;

        return_json_encode($result);
    }

    //是否填写过邀请码
    public function is_full_invite_code()
    {
        $result = array('code' => 1, 'msg' => '');
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token);

        if (!$user_info) {
            $result['code'] = 10001;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        $result['is_full'] = 0;
        //是否填写邀请码
        $record = db('invite_record')->where('invite_user_id', '=', $uid)->find();
        if($record){
            $result['is_full'] = 1;
        }

        return_json_encode($result);
    }

    //提交邀请码
    public function full_invite_code(){

        $result = array('code' => 1, 'msg' => '');
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));
        $invite_code = trim(input('param.invite_code'));
        $type = intval(input('param.type'));

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token);

        if (!$user_info) {
            $result['code'] = 10001;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        //查询是否填写过邀请码
        $record = db('invite_record')->where('invite_user_id', '=', $uid)->find();
        if($record){
            $result['code'] = 0;
            $record['msg'] = '已经填写过邀请码';
            return_json_encode($result);
        }

        //邀请码数据
        $invite_data = [
            'invite_user_id' => $uid,
            'create_time' => NOW_TIME,
        ];

        //判断邀请码是否存在
        if($type == 1){
            $invite_code = db('invite_code') -> where('invite_code','=',$invite_code) -> find();
            if(!$invite_code){
                $result['code'] = 0;
                $result['msg'] = '邀请码不存在';
                return_json_encode($result);
            }

            if($invite_code['user_id'] == $uid){
                $result['code'] = 0;
                $result['msg'] = '不能填写自己的邀请码';
                return_json_encode($result);
            }

            $invite_data['user_id'] = $invite_code['user_id'];
            $invite_data['invite_code'] = $invite_code['invite_code'];

        }else{

            $invite_data['user_id'] = 0;
            $invite_data['invite_code'] = '';
        }

        //添加邀请记录
        db('invite_record') -> insert($invite_data);

        return_json_encode($result);

    }

    //获取邀请码
    public function get_invite_code(){

        $result = array('code' => 1,'msg' => '');
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));

        if($uid == 0 || empty($token)){
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid,$token);

        if(!$user_info){
            $result['code'] = 10001;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        //查询邀请码
        $invite_code = db('invite_code') -> where('user_id','=',$uid) -> find();
        $invite_code = $invite_code['invite_code'];
        if(!$invite_code) {
            //生成邀请码
            $invite_code = create_invite_code();
            db('invite_code') -> insert(['user_id' => $uid,'invite_code' => $invite_code]);
        }

        //邀请码
        $result['invite_code'] = $invite_code;

        return_json_encode($result);

    }


}