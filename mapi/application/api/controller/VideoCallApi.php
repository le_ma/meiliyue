<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/1/25 0025
 * Time: 下午 16:58
 */

namespace app\api\controller;

use think\Db;
use VideoCallRedis;

//视频通话业务类
class VideoCallApi extends Base
{

    //一键约爱
    public function one_key_video_call()
    {

        $result = array('code' => 0, 'msg' => '', 'data' => array());
        $uid = intval(input('param.uid'));
        $token = input('param.token');

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token, ['income', 'coin_system', 'custom_video_charging_coin', 'is_use_free_time']);

        if (!$user_info) {
            $result['code'] = 10001;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        //账号是否被禁用
        if ($user_info['user_status'] == 0) {
            $result['code'] = 0;
            $result['msg'] = "您因涉嫌违规，账号受限，请联系管理员!";
            return_json_encode($result);
        }

        //随机查找在线主播进行视频通话
        $emcee_id = get_rand_emcee($uid);

        if (!$emcee_id) {

            $result['code'] = 0;
            $result['msg'] = '主播们还未上线，请耐心等待！';
            return_json_encode($result);
        }
        //echo $emcee_id;exit;
        //主播基本信息
        $emcee_user = get_user_base_info($emcee_id, ['custom_video_charging_coin']);

        $config = load_cache('config');
        //判断用户是否有一分钟免费机会
        if ($user_info['is_use_free_time'] != 0) {

            $coin = $user_info['coin'] + $user_info['coin_system'];
            $video_deduction = $config['video_deduction'];
            if (defined('OPEN_CUSTOM_VIDEO_CHARGE_COIN') && OPEN_CUSTOM_VIDEO_CHARGE_COIN == 1) {
                $level = get_level($emcee_id);
                $custom_video_charging_coin = $emcee_user['custom_video_charging_coin'];
                if ($level >= $config['custom_video_money_level'] && $custom_video_charging_coin != 0) {

                    $video_deduction = $custom_video_charging_coin;
                }
            }

            if ($coin < $video_deduction && $user_info['sex'] != 2) {
                $result['msg'] = "余额不足,请先充值";
                $result['code'] = 10002;
                return_json_encode($result);
            }
        }

        //通话频道ID
        $channel_id = NOW_TIME . $uid . mt_rand(0, 1000);

        $ext = array();
        $ext['type'] = 12;//type 12 一对一视频消息请求推送
        $sender['id'] = $uid;
        $sender['user_nickname'] = $user_info['user_nickname'];
        $sender['avatar'] = $user_info['avatar'];
        $ext['channel'] = $channel_id;//通话频道
        $ext['is_use_free'] = 0;
        if ($user_info['is_use_free_time'] == 0) {
            $ext['is_use_free'] = 1;
        }
        $ext['sender'] = $sender;

        //拨打记录
        $call_record['user_id'] = $uid;
        $call_record['call_be_user_id'] = $emcee_id;
        $call_record['channel_id'] = $channel_id;
        $call_record['status'] = 0;
        $call_record['create_time'] = NOW_TIME;
        $call_record['is_free'] = 1;
        if ($user_info['sex'] == 2) {
            $call_record['anchor_id'] = $uid;
        } else {
            $call_record['anchor_id'] = $emcee_id;
        }

        require_once DOCUMENT_ROOT . '/system/im_common.php';
        $ser = open_one_im_push($uid, $emcee_id, $ext);

        $data['channel_id'] = $channel_id;
        if ($ser['ActionStatus'] == 'OK') {
            //拨打记录
            Db::name("video_call_record")->insert($call_record);

            $result['code'] = 1;
            $result['data'] = $data;

        } else {
            $result['msg'] = "拨打视频失败!";
            $result['data'] = $data;
        }

        $result['data']['to_user_base_info'] = $emcee_user;

        // print_r($ser);exit;
        return_json_encode($result);


    }

    //拨打视频通话
    public function video_call()
    {

        $result = array('code' => 0, 'msg' => '', 'data' => array());
        $uid = intval(input('param.uid'));
        $token = input('param.token');
        $id = intval(input('param.id'));        //被呼叫人

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token, ['income', 'coin_system', 'custom_video_charging_coin']);

        if (!$user_info) {
            $result['code'] = 10001;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        //账号是否被禁用
        if ($user_info['user_status'] == 0) {
            $result['code'] = 0;
            $result['msg'] = "您因涉嫌违规，账号受限，请联系管理员!";
            return_json_encode($result);
        }

        //是否是自己
        if ($id == $uid) {
            $result['code'] = 0;
            $result['msg'] = "不能拨打自己";
            return_json_encode($result);
        }

        //是否是同性
        $to_user_info = get_user_base_info($id, ['custom_video_charging_coin,is_open_do_not_disturb'], 1);
        if ($user_info['sex'] == $to_user_info['sex']) {
            $result['code'] = 0;
            $result['msg'] = "同性之间不能通话";
            return_json_encode($result);
        }

        //判断对方是否开启勿扰
        if ($to_user_info['is_open_do_not_disturb'] == 1) {
            $result['code'] = 0;
            $result['msg'] = "对方开启了勿扰模式！";
            return_json_encode($result);
        }

        $config = load_cache('config');

//        if (!is_online($id, $config['heartbeat_interval'])) {
//            $result['code'] = 0;
//            $result['msg'] = '对方不在线';
//            return_json_encode($result);
//
//        }

        //是否认证
        if ($user_info['sex'] == 2) {

            if (get_user_auth_status($uid) != 1) {
                $result['code'] = 0;
                $result['msg'] = "未认证，无法发起视频通话！";
                return_json_encode($result);
            }

        } else {
            $to_user_auth_status = get_user_auth_status($id);
            if ($to_user_auth_status != 1) {
                $result['code'] = 0;
                $result['msg'] = "主播未认证，无法发起视频通话！";
                return_json_encode($result);
            }
        }

        //是否被对方拉黑
        $black = db('user_black')->where('user_id', '=', $id)->where('black_user_id', '=', $uid)->find();
        if ($black) {
            $result['code'] = 0;
            $result['msg'] = '您已被对方拉黑无法发起视频!';
            return_json_encode($result);
        }

        //检查是否已经存在通话记录
        $is_exits_call_record = db("video_call_record")->whereOr('user_id', '=', $id)->whereOr('anchor_id', '=', $id)->find();
        if ($is_exits_call_record) {
            $result['code'] = 0;
            $result['msg'] = '对方正在忙碌中!';
            return_json_encode($result);
        }

        $coin = $user_info['coin'] + $user_info['coin_system'];

        $emcee_id = $user_info['sex'] == 1 ? $id : $uid;
        $video_deduction = $config['video_deduction'];
        if (defined('OPEN_CUSTOM_VIDEO_CHARGE_COIN') && OPEN_CUSTOM_VIDEO_CHARGE_COIN == 1) {
            $level = get_level($emcee_id);
            $custom_video_charging_coin = $uid == $emcee_id ? $user_info['custom_video_charging_coin'] : $to_user_info['custom_video_charging_coin'];
            if ($level >= $config['custom_video_money_level'] && $custom_video_charging_coin != 0) {

                $video_deduction = $custom_video_charging_coin;
            }

        }

        if ($coin < $video_deduction && $user_info['sex'] != 2) {
            $result['msg'] = "余额不足,请先充值";
            $result['code'] = 10002;
            return_json_encode($result);
        }

        if ($to_user_info['coin'] < $video_deduction && $user_info['sex'] == 2) {
            $result['msg'] = "对方用户余额不足";
            $result['code'] = 0;
            return_json_encode($result);
        }

        //账号是否被禁用
        if ($to_user_info['user_status'] == 0) {
            $result['code'] = 0;
            $result['msg'] = "对方因涉嫌违规，账号受限，无法进行操作!";
            return_json_encode($result);
        }


        require_once DOCUMENT_ROOT . '/system/redis/VideoCallRedis.php';
        $video_call_redis = new VideoCallRedis();
        $redis_res = $video_call_redis->do_call($uid, $emcee_id);
        if ($redis_res == 10001) {

            $result['code'] = 0;
            $result['msg'] = '忙碌中,请稍后！';
            return_json_encode($result);
        }

        //检查是否已经存在通话记录
        $is_exits_call_record = db("video_call_record")->where('anchor_id', '=', $id)->find();
        if ($is_exits_call_record) {
            $result['code'] = 0;
            $result['msg'] = '对方正在忙碌中!';
            return_json_encode($result);
        }


        //通话频道ID
        $channel_id = NOW_TIME . $uid . mt_rand(0, 1000);

        $ext = array();
        $ext['type'] = 12;//type 12 一对一视频消息请求推送
        $sender['id'] = $uid;
        $sender['user_nickname'] = $user_info['user_nickname'];
        $sender['avatar'] = $user_info['avatar'];
        $ext['channel'] = $channel_id;//通话频道
        $ext['is_use_free'] = 0;
        $ext['sender'] = $sender;

        //拨打记录
        $call_record['user_id'] = $uid;
        $call_record['call_be_user_id'] = $id;
        $call_record['channel_id'] = $channel_id;
        $call_record['status'] = 0;
        $call_record['create_time'] = NOW_TIME;
        if ($user_info['sex'] == 2) {
            $call_record['anchor_id'] = $uid;
        } else {
            $call_record['anchor_id'] = $id;
        }

        require_once DOCUMENT_ROOT . '/system/im_common.php';
        $ser = open_one_im_push($uid, $id, $ext);

        $data['channel_id'] = $channel_id;
        if ($ser['ActionStatus'] == 'OK') {
            //拨打记录
            db("video_call_record")->insert($call_record);

            $result['code'] = 1;
            $result['data'] = $data;

        } else {
            $result['msg'] = "拨打视频失败!";
            $result['data'] = $data;
        }

        $result['data']['to_user_base_info'] = get_user_base_info($id);

        return_json_encode($result);
    }

    //拨打视频通话
    public function video_call_0907()
    {

        $result = array('code' => 0, 'msg' => '', 'data' => array());
        $uid = intval(input('param.uid'));
        $token = input('param.token');
        $id = intval(input('param.id'));        //被呼叫人

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token, ['income', 'coin_system', 'custom_video_charging_coin']);

        if (!$user_info) {
            $result['code'] = 10001;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        //账号是否被禁用
        if ($user_info['user_status'] == 0) {
            $result['code'] = 0;
            $result['msg'] = "您因涉嫌违规，账号受限，请联系管理员!";
            return_json_encode($result);
        }

        //是否是自己
        if ($id == $uid) {
            $result['code'] = 0;
            $result['msg'] = "不能拨打自己";
            return_json_encode($result);
        }

        //是否是同性
        $to_user_info = get_user_base_info($id, ['custom_video_charging_coin,is_open_do_not_disturb'], 0);
        if ($user_info['sex'] == $to_user_info['sex']) {
            $result['code'] = 0;
            $result['msg'] = "同性之间不能通话";
            return_json_encode($result);
        }

        //判断对方是否开启勿扰
        if ($to_user_info['is_open_do_not_disturb'] == 1) {
            $result['code'] = 0;
            $result['msg'] = "对方开启了勿扰模式！";
            return_json_encode($result);
        }

        $config = load_cache('config');

//        if (!is_online($id, $config['heartbeat_interval'])) {
//            $result['code'] = 0;
//            $result['msg'] = '对方不在线';
//            return_json_encode($result);
//
//        }

        //是否认证
        if ($user_info['sex'] == 2) {

            if (get_user_auth_status($uid) != 1) {
                $result['code'] = 0;
                $result['msg'] = "未认证，无法发起视频通话！";
                return_json_encode($result);
            }

        } else {
            $to_user_auth_status = get_user_auth_status($id);
            if ($to_user_auth_status != 1) {
                $result['code'] = 0;
                $result['msg'] = "主播未认证，无法发起视频通话！";
                return_json_encode($result);
            }
        }

        //是否被对方拉黑
        $black = db('user_black')->where('user_id', '=', $id)->where('black_user_id', '=', $uid)->find();
        if ($black) {
            $result['code'] = 0;
            $result['msg'] = '您已被对方拉黑无法发起视频!';
            return_json_encode($result);
        }

        //检查是否已经存在通话记录
        $is_exits_call_record = db("video_call_record")->whereOr('user_id', '=', $id)->whereOr('anchor_id', '=', $id)->find();
        if ($is_exits_call_record) {
            $result['code'] = 0;
            $result['msg'] = '对方正在忙碌中!';
            return_json_encode($result);
        }

        $coin = $user_info['coin'] + $user_info['coin_system'];

        $emcee_id = $user_info['sex'] == 1 ? $id : $uid;
        $video_deduction = $config['video_deduction'];
        if (defined('OPEN_CUSTOM_VIDEO_CHARGE_COIN') && OPEN_CUSTOM_VIDEO_CHARGE_COIN == 1) {
            $level = get_level($emcee_id);
            $custom_video_charging_coin = $uid == $emcee_id ? $user_info['custom_video_charging_coin'] : $to_user_info['custom_video_charging_coin'];
            if ($level >= $config['custom_video_money_level'] && $custom_video_charging_coin != 0) {

                $video_deduction = $custom_video_charging_coin;
            }
        }

        if ($coin < $video_deduction && $user_info['sex'] != 2) {
            $result['msg'] = "余额不足,请先充值";
            $result['code'] = 10002;
            return_json_encode($result);
        }

        if ($to_user_info['coin'] < $video_deduction && $user_info['sex'] == 2) {
            $result['msg'] = "对方用户余额不足";
            $result['code'] = 0;
            return_json_encode($result);
        }

        //账号是否被禁用
        if ($to_user_info['user_status'] == 0) {
            $result['code'] = 0;
            $result['msg'] = "对方因涉嫌违规，账号受限，无法进行操作!";
            return_json_encode($result);
        }


        require_once DOCUMENT_ROOT . '/system/redis/VideoCallRedis.php';
        $video_call_redis = new VideoCallRedis();
        $redis_res = $video_call_redis->do_call($uid, $emcee_id);
        if ($redis_res == 10001) {

            $result['code'] = 0;
            $result['msg'] = '忙碌中,请稍后！';
            return_json_encode($result);
        }

        //检查是否已经存在通话记录
        $is_exits_call_record = db("video_call_record")->where('anchor_id', '=', $id)->find();
        if ($is_exits_call_record) {
            $result['code'] = 0;
            $result['msg'] = '对方正在忙碌中!';
            return_json_encode($result);
        }


        //通话频道ID
        $channel_id = NOW_TIME . $uid . mt_rand(0, 1000);

        //拨打记录
        $call_record['user_id'] = $uid;
        $call_record['call_be_user_id'] = $id;
        $call_record['channel_id'] = $channel_id;
        $call_record['status'] = 0;
        $call_record['create_time'] = NOW_TIME;
        if ($user_info['sex'] == 2) {
            $call_record['anchor_id'] = $uid;
        } else {
            $call_record['anchor_id'] = $id;
        }

        $data['channel_id'] = $channel_id;
        //拨打记录
        db("video_call_record")->insert($call_record);

        $result['code'] = 1;
        $result['data'] = $data;
        $result['data']['to_user_base_info'] = get_user_base_info($id);

        return_json_encode($result);
    }

    //答复视频通话
    public function reply_video_call()
    {

        $result = array('code' => 1, 'msg' => '', 'data' => array());
        $uid = input('param.uid');
        $token = input('param.token');
        $id = intval(input('param.to_user_id'));//接收人
        $channel = input('param.channel');   //获取通道字符串
        $type = intval(input('param.type'));
        if ($id == 0) {
            intvals($id);
        }
        if ($type == 0) {
            intvals($type);
        }

        //查询是否存在通话记录
        $call_record = db('video_call_record')->where(['channel_id' => $channel])->find();
        if (!$call_record) {
            $result['code'] = 0;
            $result['msg'] = '通话记录不存在';
            return_json_encode($result);
        }

        $user = db('user')->where("id=$uid and token='$token'")->find();
        $ext = array();
        $sender['id'] = $uid;
        $sender['user_nickname'] = $user['user_nickname'];
        $sender['avatar'] = $user['avatar'];
        $ext['type'] = 13;
        $ext['channel'] = $channel;
        $ext['sender'] = $sender;
        $ext['reply_type'] = $type;

        if ($type == 1) {
            //$message_status = "接通";
            $change_data['status'] = 1;
            //修改通话状态
            Db::name('video_call_record')->where(['id' => $call_record['id']])->update($change_data);

        } else if ($type == 2) {
            //$message_status = "拒绝";
            $call_record['status'] = 2;
            $call_record['end_time'] = time();
            //拒绝接听电话删除通话记录
            db('video_call_record_log')->insert($call_record);
            db('video_call_record')->where(['id' => $call_record['id']])->delete();


            //删除拨打视频通话缓存记录
            require_once DOCUMENT_ROOT . '/system/redis/VideoCallRedis.php';
            $video_call_redis = new VideoCallRedis();
            $video_call_redis->del_call($call_record['anchor_id']);

        }

        require_once DOCUMENT_ROOT . '/system/im_common.php';
        $ser = open_one_im_push($uid, $id, $ext);

        if ($ser['ActionStatus'] == 'OK') {
            $result['data']['channel'] = $channel;
            $result['data']['type'] = $type;

        } else {
            $push_record['status'] = 2;
        }
        return_json_encode($result);
    }

    //答复视频通话
    public function reply_video_call_0907()
    {

        $result = array('code' => 1, 'msg' => '', 'data' => array());
        $uid = input('param.uid');
        $token = input('param.token');
        $id = intval(input('param.to_user_id'));//接收人
        $channel = input('param.channel');   //获取通道字符串
        $type = intval(input('param.type'));

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token, ['income', 'coin_system', 'custom_video_charging_coin']);

        if (!$user_info) {
            $result['code'] = 10001;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        //查询是否存在通话记录
        $call_record = db('video_call_record')->where(['channel_id' => $channel])->find();
        if (!$call_record) {
            $result['code'] = 0;
            $result['msg'] = '通话记录不存在';
            return_json_encode($result);
        }

        if ($type == 1) {
            //$message_status = "接通";
            $change_data['status'] = 1;
            //修改通话状态
            db('video_call_record')->where(['id' => $call_record['id']])->update($change_data);

        } else if ($type == 2) {
            //$message_status = "拒绝";
            $call_record['status'] = 2;
            $call_record['end_time'] = time();
            //拒绝接听电话删除通话记录
            db('video_call_record_log')->insert($call_record);
            db('video_call_record')->where(['id' => $call_record['id']])->delete();


            //删除拨打视频通话缓存记录
            require_once DOCUMENT_ROOT . '/system/redis/VideoCallRedis.php';
            $video_call_redis = new VideoCallRedis();
            $video_call_redis->del_call($call_record['anchor_id']);

        }

        $result['data']['to_user_id'] = $id;
        $result['data']['channel'] = $channel;
        $result['data']['type'] = $type;

        return_json_encode($result);
    }

    //取消视频电话
    public function hang_up_video_call()
    {

        $result = array('code' => 1, 'msg' => '', 'data' => array());
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token);

        if (!$user_info) {
            $result['code'] = 0;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        $call_record = Db::name('video_call_record')->where(['user_id' => $user_info['id']])->find();
        if ($call_record) {
            $call_record['status'] = 3;
            $call_record['end_time'] = NOW_TIME;
            //删除通话记录
            db('video_call_record')->where(['user_id' => $user_info['id']])->delete();
            //同步到日志
            db('video_call_record_log')->insert($call_record);
        }

        $ext['type'] = 13;//type 13 一对一视频消息挂断推送
        $sender['id'] = $uid;
        $sender['user_nickname'] = $user_info['user_nickname'];
        $sender['avatar'] = $user_info['avatar'];
        $ext['channel'] = $call_record['channel_id'];//通话频道
        $ext['sender'] = $sender;
        $ext['reply_type'] = 1;

        require_once DOCUMENT_ROOT . '/system/im_common.php';

        $ser = open_one_im_push($uid, $call_record['call_be_user_id'], $ext);

        $data['channel_id'] = $call_record['channel_id'];
        if ($ser['ActionStatus'] == 'OK') {

            $result['code'] = 1;
            $result['data'] = $data;

        } else {
            $result['msg'] = "挂断出现错误";
            $result['data'] = $data;
        }

        //删除拨打视频通话缓存记录
        require_once DOCUMENT_ROOT . '/system/redis/VideoCallRedis.php';
        $video_call_redis = new VideoCallRedis();
        $video_call_redis->del_call($call_record['anchor_id']);

        //Db::name('user_message_log') -> insert($push_record);              //推送记录

        return_json_encode($result);
    }

    //取消视频电话
    public function hang_up_video_call_0907()
    {

        $result = array('code' => 1, 'msg' => '', 'data' => array());
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token);

        if (!$user_info) {
            $result['code'] = 0;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        $call_record = db('video_call_record')->where(['user_id' => $user_info['id']])->find();
        if ($call_record) {
            $call_record['status'] = 3;
            $call_record['end_time'] = NOW_TIME;
            //删除通话记录
            db('video_call_record')->where(['user_id' => $user_info['id']])->delete();
            //同步到日志
            db('video_call_record_log')->insert($call_record);
        }

        $data['channel_id'] = $call_record['channel_id'];
        $result['code'] = 1;
        $result['data'] = $data;

        //删除拨打视频通话缓存记录
        require_once DOCUMENT_ROOT . '/system/redis/VideoCallRedis.php';
        $video_call_redis = new VideoCallRedis();
        $video_call_redis->del_call($call_record['anchor_id']);

        return_json_encode($result);
    }

    //结束视频通话
    public function end_video_call()
    {

        $result = array('code' => 1, 'msg' => '', 'data' => array());
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));
        $to_user_id = intval(input('param.to_user_id'));

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token);

        if (!$user_info) {
            $result['code'] = 0;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        //查询通话记录
        $video_call_record = Db::name('video_call_record')
            ->where(['user_id' => $user_info['id'], 'call_be_user_id' => $to_user_id])
            ->whereOr(['call_be_user_id' => $user_info['id'], 'user_id' => $to_user_id])
            ->find();

        if (!$video_call_record) {
            $result['code'] = 0;
            $result['msg'] = '操作失败';
            return_json_encode($result);
        }

        $video_call_record['end_time'] = NOW_TIME;
        $video_call_record['status'] = 3;
        //通话时长
        $video_call_record['call_time'] = $video_call_record['end_time'] - $video_call_record['create_time'];

        //删除通话记录，添加日志记录
        db('video_call_record')->delete($video_call_record['id']);
        db('video_call_record_log')->insert($video_call_record);


        //是否点过赞
        $fabulous_record = Db::name('user_fabulous_record')
            ->where('user_id', '=', $uid)
            ->where('to_user_id', '=', $to_user_id)
            ->find();

        $result['fabulous'] = 1;
        if (!$fabulous_record) {
            $result['fabulous'] = 0;
        }

        $ext['type'] = 14;//type 14 结束视频消息
        $sender['id'] = $uid;
        $sender['user_nickname'] = $user_info['user_nickname'];
        $sender['avatar'] = $user_info['avatar'];
        $ext['channel'] = $video_call_record['channel_id'];//通话频道
        $ext['sender'] = $sender;

        require_once DOCUMENT_ROOT . '/system/im_common.php';
        $ser = open_one_im_push($uid, $to_user_id, $ext);

        $data['channel_id'] = $video_call_record['channel_id'];
        if ($ser['ActionStatus'] == 'OK') {

            $result['code'] = 1;
            $result['data'] = $data;

        } else {

            $result['msg'] = "一对一挂断推送失败";
            $result['data'] = $data;
        }

        //删除拨打视频通话缓存记录
        require_once DOCUMENT_ROOT . '/system/redis/VideoCallRedis.php';
        $video_call_redis = new VideoCallRedis();
        $video_call_redis->del_call($video_call_record['anchor_id']);

        return_json_encode($result);
    }


    //结束视频通话
    public function end_video_call_0907()
    {

        $result = array('code' => 1, 'msg' => '', 'data' => array());
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));
        $to_user_id = intval(input('param.to_user_id'));

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token);

        if (!$user_info) {
            $result['code'] = 0;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        //查询通话记录
        $video_call_record = db('video_call_record')
            ->where(['user_id' => $user_info['id'], 'call_be_user_id' => $to_user_id])
            ->whereOr(['call_be_user_id' => $user_info['id'], 'user_id' => $to_user_id])
            ->find();

        if (!$video_call_record) {
            $result['code'] = 0;
            $result['msg'] = '操作失败';
            return_json_encode($result);
        }

        $video_call_record['end_time'] = NOW_TIME;
        $video_call_record['status'] = 3;
        //通话时长
        $video_call_record['call_time'] = $video_call_record['end_time'] - $video_call_record['create_time'];

        //删除通话记录，添加日志记录
        db('video_call_record')->delete($video_call_record['id']);
        db('video_call_record_log')->insert($video_call_record);


        //是否点过赞
        $fabulous_record = Db::name('user_fabulous_record')
            ->where('user_id', '=', $uid)
            ->where('to_user_id', '=', $to_user_id)
            ->find();

        $result['fabulous'] = 1;
        if (!$fabulous_record) {
            $result['fabulous'] = 0;
        }

        $data['channel_id'] = $video_call_record['channel_id'];
        $result['code'] = 1;
        $result['data'] = $data;

        //删除拨打视频通话缓存记录
        require_once DOCUMENT_ROOT . '/system/redis/VideoCallRedis.php';
        $video_call_redis = new VideoCallRedis();
        $video_call_redis->del_call($video_call_record['anchor_id']);

        return_json_encode($result);
    }


    //视频通话计时扣费
    public function video_call_time_charging()
    {

        $result = array('code' => 1, 'msg' => '');
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token, ['custom_video_charging_coin', 'is_use_free_time']);

        if (!$user_info) {
            $result['code'] = 0;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        //查询正在通话的记录
        $call_record = db('video_call_record')->where(['user_id' => $uid])->whereOr(['call_be_user_id' => $uid])->find();
        if (!$call_record) {
            $result['code'] = 10011;
            $result['msg'] = '通话记录不存在';
            return_json_encode($result);
        }

        $is_free = 0;
        //查询用户是否有免费试用
        if ($call_record['is_free'] == 1 && $user_info['is_use_free_time'] == 0) {
            $is_free = 1;
            //修改用户免费状态为已使用过
            db('user')->where('id', '=', $uid)->setField('is_use_free_time', 1);
        }

        $config = load_cache('config');

        $to_user_info = get_user_base_info($call_record['anchor_id'], ['custom_video_charging_coin'], 0);
        //系统默认扣费金额
        $charging_coin = $config['video_deduction'];
        //是否开启了自定义金额
        if (defined('OPEN_CUSTOM_VIDEO_CHARGE_COIN') && OPEN_CUSTOM_VIDEO_CHARGE_COIN == 1 && $is_free != 1) {

            $level = get_level($call_record['anchor_id']);
            if ($level >= $config['custom_video_money_level'] && $to_user_info['custom_video_charging_coin'] != 0) {

                $charging_coin = $to_user_info['custom_video_charging_coin'];
            }
        }
        $result['charging_coin'] = $charging_coin;

        // 启动事务
        Db::startTrans();
        try {
            $charging_coin_res = 0;
            //不免费扣除用户余额
            if ($is_free != 1) {
                $charging_coin_res = db('user')->where(['id' => $uid])->setDec('coin', $charging_coin);
            }

            if (!$is_free && !$charging_coin_res) {
                $result['msg'] = "余额不足！";
                $result['code'] = 10002;
            }

            //增加总消费记录
            if ($charging_coin_res) {

                //增加主播收益
                db('user')->where(['id' => $call_record['anchor_id']])->inc('income', $charging_coin)->inc('income_total', $charging_coin)->update();

                //查询用户是否需要续费
                $balance = $user_info['coin'] - $charging_coin;
                if ($balance <= $charging_coin) {

                    $result['msg'] = "余额不足，是否续费！";
                    $result['code'] = 10013;
                }

                //增加视频扣费记录
                $data = [
                    'user_id' => $uid,
                    'to_user_id' => $call_record['anchor_id'],
                    'coin' => $charging_coin,
                    'profit' => $charging_coin,
                    'create_time' => NOW_TIME,
                    'channel_id' => $call_record['channel_id'],
                ];

                db('video_charging_record')->insert($data);

                //增加总扣费记录
                $log_id = add_charging_log($uid, $call_record['anchor_id'], 4, $charging_coin, $call_record['id']);

                //增加邀请分成
                invite_back_now($charging_coin, $uid, $log_id);
                invite_back_now($charging_coin, $call_record['anchor_id'], $log_id);
            }

            // 提交事务
            Db::commit();
        } catch (\Exception $e) {

            //$result['msg'] = $e -> getMessage();
            $result['msg'] = "余额不足！" . $e->getMessage();
            $result['code'] = 10002;
            // 回滚事务
            Db::rollback();
        }

        return_json_encode($result);

    }


    //获取结束收益信息
    public function get_video_call_end_info()
    {

        $result = array('code' => 1, 'msg' => '', 'data' => array());
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));
        $channel_id = trim(input('param.channel_id'));

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token);

        if (!$user_info) {
            $result['code'] = 0;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        //查询通话记录
        $video_call_record = db('video_call_record_log')
            ->where(['channel_id' => $channel_id])
            ->find();

        //查询消费数量
        $result['data'] = 0;

        if ($uid == $video_call_record['anchor_id']) {

            $total_coin = db('video_charging_record')
                ->where('channel_id', '=', $video_call_record['channel_id'])
                ->where('to_user_id', '=', $user_info['id'])
                ->sum('profit');

            $gift_total = db('user_gift_log')
                ->where('channel_id', '=', $video_call_record['channel_id'])
                ->where('to_user_id', '=', $user_info['id'])
                ->sum('gift_coin');


            $result['data'] = $total_coin + $gift_total;
            $result['is_follow'] = 1;
            //是否点过赞
        } else if ($uid == $video_call_record['user_id'] || $uid == $video_call_record['call_be_user_id']) {

            $total_coin = Db::name('video_charging_record')
                ->where('channel_id', '=', $video_call_record['channel_id'])
                ->where('user_id', '=', $user_info['id'])
                ->sum('coin');

            $gift_total = db('user_gift_log')
                ->where('channel_id', '=', $video_call_record['channel_id'])
                ->where('user_id', '=', $user_info['id'])
                ->sum('gift_coin');

            $result['data'] = $total_coin + $gift_total;

            $attention = db('user_attention')->where("uid=$uid and attention_uid=" . $video_call_record['anchor_id'])->find();
            $result['is_follow'] = 0;
            if ($attention) {
                $result['is_follow'] = 1;
            }
        }

        return_json_encode($result);

    }

    //定时查看是否通话超时
    public function check_time_out()
    {

        $result = array('code' => 1, 'msg' => '', 'data' => array());
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));
        $channel_id = trim(input('param.channel_id'));

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token);

        if (!$user_info) {
            $result['code'] = 0;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        //查询视频记录
        $config = load_cache('config');
        $video_record = db('video_call_record')->where('channel_id', '=', $channel_id)->find();
        $time = NOW_TIME - $video_record['create_time'];
        if ($time > $config['video_call_time_out']) {
            //超时
            $result['status'] = 4;
        } else {
            $result['status'] = 0;
        }

        return_json_encode($result);
    }

    //是否需要扣费
    public function is_need_charging()
    {

        $result = array('code' => 1, 'msg' => '');
        $uid = intval(input('param.uid'));
        $to_user_id = intval(input('param.to_user_id'));

        $user_info = get_user_base_info($uid);

        $result['is_need_charging'] = 0;
        if ($user_info['sex'] == 1) {
            $result['is_need_charging'] = 1;
        }

        $config = load_cache('config');
        $result['video_deduction'] = $config['video_deduction'];

        $emcee_id = $user_info['sex'] == 1 ? $to_user_id : $uid;
        $emcee_info = get_user_base_info($emcee_id, ['custom_video_charging_coin']);
        if (defined('OPEN_CUSTOM_VIDEO_CHARGE_COIN') && OPEN_CUSTOM_VIDEO_CHARGE_COIN == 1) {
            $emcee_level = get_level($emcee_id);
            //判断用户等级是否符合规定
            if ($emcee_level >= $config['custom_video_money_level'] && $emcee_info['custom_video_charging_coin'] != 0) {
                $result['video_deduction'] = $emcee_info['custom_video_charging_coin'];
            }
        }

        return_json_encode($result);
    }

    //通话满意度点赞
    public function video_fabulous()
    {

        $result = array('code' => 1, 'msg' => '', 'data' => array());
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));
        $channel_id = trim(input('param.channel_id'));

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token);

        if (!$user_info) {
            $result['code'] = 0;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        $record = db('video_call_record_log')->where('channel_id', '=', $channel_id)->find();
        if (!$record) {

            return_json_encode($result);
        }

        db('video_call_record_log')->where('channel_id', '=', $channel_id)->setField('is_fabulous', 1);

        return_json_encode($result);
    }
}






























