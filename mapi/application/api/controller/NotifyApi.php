<?php
/**
 * Created by PhpStorm.
 * User: weipeng
 * Date: 2018/3/29
 * Time: 11:12
 */

namespace app\api\controller;


use alipay_app_pay;
use kuaijiealipay_app_pay;
use qianyingalipay_app_pay;
use qianyingnewalipay_app_pay;
use wechat_app_pay;

class NotifyApi
{
    //千应支付回调
    public function qianying_notify(){

        require_once DOCUMENT_ROOT."/system/pay_class/qianyingalipay_app_pay_menu.php";
        $o = new qianyingalipay_app_pay();
        file_put_contents(DOCUMENT_ROOT."/public/ealipay_".date("Y-m-dHis").".txt",print_r($_REQUEST,true));
        $o->notify($_REQUEST);
    }


    //快接支付回调
    public function kuaijie_notify(){

        require_once DOCUMENT_ROOT."/system/pay_class/kuaijiealipay_app_pay_menu.php";
        $o = new kuaijiealipay_app_pay();
        file_put_contents(DOCUMENT_ROOT."/public/ealipay_".date("Y-m-dHis").".txt",print_r($_REQUEST,true));
        $o->notify($_REQUEST);
    }

    //官方支付宝回调
    public function alipay_notify(){

        require_once DOCUMENT_ROOT."/system/pay_class/alipay_app_pay_menu.php";
        $o = new alipay_app_pay();
        file_put_contents(DOCUMENT_ROOT."/public/ealipay_".date("Y-m-dHis").".txt",print_r($_REQUEST,true));
        $o->notify($_REQUEST);
    }
    //官方微信回调
    public function wechatpay_notify(){

        require_once DOCUMENT_ROOT."/system/pay_class/wechat_app_pay_menu.php";
        $o = new wechat_app_pay();
        file_put_contents(DOCUMENT_ROOT."/public/ealipay_".date("Y-m-dHis").".txt",print_r($_REQUEST,true));
        $o->notify($_REQUEST);
    }

    public function qianyingnew_notify(){
        require_once DOCUMENT_ROOT."/system/pay_class/qianyingnewalipay_app_pay_menu.php";
        $o = new qianyingnewalipay_app_pay();
        file_put_contents(DOCUMENT_ROOT."/public/ealipay_".date("Y-m-dHis").".txt",print_r($_REQUEST,true));
        $o->notify($_REQUEST);
    }
}