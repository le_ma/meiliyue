<?php
/**
 * Created by PhpStorm.
 * User: weipeng
 * Date: 2018/3/10
 * Time: 14:32
 */

namespace app\api\controller;


class DealApi extends Base
{

    //私聊付费
    public function request_private_chat_pay()
    {

        $result = array('code' => 1, 'msg' => '');

        $uid = input('param.uid');
        $token = input('param.token');
        $to_user_id = input('param.to_user_id');

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token);

        if (!$user_info) {
            $result['code'] = 0;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        //账号是否被禁用
        if ($user_info['user_status'] == 0) {
            $result['code'] = 0;
            $result['msg'] = "您因涉嫌违规，账号受限，请联系管理员!";
            return_json_encode($result);
        }


        $config = load_cache('config');
        $charging_coin = $config['private_chat_money'];
        // 启动事务
        db()->startTrans();
        try {
            $charging_coin_res = db('user')->where(['id' => $uid])->setDec('coin', $charging_coin);

            if ($charging_coin_res) {
                //增加主播收益
                db('user')->where(['id' => $to_user_id])->inc('income', $charging_coin)->inc('income_total', $charging_coin)->update();

                //增加私信付费记录
                $private_chat_log = [
                    'user_id' => $uid,
                    'to_user_id' => $to_user_id,
                    'coin' => $charging_coin,
                    'create_time' => NOW_TIME,

                ];
                $table_id = db('user_private_chat_log')->insert($private_chat_log);

                //增加总消费记录
                $log_id = add_charging_log($uid, $to_user_id, 5, $charging_coin, $table_id);

                //增加邀请分成
                invite_back_now($charging_coin, $uid, $log_id);
                //增加邀请分成主播
                invite_back_now($charging_coin, $to_user_id, $log_id);
            } else {

                $result['msg'] = "余额不足";
                $result['code'] = 10002;
            }

            // 提交事务
            db()->commit();
        } catch (\Exception $e) {

            $result['msg'] = "余额不足";
            $result['code'] = 10002;
            // 回滚事务
            db()->rollback();
        }

        return_json_encode($result);

    }

    //赠送礼物
    public function send_gift()
    {

        $result = array('code' => 1, 'msg' => '');
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));
        $to_user_id = intval(input('param.to_user_id'));
        $channel = trim(input('param.channel'));

        $gid = intval(input('param.gid'));
        $count = intval(input('param.count'));

        if ($count == 0) {
            $result['code'] = 0;
            $result['msg'] = '礼物数量必须大于1';
            return_json_encode($result);
        }

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token);

        if (!$user_info) {
            $result['code'] = 10001;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        //账号是否被禁用
        if ($user_info['user_status'] == 0) {
            $result['code'] = 0;
            $result['msg'] = "您因涉嫌违规，账号受限，请联系管理员!";
            return_json_encode($result);
        }


        $gift = load_cache('gift_id', ['id' => $gid]);
        if (!$gift) {
            $result['code'] = 0;
            $result['msg'] = '礼物信息不存在';
            return_json_encode($result);
        }

        $charging_coin = $count * $gift['coin'];
        // 启动事务
        db()->startTrans();
        try {
            $charging_coin_res = db('user')->where(['id' => $uid])->setDec('coin', $charging_coin);

            if ($charging_coin_res) {
                //增加主播收益
                db('user')->where(['id' => $to_user_id])->inc('income', $charging_coin)->inc('income_total', $charging_coin)->update();

//                $data = [
//                    'user_id' => $uid,
//                    'to_user_id' => $to_user_id,
//                    'coin' => $charging_coin,
//                    'profit' => $charging_coin,
//                    'create_time' => NOW_TIME,
//                    'channel_id' => $channel,
//                ];
//
//                //增加扣费记录
//                db('video_charging_record') -> insert($data);

                //增加送礼物记录
                $gift_log = [
                    'user_id' => $uid,
                    'to_user_id' => $to_user_id,
                    'gift_id' => $gift['id'],
                    'gift_name' => $gift['name'],
                    'gift_count' => $count,
                    'channel_id' => $channel,
                    'gift_coin' => $charging_coin,
                    'create_time' => NOW_TIME,

                ];
                $table_id = db('user_gift_log')->insert($gift_log);

                //增加总消费记录
                $log_id = add_charging_log($uid, $to_user_id, 3, $charging_coin, $table_id);

                //增加邀请分成
                invite_back_now($charging_coin, $uid, $log_id);
                //增加邀请分成主播
                invite_back_now($charging_coin, $to_user_id, $log_id);

                $result['send'] = $this->deal_send($uid, $to_user_id, $count, $channel, $user_info, $gift);
            } else {

                $result['msg'] = "余额不足";
                $result['code'] = 10002;
            }

            // 提交事务
            db()->commit();
        } catch (\Exception $e) {

            $result['msg'] = "余额不足";
            $result['code'] = 10002;
            // 回滚事务
            db()->rollback();
        }

        return_json_encode($result);

    }


    public function deal_send($user_id, $to_user_id, $num, $channel, $user_info, $gift)
    {


        $total_coin = $gift['coin'] * $num;
        //$root['from_msg'] = $user_info['user_nickname'] . "送给你 " . $num . "个" . $gift['name'];
        $root['from_msg'] = "送出 " . $num . "个" . $gift['name'];
        $root['from_score'] = "你的经验值+" . $total_coin;
        $root['to_ticket'] = intval($total_coin);
        $root['to_diamonds'] = $gift['coin'];//可获得的：钻石数；只有红包时，才有
        $root['to_user_id'] = $to_user_id;
        $root['prop_icon'] = $gift['img'];
        $root['status'] = 1;
        $root['prop_id'] = $gift['id'];
        $root['to_msg'] = "收到" . $num . "个" . $gift['name'] . ",获得" . $total_coin . '积分' . ",可以去个人主页>我的收益 查看哦";
        $root['total_ticket'] = 0;//用户总的：印票数

        return $root;
    }
}