<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:49:"themes/admin_simpleboot3/admin/agent/addlink.html";i:1532330136;s:43:"themes/admin_simpleboot3/public/header.html";i:1519953092;}*/ ?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="__TMPL__/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="__TMPL__/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="__STATIC__/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "__ROOT__/",
            WEB_ROOT: "__WEB_ROOT__/",
            JS_ROOT: "static/js/",
            APP: '<?php echo \think\Request::instance()->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="__TMPL__/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="__STATIC__/js/layer/layer.js" rel="stylesheet"></script>
    <script src="__STATIC__/js/wind.js"></script>
    <script src="__TMPL__/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip();
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
</head>
<body>
<div class="wrap">
    <ul class="nav nav-tabs">
        <li><a href="<?php echo url('agent/link'); ?>">推广链接</a></li>
        <li class="active"><a href="#">修改下级代理</a></li>
    </ul>
    <form class="form-horizontal js-ajax-form margin-top-20" role="form" method="post" action="<?php echo url('agent/link_upd'); ?>">
        <div class="form-group">
            <label for="input-user-nickname" class="col-sm-2 control-label">渠道号</label>
            <div class="col-md-6 col-sm-10">
                <input type="text" class="form-control" id="input-user-nickname" name="channel" value="<?php echo (isset($users['channel']) && ($users['channel'] !== '')?$users['channel']:''); ?>" readOnly="true">
            </div>
        </div>
        <div class="form-group">
            <label for="input-user-nickname" class="col-sm-2 control-label">推广地址</label>
            <div class="col-md-6 col-sm-10">
                <input type="text" class="form-control" id="input-user-link" name="link" value="<?php echo (isset($users['link']) && ($users['link'] !== '')?$users['link']:''); ?>" readOnly="true">
            </div>
        </div>


        <div class="form-group">
            <label for="input-user-nickname" class="col-sm-2 control-label">一级代理ID</label>
            <div class="col-md-6 col-sm-10">
                <select name="agent_id1" id="gift" style="width: 30%;height: 35px; border-color: #dce4ec; color: #a5b6c6;">
                    <?php if(is_array($type) || $type instanceof \think\Collection || $type instanceof \think\Paginator): if( count($type)==0 ) : echo "" ;else: foreach($type as $key=>$v): ?>
                        <option value="<?php echo $v['id']; ?>" <?php if($users['agent_id1'] == $v['id']): ?> selected="delected" <?php endif; ?> ><?php echo $v['agent_login']; ?> ( <?php echo $v['id']; ?> )</option>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                </select>

            </div>
        </div>
        <div class="form-group">
            <label for="input-user-nickname" class="col-sm-2 control-label">分成（%）</label>
            <div class="col-md-6 col-sm-10">
                <input type="text" class="form-control" id="input-user-wx" name="divide_into1" value="<?php echo (isset($users['divide_into1']) && ($users['divide_into1'] !== '')?$users['divide_into1']:''); ?>" placeholder="请输入分成，如 50 则是 50%">
            </div>
        </div>
        <input type="hidden" name="id" value="<?php echo (isset($users['id']) && ($users['id'] !== '')?$users['id']:''); ?>"/>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary js-ajax-submit"><?php echo lang('SAVE'); ?></button>
            </div>
        </div>
    </form>
</div>
<script src="__STATIC__/js/admin.js"></script>
</body>
</html>