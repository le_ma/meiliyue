<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/1/20 0020
 * Time: 上午 11:02
 */

namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;
use app\admin\model\AdminMenuModel;

class LevelController extends AdminBaseController
{
    /**
     * 等级列表
     */
    public function level_index()
    {
        $level = Db::name("level")->select();
        $this->assign('level', $level);
        return $this->fetch();
    }

    /**
     * 等级添加
     */
    public function add()
    {
        $id = input('param.id');
        if ($id) {
            $name = Db::name("level")->where("levelid=$id")->find();
            $this->assign('level', $name);
        }
        return $this->fetch();
    }

    public function addPost()
    {
        $param = $this->request->param();
        $id = $param['levelid'];
        $data = $param['post'];
        $data['addtime'] = time();
        if ($id) {
            $result = Db::name("level")->where("levelid=$id")->update($data);
        } else {
            $result = Db::name("level")->insert($data);
        }
        if ($result) {
            $this->success("保存成功", url('level/index'));
        } else {
            $this->error("保存失败");
        }
    }

    //删除
    public function del()
    {
        $param = request()->param();
        $result = Db::name("level")->where("levelid=" . $param['id'])->delete();
        return $result ? '1' : '0';
        exit;
    }

    public function star_index()
    {

        $list = db('user_star_level')->select();

        $this->assign('list', $list);

        return $this->fetch();
    }

    public function add_star()
    {

        return $this->fetch();
    }

    public function add_star_post()
    {

        $param = $this->request->param();

        $param['create_time'] = time();
        $result = db("user_star_level")->insert($param);

        if ($result) {
            $this->success("保存成功", url('level/star_index'));
        } else {
            $this->error("保存失败");
        }
    }

    public function edit_star()
    {

        $id = input('param.id');
        $data = db("user_star_level")->find($id);

        $this->assign('level', $data);
        return $this->fetch();

    }

    //编辑星级
    public function edit_star_post()
    {

        $id = input('param.id');

        $param = $this->request->param();

        $result = db("user_star_level")->where('id', '=', $id)->update($param);

        if ($result) {
            $this->success("保存成功", url('level/star_index'));
        } else {
            $this->error("保存失败");
        }
    }

}
