<?php

namespace app\api\controller;

use system\Tencent;
use think\Db;

class LoginApi extends Base
{

    //三方登录
    public function auth_login()
    {

        $result = array('code' => 0, 'msg' => '', 'data' => array());
        $plat_id = trim(input('param.plat_id'));
        $invite_code = trim(input('param.invite_code'));

        $user_info = db('user')->where("plat_id='$plat_id'")->find();

        //登录
        if ($user_info) {
            //生成token
            $token = md5($user_info['id'] . NOW_TIME . '3DW123@#$$$$@@');
            //更新的信息
            $data = array('token' => $token, 'last_login_time' => NOW_TIME);
            $update_res = db('user')->where("id=" . $user_info['id'])->update($data);

            if (!$update_res) {
                $result['code'] = 0;
                $result['msg'] = "登录失败!";
            }

            if ($user_info['user_status'] != 0) {
                $result['code'] = 1;
                $result['msg'] = "登录成功!";

                $result['data'] = array(
                    'id' => $user_info['id'],
                    'token' => $token,
                    'sex' => $user_info['sex'],
                    'user_nickname' => $user_info['user_nickname'],
                    'avatar' => $user_info['avatar'],
                    'address' => $user_info['address'],
                    'is_reg_perfect' => $user_info['is_reg_perfect'],
                );

                $signature = load_cache('usersign', ['id' => $user_info['id']]);
                if ($signature['status'] != 1) {
                    $result['code'] = 0;
                    $result['msg'] = $signature['error'];
                    return_json_encode($result);
                }

                require_once DOCUMENT_ROOT . '/meiliyue/system/im_common.php';
                update_im_user_info($user_info['id']);

                $result['data']['user_sign'] = $signature['usersign'];
            } else {
                if ($user_info['user_status'] == 0) {
                    $result['msg'] = "用户禁止登陆!";
                } else {
                    $result['msg'] = "您的账号已拉黑!";
                }
            }

        } else {

            //注册
            $id = db('mb_user')->insertGetId(array('plat_id' => $plat_id));
            $token = md5($id . NOW_TIME . '3DW123@#$$$$@@');
            $data = array(
                'id' => $id,
                'user_type' => 2,
                'user_nickname' => '新注册用户-' . rand(88888, 99999),
                'create_time' => NOW_TIME,
                'last_login_time' => NOW_TIME,
                'sex' => 0,
                'avatar' => SITE_URL . '/image/headicon.png',
                'token' => $token,
                'address' => "外太空",
                'plat_id' => $plat_id,
                'login_type' => 1,
            );

            $reg_result = db('user')->insert($data);
            if ($reg_result) {
                $result['data'] = array(
                    'id' => $id,
                    'token' => $token,
                    'is_reg_perfect' => 0
                );

                $signature = load_cache('usersign', ['id' => $id]);
                if ($signature['status'] != 1) {
                    $result['code'] = 0;
                    $result['msg'] = $signature['error'];
                    return_json_encode($result);
                }

                //添加邀请码
                create_invite_code_0910($id);

                //注册邀请奖励业务
                reg_invite_service($id, $invite_code);

                require_once DOCUMENT_ROOT . '/meiliyue/system/im_common.php';
                update_im_user_info($user_info['id']);

                $result['data']['user_sign'] = $signature['usersign'];

                $result['code'] = 1;
                $result['msg'] = "注册成功!";
            } else {
                $result['code'] = 0;
                $result['msg'] = "注册失败，请重新注册！";
            }
        }
        return_json_encode($result);
    }


    //登陆注册
    public function do_login()
    {
        $result = array('code' => 0, 'msg' => '', 'data' => array());

        $mobile = input('param.mobile');
        $code = intval(input('param.code'));
        $address = trim(input('param.address'));
        $invite_code = trim(input('param.invite_code'));

        if (!is_numeric($mobile) || strlen($mobile) != 11) {

            $result['code'] = 0;
            $result['msg'] = '参数错误';
        }

        if ($code == 0) {
            $result['code'] = 0;
            $result['msg'] = '验证码错误';
        }

        $config = load_cache('config');

        /*
         * 苹果上架审核
         * */
        if ($mobile == '13444444444' && $code == 111111 && $config['is_grounding'] == 1) {
            $ver = 1;
        } else {
            $ver = db('verification_code')->where("code='$code' and account='$mobile' and expire_time > " . NOW_TIME)->find();
        }
        if (!$ver) {

            $result['code'] = 0;
            $result['msg'] = "验证码错误，请重新获取！";
            return_json_encode($result);

        } else {

            $user_info = db('user')->where("mobile='$mobile'")->find();

            //登录
            if ($user_info) {
                //生成token
                $token = md5($user_info['id'] . NOW_TIME . '3DW123@#$$$$@@');
                //更新的信息
                $data = array('token' => $token, 'last_login_time' => NOW_TIME);
                $update_res = db('user')->where("id=" . $user_info['id'])->update($data);

                if (!$update_res) {
                    $result['code'] = 0;
                    $result['msg'] = "登录失败!";
                }

                if ($user_info['user_status'] != 0) {
                    $result['code'] = 1;
                    $result['msg'] = "登录成功!";

                    $result['data'] = array(
                        'id' => $user_info['id'],
                        'token' => $token,
                        'sex' => $user_info['sex'],
                        'user_nickname' => $user_info['user_nickname'],
                        'avatar' => $user_info['avatar'],
                        'address' => $user_info['address'],
                        'is_reg_perfect' => $user_info['is_reg_perfect'],
                    );

                    $signature = load_cache('usersign', ['id' => $user_info['id']]);
                    if ($signature['status'] != 1) {
                        $result['code'] = 0;
                        $result['msg'] = $signature['error'];
                        return_json_encode($result);
                    }

                    require_once DOCUMENT_ROOT . '/meiliyue/system/im_common.php';
                    update_im_user_info($user_info['id']);

                    $result['data']['user_sign'] = $signature['usersign'];
                } else {
                    if ($user_info['user_status'] == 0) {
                        $result['msg'] = "用户禁止登陆!";
                    } else {
                        $result['msg'] = "您的账号已拉黑!";
                    }
                }

            } else {

                //检查IP是否超过注册量
                $client_ip = request()->ip();
                $ip_log = db('ip_reg_log')->where(['ip' => $client_ip])->find();
                if ($ip_log && $ip_log['count'] >= IP_REG_MAX_COUNT) {
                    $result['code'] = 0;
                    $result['msg'] = "IP注册量超过限制！";
                    return_json_encode($result);
                }

                //  注册
                $id = db('mb_user')->insertGetId(array('mobile' => $mobile));
                $token = md5($id . NOW_TIME . '3DW123@#$$$$@@');
                $data = array(
                    'id' => $id,
                    'user_type' => 2,
                    'user_nickname' => '新注册用户-' . rand(88888, 99999),
                    'create_time' => NOW_TIME,
                    'last_login_time' => NOW_TIME,
                    'sex' => 0,
                    'avatar' => SITE_URL . '/image/headicon.png',
                    'mobile' => $mobile,
                    'token' => $token,
                    'address' => $address ? $address : "外太空",
                );

                $reg_result = db('user')->insert($data);

                if ($reg_result) {
                    $result['data'] = array(
                        'id' => $id,
                        'token' => $token,
                        'is_reg_perfect' => 0
                    );

                    $signature = load_cache('usersign', ['id' => $id]);
                    if ($signature['status'] != 1) {
                        $result['code'] = 0;
                        $result['msg'] = $signature['error'];
                        return_json_encode($result);
                    }
                    $result['data']['user_sign'] = $signature['usersign'];

                    //增加注册IP记录
                    if ($ip_log) {
                        db('ip_reg_log')->where(['ip' => $client_ip])->setInc('count', 1);
                    } else {
                        db('ip_reg_log')->insert(['ip' => $client_ip, 'count' => 1]);
                    }

                    //添加邀请码
                    create_invite_code_0910($id);
                    reg_invite_service($id, $invite_code);

                    require_once DOCUMENT_ROOT . '/meiliyue/system/im_common.php';
                    update_im_user_info($id);

                    $result['code'] = 1;
                    $result['msg'] = "注册成功!";
                } else {
                    $result['code'] = 0;
                    $result['msg'] = "注册失败，请重新注册！";
                }
            }
        }
        return_json_encode($result);
    }

    //完善注册信息
    public function perfect_reg_info()
    {

        $result = array('code' => 0, 'msg' => '');
        $one = request()->file('avatar');   //获取头像
        $sex = intval(request()->post('sex'));
        $user_nickname = trim(request()->post('user_nickname'));
        $uid = request()->post('uid');
        $address = trim(request()->post('address'));
        $token = trim(request()->post('token'));

        $user_info = check_token($uid, $token);

        if (!$user_info) {
            $result['code'] = 0;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        if ($sex == 0) {
            $result['msg'] = "请选择性别";
            return_json_encode($result);
        }
        if (empty($user_nickname)) {
            $result['msg'] = "用户名不能为空";
            return_json_encode($result);
        }

        if ($one) {
            $upload_one = oss_upload($one);      //单图片上传
        } else {
            $result['msg'] = "请上传头像";
            return_json_encode($result);
        }
        $exits_name = db('user')->where("user_nickname='$user_nickname'")->find();
        if ($exits_name) {
            $result['code'] = 0;
            $result['msg'] = "用户名重复，请重新输入用户名";
            return_json_encode($result);
        }

        $config = load_cache('config');

        //注册赠送积分或钻石
        $give_coin = $config['system_coin_registered'];
        $give_income = $config['system_coin_registered_women'];
        if ($sex == 1) {
            $give_income = 0;
        } else {
            $give_coin = 0;
        }

        $data = array(
            'user_nickname' => $user_nickname,
            'sex' => $sex == 1 ? $sex : 2,
            'avatar' => $upload_one,
            'address' => $address ? $address : "外太空",
            'is_reg_perfect' => 1,
            'income' => $give_income,
            'income_total' => $give_income,
            'coin' => $give_coin,

        );

        $update_res = db('user')->where("id=$uid and token='$token'")->update($data);

        if ($update_res) {
            reg_invite_perfect_info_service($uid, $sex);

            $result['data'] = $data;
            $result['code'] = 1;
            $result['msg'] = "信息保存成功!";
        }
        return_json_encode($result);
    }

    //分享注册
    public function share_reg()
    {

        $invite_code = input('param.invite_code');

        $this->assign('invite_code', $invite_code);
        return $this->fetch();
    }

    //分享注册账号
    public function share_reg_insert()
    {
        $result = array('code' => 0, 'msg' => '', 'data' => array());

        $mobile = input('param.mobile');
        $code = intval(input('param.code'));
        $invite_code = trim(input('param.invite_code'));

        $config = load_cache('config');
        $ver = db('verification_code')->where("code='$code' and account='$mobile' and expire_time > " . NOW_TIME)->find();
        if (!$ver) {
            $result['code'] = 0;
            $result['msg'] = "验证码错误，请重新获取！";
        } else {

            if (strlen($invite_code) != 0) {
                $invite_code_info = db('invite_code')->where('invite_code', '=', $invite_code)->find();
                if (!$invite_code_info) {
                    $result['code'] = 0;
                    $result['msg'] = '邀请码不存在';
                    return_json_encode($result);
                }

                $invite_data['user_id'] = $invite_code_info['user_id'];
                $invite_data['invite_code'] = $invite_code_info['invite_code'];
            }

            $sdk_app_id = $config['tencent_sdkappid'];
            $user_info = db('user')->where("mobile='$mobile'")->find();

            if ($user_info) {
                $result['code'] = 0;
                $result['msg'] = '该手机已被注册!';
                return_json_encode($result);
            }
            //  注册
            $id = db('mb_user')->insertGetId(array('mobile' => $mobile));
            $token = md5($mobile . $id . NOW_TIME);
            $data = array(
                'id' => $id,
                'user_type' => 2,
                'user_nickname' => '新注册用户-' . rand(88888, 99999),
                'create_time' => NOW_TIME,
                'last_login_time' => NOW_TIME,
                'sex' => 0,
                'avatar' => SITE_URL . '/image/headicon.png',
                'mobile' => $mobile,
                'token' => $token,
                'address' => "外太空",
            );

            $reg_result = db('user')->insert($data);
            if ($reg_result) {

                if ($invite_data) {
                    $invite_data['invite_user_id'] = $id;
                    $invite_data['create_time'] = NOW_TIME;
                    //添加邀请记录
                    db('invite_record')->insert($invite_data);
                }

                $result['data'] = array(
                    'id' => $id,
                    'token' => $token,
                    'sdkappid' => $sdk_app_id,
                    'is_reg_perfect' => 0
                );

                $signature = load_cache('usersign', ['id' => $id]);
                if ($signature['status'] != 1) {
                    $result['code'] = 0;
                    $result['msg'] = $signature['error'];
                    return_json_encode($result);
                }
                $result['data']['user_sign'] = $signature['usersign'];

                $result['code'] = 1;
                $result['msg'] = "注册成功!";
            } else {

                $result['code'] = 0;
                $result['msg'] = "注册失败，请重新注册！";
                //$data = array('msg'=>"注册失败，请重新注册",'error'=>$reg_result);
                //save_log($data);
            }
        }

        return_json_encode($result);
    }

    //分享注册成功
    public function share_reg_success()
    {

        $config = load_cache('config');

        $this->assign('ios_download_url', $config['ios_download_url']);
        $this->assign('android_download_url', $config['android_download_url']);
        return $this->fetch();
    }

    //发送验证码
    public function code()
    {
        $result = array('code' => 0, 'msg' => '', 'data' => array());
        $mobile = input('param.mobile');
        if (empty($mobile)) {
            intvals($mobile);
        }

        //TODO 限制 每个ip 的发送次数
        $code = get_verification_code($mobile);

        if (!$code) {
            $result['msg'] = "验证码发送过多,请明天再试!";
            //$data = array('msg'=>$result['msg'],'error'=>$code);
            //save_log($data);
            return_json_encode($result);
        }

        //互亿无线
        $target = "http://106.ihuyi.cn/webservice/sms.php?method=Submit";
        $config = load_cache('config');

        if ($config['system_sms_open'] == 1) {
            $post_data = "account=" . $config['system_sms_key'] . "&password=" . $config['system_sms_id'] . "&mobile=" . $mobile . "&content=" . rawurlencode("您的验证码是：" . $code . "。请不要把验证码泄露给其他人。");
            //密码可以使用明文密码或使用32位MD5加密
            $gets = xml_to_array(post($post_data, $target));

//            dump($gets);
            if ($gets['SubmitResult']['code'] == 2) {

                $result['code'] = 1;
                $result['msg'] = '验证码已经发送成功!';
            } else {
                $result['code'] = 0;
                $result['msg'] = $gets['SubmitResult']['msg'];
            }
        } else {
            $code = '123456';
            $result['msg'] = '验证码已经发送成功!';
            $result['code'] = 1;
        }

        verification_code_log($mobile, $code);
        return_json_encode($result);
    }

    public function share_reg_new()
    {
        $invite_code = input('param.invite_code');

        $invite_code_info = db('invite_code')->where('invite_code', '=', $invite_code)->find();
        $user_info = get_user_base_info($invite_code_info['user_id']);

        $this->assign('user_info', $user_info);
        $this->assign('invite_code', $invite_code);
        return $this->fetch();
    }


}
