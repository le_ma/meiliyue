<?php
/**
 * Created by PhpStorm.
 * User: weipeng
 * Date: 2018/3/1
 * Time: 15:20
 */

namespace app\api\controller;

use Qiniu\Auth;
use think\Config;

class AppApi extends Base
{

    //获取七牛上传token
    public function get_qiniu_upload_token(){

        $result = array('code' => 1,'msg' => '');

        require_once DOCUMENT_ROOT . '/system/qiniu/autoload.php';
        // 需要填写你的 Access Key 和 Secret Key
        $accessKey = Config::get('qiniu.accessKey');
        $secretKey = Config::get('qiniu.secretKey');
        // 构建鉴权对象
        $auth = new Auth($accessKey, $secretKey);
        // 要上传的空间
        $result['bucket'] = Config::get('qiniu.bucket');
        $result['domain'] = Config::get('qiniu.DOMAIN');
        $result['token'] = $auth->uploadToken($result['bucket']);

        return_json_encode($result);
    }

    //配置文件
    public function config(){

        $result = array('code' => 1,'msg' => '','data' => array());

        $config = load_cache('config');
        //心跳
        $data['heartbeat'] = $config['heartbeat_interval'];
        $data['group_id'] = $config['acquire_group_id'];
        $data['sdkappid'] = $config['tencent_sdkappid'];
        $data['accountType'] = $config['accountType_one'];
        $data['private_photos'] = $config['private_photos'];
        $data['app_qgorq_key'] = $config['app_qgorq_key'];
        $data['video_deduction'] = $config['video_deduction'];
        $data['tab_live_heart_time'] = $config['tab_live_heart_time'];
        $data['system_message'] = $config['system_message'];
        $data['currency_name'] = $config['currency_name'];

        /*---------0608新增-----------*/
        $data['is_open_chat_pay'] = $config['is_open_chat_pay'];
        $data['private_chat_money'] = $config['private_chat_money'];
        $data['video_call_msg_alert']= $config['video_call_msg_alert'];
        /*--------------------*/


        //客户端h5链接
        $data['app_h5']=array(
            'newbie_guide'=>'http://'.$_SERVER['HTTP_HOST'].'/mapi/public/index.php/api/novice_guide_api/index',//新手引导
            'my_level'=>'http://'.$_SERVER['HTTP_HOST'].'/mapi/public/index.php/api/level_api/index',                //我的等级 用户uid   token
            'invite_friends'=>'http://'.$_SERVER['HTTP_HOST'].'/mapi/public/index.php/api/invite_api/index',         //邀请好友 用户uid
            'disciple_contribution'=>'http://'.$_SERVER['HTTP_HOST'].'/mapi/public/index.php/api/disciple_api/index', //徒弟贡献榜 用户uid token
            'my_detail'=>'http://'.$_SERVER['HTTP_HOST'].'/mapi/public/index.php/api/detail_api/defaults',              //我的明细 	用户uid 不填是所有的 1聊币2积分
            'system_message'=> 'http://'.$_SERVER['HTTP_HOST'].'/mapi/public/index.php/api/system_message_api/index',
            'about_me' => 'http://'.$_SERVER['HTTP_HOST'] . '/mapi/public/index.php/api/novice_guide_api/content/id/6.html',//关于我们
            'invite_reg_url' => 'http://'.$_SERVER['HTTP_HOST'] . '/mapi/public/index.php/api/login_api/share_reg_new',//邀请注册链接
            'private_clause_url' => 'http://'.$_SERVER['HTTP_HOST'] . '/mapi/public/index.php/api/novice_guide_api/content/id/7.html',//隐私条款
            'invite_share_menu' => 'http://'.$_SERVER['HTTP_HOST'] . '/mapi/public/index.php/api/invitation_api/index',//邀请收益
            'online_custom_service' => 'http://'.$_SERVER['HTTP_HOST'] . ':9010/client',//客服地址
            'vip_url' => 'http://'.$_SERVER['HTTP_HOST'] . '/mapi/public/index.php/api/vip_api/index'
        );

        //查询开屏广告
        $splash = db('slide_item') -> find(6);
        $data['splash_url'] = '';
        $data['splash_img_url'] = '';
        $data['splash_content'] = '';

        if($splash){
            $data['splash_url'] = $splash['url'];
            $data['splash_img_url'] = $splash['image'];
            $data['splash_content'] = $splash['content'];
        }

        //是否开启了自定义金额
        $data['open_custom_video_charge_coin'] = 0;
        if(defined('OPEN_CUSTOM_VIDEO_CHARGE_COIN') && OPEN_CUSTOM_VIDEO_CHARGE_COIN == 1){
            $data['open_custom_video_charge_coin'] = 1;
        }

        //是否开启邀请模块
        $data['open_invite'] = 0;
        if(defined('OPEN_INVITE') && OPEN_INVITE == 1){
            $data['open_invite'] = 1;
        }


        /*--------0816新增----------*/
        $data['open_video_chat']= 0;
        if(defined('OPEN_VIDEO_CHAT') && OPEN_VIDEO_CHAT == 1){
            $data['open_video_chat'] = 1;
        }


        //脏字库
        $data['dirty_word'] = '';
        if($config['dirty_word']){
            $data['dirty_word'] = $config['dirty_word'];
        }

        //客服QQ
        $data['custom_service_qq']  = $config['custom_service_qq'];

        //版本控制
        $data['android_download_url'] = $config['android_download_url'];
        $data['android_app_update_des'] = $config['android_app_update_des'];
        $data['android_version'] = $config['android_version'];

        $data['ios_download_url'] = $config['ios_download_url'];
        $data['ios_app_update_des'] = $config['ios_app_update_des'];
        $data['ios_version'] = $config['ios_version'];

        $data['is_force_upgrade'] = $config['is_force_upgrade'];

        //是否开启ios上架审核
        $data['is_ios_base'] = $config['is_grounding'];

        //查询首页星级列表
        if(defined('OPEN_STAR') && OPEN_STAR == 1){
            $data['star_level_list'] = db('user_star_level') -> field('level_name,id') -> select();
        }

        $result['data'] = $data;
        return_json_encode($result);
    }

    //心跳间隔时间 Redis 存储
    public function interval(){

        $result = array('code' => 1,'msg' => '','data' => array());
        $uid = input('param.uid');
        $token = input('param.token');

        update_heartbeat($uid);

        return_json_encode($result);
    }

    public function test(){

        require_once DOCUMENT_ROOT . '/system/umeng/UmengPush.php';

        $push = new \UmengPush('5afa96c4f29d983e09000070','axpsgwhp3c5mnwvpgpi4kny6jixc2ml1');
        $push -> sendAndroidCustomizedcast();

    }

}