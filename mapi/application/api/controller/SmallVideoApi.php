<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/1/18 0018
 * Time: 下午 16:21
 */

namespace app\api\controller;

use think\Db;
use \app\api\controller\Base;

class SmallVideoApi extends Base
{

    //小视频
    public function index()
    {
        $result = array('code' => 1, 'msg' => '', 'data' => array());
        $page = intval(input('param.page'));
        $uid = intval(input('param.uid'));
        $type = trim(input('param.type'));

        if (empty($type)) {
            intvals($type);
        }
        if ($type == 'reference') {            //推荐视频
            $video = $this->reference($page, $uid);

        } elseif ($type == 'latest') {        //最新视频
            $video = $this->latest($page, $uid);

        } elseif ($type == 'attention') {        //关注视频

            $video = $this->attention($page, $uid);

        } else {                          //附近的视频
            $lat = input('param.lat');
            $lng = input('param.lng');

            if (empty($lat)) {
                intvals($lat);
            }

            if (empty($lng)) {
                intvals($lng);
            }

            $video = $this->near($page, $lng, $lat, $uid);
        }

//        $config = load_cache('config');
//        foreach ($video as &$v){
//            //获取视频地址
//            //$v['video_url'] = get_sign_video_url($config['tencent_video_sign_key'],$v['video_url']);
//            //$v['is_follow'] = get_attention($uid,$v['uid']);
//        }

        $result['data'] = $video;
        return_json_encode($result);
    }

    //推荐视频
    public function reference($num = 0, $uid)
    {

        $video = Db::name('user_video')
            ->alias('a')
            ->field('a.*,u.user_nickname,u.avatar')
            ->join('user u', 'a.uid=u.id')
            ->where("a.is_recommend=1 and a.type=1")
            ->order("a.follow_num desc")
            ->page($num)
            ->select();

        //echo db('user_video') -> getLastSql();exit;
        return $video;
    }

    //最新视频
    public function latest($num = 0, $uid)
    {
        $video = db('user_video')
            ->alias('a')
            ->field('a.*,c.user_nickname,c.avatar')
            ->join('user c', 'c.id=a.uid')
            ->where("a.type=1")
            ->order("a.addtime desc")
            ->page($num)
            ->select();

        return $video;
    }

    //关注视频
    public function attention($num = 0, $uid)
    {

        $video = db('user_video')
            ->alias('a')
            ->field('a.*,c.user_nickname,c.avatar')
            ->join('user_attention u', 'a.uid=u.attention_uid')
            ->join('user c', 'c.id=a.uid')
            ->where("a.type=1 and u.uid=$uid")
            ->order("a.addtime desc")
            ->page($num)
            ->select();

        return $video;
    }

    //附近的视频
    public function near($num = 0, $lng = '', $lat = '', $uid)
    {

        $squares = returnSquarePoint($lng, $lat);
        $where = "a.lat<>0 and a.lat>{$squares['right-bottom']['lat']} and a.lat<{$squares['left-top']['lat']} and a.lng>{$squares['left-top']['lng']} and
        a.lng<{$squares['right-bottom']['lng']} and a.type=1";

        $video = Db::name('user_video')
            ->alias("a")->field('a.*,c.user_nickname,c.avatar')
            ->join('user c', 'c.id=a.uid')
            ->where($where)
            ->order("a.addtime desc")
            ->page($num)
            ->select();
        return $video;
    }

    //视频分享次数加1
    public function share_number()
    {
        $result = array('code' => 0, 'msg' => '增加分享次数失败', 'data' => array());
        $id = input("param.id");
        if (empty($id)) {
            intvals($id);
        }
        $name = Db::name("user_video")->where("id=$id")->setInc("share");
        if ($name) {
            $result['code'] = '1';
            $result['msg'] = '增加分享次数成功';
        }
        return_json_encode($result);
    }
}