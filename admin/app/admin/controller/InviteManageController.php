<?php
/**
 * Created by PhpStorm.
 * User: weipeng
 * Date: 2018/7/25
 * Time: 17:45
 */

namespace app\admin\controller;


use cmf\controller\AdminBaseController;

class InviteManageController extends AdminBaseController
{

    public function income_index(){

        $where = [];
        if(isset($_REQUEST['invite_code']) && $_REQUEST['invite_code'] != ''){
            $where['i.invite_code'] = $_REQUEST['invite_code'];
        }

        if(isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != ''){
            $where['i.user_id'] = $_REQUEST['user_id'];
        }

        if(isset($_REQUEST['invite_user_id']) && $_REQUEST['invite_user_id'] != ''){
            $where['i.invite_user_id'] = $_REQUEST['invite_user_id'];
        }

        $list = db('invite_profit_record')
            -> alias('i')
            -> join('user u','i.user_id=u.id')
            -> field('u.user_nickname,i.*')
            -> where($where)
            -> order('i.create_time desc')
            -> paginate(20);

        $data = $list -> toArray()['data'];
        foreach ($data as &$v){
            //$user_info = db('user') -> where('id','=',$v['user_id']) -> field('user_nickname') -> find();
            $invite_user_info = db('user') -> where('id','=',$v['invite_user_id']) -> field('user_nickname') -> find();
            //$v['user_nickname'] = $user_info['user_nickname'];
            $v['invite_user_nickname'] = $invite_user_info['user_nickname'];
        }

        $this->assign('request',$_REQUEST);
        $this->assign('list',$data);
        $this->assign('page',$list->render());
        return $this->fetch();
    }

    public function invite_record_index(){

        $where = [];
        if(isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != ''){
            $where['i.user_id'] = $_REQUEST['user_id'];
        }

        $list = db('invite_record')
            -> alias('i')
            -> join('user u','i.user_id=u.id')
            -> field('u.user_nickname,i.*,u.invitation_coin')
            -> group('i.user_id')
            -> order('i.create_time desc')
            -> where($where)
            -> paginate(20);

        $data = $list -> toArray()['data'];

        foreach ($data as &$v){
            $v['invite_count'] = db('invite_record') -> where('user_id','=',$v['user_id']) -> count();
            $v['invite_total_income'] = db('invite_profit_record') -> where('user_id','=',$v['user_id']) -> sum('income');
            $v['sum_coin']= db('invite_cash_record')->where("status !=2  and uid=".$v['user_id'])->sum("coin");

        }

        $this->assign('list',$data);
        $this->assign('page',$list->render());
        $this->assign('request',$_REQUEST);
        return $this->fetch();
    }

    /*
     *   邀请提现记录
     * */
    public function withdrawal(){

        $where = [];
        if(!input("param.page")){
            session("withdrawal",null);
        }
        if(isset($_REQUEST['uid']) || isset($_REQUEST['start_time']) || isset($_REQUEST['end_time']) || isset($_REQUEST['status'])){
            session("withdrawal",$_REQUEST);
        }
        $uid=session("withdrawal.uid") ? session("withdrawal.uid") :'';
        $start_time=session("withdrawal.start_time") ? session("withdrawal.start_time") :'';
        $end_time=session("withdrawal.end_time") ? session("withdrawal.end_time") :'';
        $status=(session("withdrawal.status") !='-1') &&  (session("withdrawal.status") !='')? session("withdrawal.status") :'';
        if($uid){
            $where['i.uid'] = $uid;
        }

        if($start_time){
            $starttime=strtotime($start_time.":00");
            $where['addtime']=['> time',$starttime];
        }
        if($end_time){
            $endtime=strtotime($end_time.":59");
            $where['addtime']=['< time',$endtime];
        }
        if($status || $status =='0'){
            $where['i.status'] = $status;
        }

        $list = db('invite_cash_record')
            -> alias('i')
            -> join('user u','i.uid=u.id')
            -> field('u.user_nickname,i.*')
            -> order('i.addtime desc')
            -> where($where)
            -> paginate(10);

        $data = $list -> toArray();


        $this->assign('list',$data['data']);
        $this->assign('page',$list->render());
        $this->assign('request',session("withdrawal"));
        return $this->fetch();
    }
    /*
     *  操作数据库提现
     * */

    public function operation(){
        $id=input("param.id");
        $type=input("param.type");

        $root=array('msg'=>'参数错误','status'=>0);
        if(!$id){
            echo json_encode($root);exit;
        }

        $list = db('invite_cash_record')->where("id=$id")->update(array('status'=>$type));

        if($list){
            $user = db('invite_cash_record')->where("id=$id")->field("uid,coin")->find();
            if($type ==2){
                db('user')->where('id='.$user['uid'])->inc('invitation_coin', $user['coin'])->update();
            }
            $root['msg']='操作成功';
            $root['status']='1';
        }else{
            $root['msg']='操作失败';
        }
        echo json_encode($root);exit;
    }
}