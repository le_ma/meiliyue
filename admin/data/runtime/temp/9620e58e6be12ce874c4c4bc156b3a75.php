<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:60:"themes/admin_simpleboot3/admin/withdrawals_manage/index.html";i:1528728704;s:43:"themes/admin_simpleboot3/public/header.html";i:1519953092;}*/ ?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="__TMPL__/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="__TMPL__/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="__STATIC__/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "__ROOT__/",
            WEB_ROOT: "__WEB_ROOT__/",
            JS_ROOT: "static/js/",
            APP: '<?php echo \think\Request::instance()->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="__TMPL__/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="__STATIC__/js/layer/layer.js" rel="stylesheet"></script>
    <script src="__STATIC__/js/wind.js"></script>
    <script src="__TMPL__/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip();
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
</head>

<style>

    #status,#type{    width: 100px;
        height: 32px;
        border-color: #dce4ec;
        color: #aeb5bb;}

</style>
<body>
<div class="wrap js-check-wrap">
    <ul class="nav nav-tabs">
        <li class="active"><a href="<?php echo url('withdrawals_manage/index'); ?>">提现记录</a></li>
    </ul>
    <div class="table-actions">
        <form class="well form-inline margin-top-20" method="post" action="<?php echo url('withdrawals_manage/index'); ?>">
            手机号:
            <input type="text" class="form-control" name="mobile" style="width: 120px;" value="<?php echo $request['mobile']; ?>" placeholder="请输入<?php echo lang('MOBILE'); ?>">
            审核状态：
            <select name="status" id="status">
                <option value="0">未审核</option>
                <option value="1" <?php if($request['status'] == 1): ?> selected="selected" <?php endif; ?>>已审核</option>
                <option value="2" <?php if($request['status'] == 2): ?> selected="selected" <?php endif; ?>>已拒绝</option>
            </select>
            <input type="submit" class="btn btn-primary" value="搜索"/>
            <a class="btn btn-danger" href="<?php echo url('withdrawals_manage/index'); ?>">清空</a>
        </form>
    </div>
    <table class="table table-hover table-bordered table-list">
        <thead>
        <tr>
            <th width="50">ID</th>
            <th>用户ID</th>
            <th>用户昵称</th>
            <th>手机号码</th>
            <th>提现数量</th>
            <th>提现金额</th>
            <th>提现姓名</th>
            <th>提现支付账号</th>
            <th>提交时间</th>
            <th>状态</th>
            <th width="130">操作</th>
        </tr>
        </thead>
        <tbody>
        <?php 
            $statuses=array('0'=>"未审核","1"=>"审核通过","2" => "拒绝提现");
         if(is_array($data) || $data instanceof \think\Collection || $data instanceof \think\Paginator): if( count($data)==0 ) : echo "" ;else: foreach($data as $key=>$vo): ?>
            <tr>
                <td><?php echo $vo['id']; ?></td>
                <td><?php echo $vo['user_id']; ?></td>
                <td><?php echo $vo['user_nickname']; ?></td>
                <td><?php echo $vo['mobile']; ?></td>
                <td><?php echo $vo['income']; ?></td>
                <th><?php echo $vo['money']; ?></th>
                <td><?php echo $vo['gathering_name']; ?></td>
                <td><?php echo $vo['gathering_number']; ?></td>
                <td><?php echo date("Y-m-d H:i:s",$vo['create_time'] ); ?></td>
                <td><?php echo $statuses[$vo['status']]; ?></td>
                <td>

                    <?php if($vo['status'] == 0): ?>
                        <a href="<?php echo url('withdrawals_manage/adopt_cash',array('id'=>$vo['id'])); ?>">审核通过</a>
                        |
                        <a href="<?php echo url('withdrawals_manage/refuse_cash',array('id'=>$vo['id'])); ?>">拒绝</a>
                    <?php else: ?>

                        <a href="<?php echo url('withdrawals_manage/del',array('id'=>$vo['id'])); ?>">删除</a>
                    <?php endif; ?>

                </td>
            </tr>
        <?php endforeach; endif; else: echo "" ;endif; ?>
        </tbody>
    </table>

    <div class="pagination"><?php echo $page; ?></div>
</div>
<script type="text/javascript" src="__STATIC__/js/admin.js"></script>
</body>
</html>