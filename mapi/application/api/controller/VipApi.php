<?php

namespace app\api\controller;
use app\api\controller\Base;

/**
 * Created by PhpStorm.
 * User: weipeng
 * Date: 2018/8/17
 * Time: 00:14
 */
class VipApi extends Base{

    public function index(){

        $result = array('code' => 1,'msg' => '');
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));

        if($uid == 0 || empty($token)){
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid,$token);

        if(!$user_info){
            $result['code'] = 10001;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        $user_info = get_user_base_info($uid,['vip_end_time'],1);

        $vip_day = intval(($user_info['vip_end_time'] - time()) / (60 * 60 * 24));

        $list = db('vip_rule') -> select();

        $this->assign('user_info',$user_info);
        $this->assign('vip_day',$vip_day > 0 ? $vip_day : 0);
        $this->assign('uid',$uid);
        $this->assign('token',$token);
        $this->assign('list',$list);
        return $this->fetch();
    }


    //购买会员
    public function buy_vip(){

        $result = array('code' => 1,'msg' => '');
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));
        $vip_id = intval(input('param.id'));

        if($uid == 0 || empty($token)){
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid,$token);

        if(!$user_info){
            $result['code'] = 10001;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        $rule = db('vip_rule') -> find($vip_id);

        if(!$rule){
            $result['code'] = 0;
            $result['msg'] = '规则不存在！';
            return_json_encode($result);
        }

        $user_info = get_user_base_info($uid,['vip_end_time'],1);

        if($user_info['coin'] < $rule['money']){

            $result['code'] = 0;
            $result['msg'] = '余额不足！';
            return_json_encode($result);
        }

        $res = db('user') -> where('id','=',$uid) -> setDec('coin',$rule['money']);
        if(!$res){

            $result['code'] = 0;
            $result['msg'] = '购买失败！';
            return_json_encode($result);
        }

        $vip_time = $rule['day_count'] * 60 * 60 * 24;
        if($user_info['vip_end_time'] > time()){

            db('user') -> where('id','=',$uid) -> setInc('vip_end_time',$vip_time);
        }else{

            $vip_time = time() + $vip_time;
            db('user') -> where('id','=',$uid) -> setField('vip_end_time',$vip_time);
        }

        return_json_encode($result);
    }
}