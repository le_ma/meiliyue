<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:57:"themes/admin_simpleboot3/admin/video_call_list/index.html";i:1529571963;s:43:"themes/admin_simpleboot3/public/header.html";i:1519953092;}*/ ?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="__TMPL__/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="__TMPL__/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="__STATIC__/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "__ROOT__/",
            WEB_ROOT: "__WEB_ROOT__/",
            JS_ROOT: "static/js/",
            APP: '<?php echo \think\Request::instance()->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="__TMPL__/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="__STATIC__/js/layer/layer.js" rel="stylesheet"></script>
    <script src="__STATIC__/js/wind.js"></script>
    <script src="__TMPL__/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip();
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
</head>
<body>
<div class="wrap js-check-wrap">
    <ul class="nav nav-tabs">
        <li class="active"><a href="javascript:;">通话列表</a></li>
    </ul>

    <form class="js-ajax-form" action="<?php echo url('refill/upd'); ?>" method="post">

        <table class="table table-hover table-bordered table-list">
            <thead>
            <tr>
                <th>ID</th>
                <th>用户昵称(ID)</th>
                <th>主播昵称(ID)</th>
                <th>频道ID</th>
                <th>状态</th>
                <th>拨打时间</th>
                <th>操作</th>
            </tr>
            </thead>
            <tfoot>
            <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): if( count($list)==0 ) : echo "" ;else: foreach($list as $key=>$vo): ?>
                <tr>
                    <td><?php echo $vo['id']; ?></td>
                    <td><?php echo $vo['emcee_info']['user_nickname']; ?>(<?php echo $vo['emcee_info']['id']; ?>)</td>
                    <td><?php echo $vo['user_info']['user_nickname']; ?>(<?php echo $vo['user_info']['id']; ?>)</td>
                    <td><?php echo $vo['channel_id']; ?></td>
                    <?php if($vo['status'] == 1): ?>
                        <td>通话中</td>
                        <?php else: ?>
                        <td>等待接通</td>
                    <?php endif; ?>
                    <td><?php echo date("Y-m-d H:i:s",$vo['create_time'] ); ?></td>
                    <td>
                        <a href="#" onclick="select_video('<?php echo $vo['id']; ?>')">查看视频</a>
                        | <a href="<?php echo url('video_call_list/close',array('id'=>$vo['id'])); ?>">关闭</a>
                    </td>
                </tr>
            <?php endforeach; endif; else: echo "" ;endif; ?>
            </tfoot>
        </table>

    </form>
</div>
<script src="__STATIC__/js/layer/layer.js" rel="stylesheet"></script>
<script>

    function select_video(id){
        layer.open({
            type: 2,
            title: '查看视频',
            shadeClose: true,
            shade: 0.8,
            area: ['90%', '90%'],
            content: '/admin/public/admin/video_call_list/select_video/id/' + id //iframe的url
        });
    }
    $(".del").click(function(){
        var id=$(this).attr('data-id');
        layer.confirm('确定删除？', {
            btn: ['确定','取消'] //按钮
        }, function(){
            $.ajax({
                url: "<?php echo url('refill/del'); ?>",
                type: 'post',
                dataType: 'json',
                data: {id: id},
                success: function (data) {
                    if(data =='1'){
                        layer.msg("删除成功",{time: 2000, icon:1},function(){
                            window.location.reload();
                        });
                    }else{
                        layer.msg("删除失败",{time: 2000, icon:2});
                    }
                }
            });

        });
    })
    $(".btn-primary").click(function(){
        $(".js-ajax-form").submit();
    })
</script>
</body>
</html>