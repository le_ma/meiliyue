<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/1/24 0024
 * Time: 上午 10:01
 */

namespace app\api\controller;

use think\Db;
use \app\api\controller\Base;
use UserOnlineStateRedis;

class RecommendedApi extends Base
{
    //首页推荐用户
    public function recommend_user()
    {

        $result = array('code' => 1, 'msg' => '', 'data' => array());
        $page = intval(input('param.page'));
        $uid = intval(input('param.uid'));

        $user_info = get_user_base_info($uid);
        $sex = 1;
        if ($user_info['sex'] == 1) {
            $sex = 2;
        }

        $config = load_cache('config');
        $video = db('user')
            ->alias('a')
            ->field('a.id,a.sex,a.user_nickname,a.avatar,a.level')
            ->join('monitor m', 'a.id=m.user_id', 'left')
            ->where("a.user_status!=0 and a.sex=$sex and a.reference=1 and a.is_online=1")
            ->order("m.monitor_time desc")
            ->page($page)
            ->select();

        foreach ($video as &$v) {
            //好评百分比
            $v['evaluation'] = db('video_call_record_log')->where('anchor_id', '=', $v['id'])->where('is_fabulous', '=', 1)->count();
            $level = get_level($v['id']);
            $v['level_name'] = $level;
            //获取主播是否登录
            $v['is_online'] = is_online($v['id'], $config['heartbeat_interval']);
        }

        $result['online_emcee_count'] = 0;
        $result['online_emcee'] = [];
        //获取在线主播人数
        if ($page == 1) {
            require_once DOCUMENT_ROOT . '/system/redis/UserOnlineStateRedis.php';
            $result['online_emcee'] = db('user')->alias('u')->field('u.id,u.avatar')->where('u.is_online', '=', 1)->where('u.sex', '=', 2)->limit(3)->select();
            $user_online_state_redis = new UserOnlineStateRedis();
            $result['online_emcee_count'] = $user_online_state_redis->get_female_online_count();

        }
        $result['data'] = $video;
        return_json_encode($result);
    }

    //首页轮播图
    public function shuffling()
    {

        $result = array('code' => 0, 'msg' => '', 'data' => array());
        $shuffling = intval(input('param.shuffling'));

        $img = db('slide_item')->where("slide_id=$shuffling and status=1")->order("list_order desc")->field('id,image,title,url')->select();
        if ($img) {
            $result['code'] = '1';
            $result['data'] = $img;
        } else {
            $result['msg'] = "暂无图片";
        }

        return_json_encode($result);
    }

}