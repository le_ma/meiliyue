<?php
/**
 * Created by PhpStorm.
 * User: weipeng
 * Date: 2018/3/4
 * Time: 20:31
 */

namespace app\api\controller;

use think\Db;

class LevelApi extends Base
{

    /**
     * h5 页面 等级
     */
    public function index(){
        $uid=input('param.uid');
        $token=input('param.token');
        if(empty($uid) ||empty($token)){
            $this->error("传参错误");exit;
        }

        $name = Db::name("user")->where("id=$uid and token='$token'")->field("sex")->find();
        if(empty($name)){
            $this->error("暂无用户数据");exit;
        }

        $level_my = get_grade_level($uid);
        //等级列表
        $Level = load_cache('level');

        //var_dump($Level);exit;
        $this->assign('level_my', $level_my);
        $this->assign('level', $Level);
        $this->assign('name', $name);

        return $this->fetch();
    }
}