<?php
/**
 * Created by PhpStorm.
 * User: weipeng
 * Date: 2018/3/19
 * Time: 10:34
 */

namespace app\admin\controller;


use cmf\controller\AdminBaseController;

class JoinInController extends AdminBaseController
{

    public function index(){

        $list = db('join_in')
            ->alias('j')
            ->join("user u","u.id=j.user_id")
            ->field('u.user_nickname,u.mobile,j.*')
            ->order("j.create_time DESC")
            ->paginate(20);
        //echo db() -> getLastSql();exit;

        $data = $list->toArray();

        $page = $list->render();
        //dump($data);exit;

        $this->assign('data',$data['data']);
        $this->assign('page', $page);
        return $this -> fetch();
    }

}