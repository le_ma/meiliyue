<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/1/23 0023
 * Time: 上午 11:04
 */

namespace app\user\controller;
use cmf\controller\AdminBaseController;
use think\Db;

class ReferenceController extends AdminBaseController
{

    //推荐用户
    public function reference()
    {
        $user = Db::name("user_reference")->order('orderno desc')->paginate(10);
        $lists=$user->toArray();
        foreach($lists['data'] as &$v){
            $uid=$v['uid'];
            $users = Db::name("user")->where("id=$uid")->find();
            $v['user_nickname']=$users['user_nickname'];
            $v['user_status']=$users['user_status'];
            $v['mobile']=$users['mobile'];
        }
        $this->assign('user',$lists['data']);

        $this->assign('page', $user->render());
        return $this->fetch();
    }
    /**
     * 本站用户拉黑
     * @adminMenu(
     *     'name'   => '本站用户拉黑',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '本站用户拉黑',
     *     'param'  => ''
     * )
     */
    public function ban()
    {
        $id = input('param.id', 0, 'intval');
        if ($id) {
            $result = Db::name("user")->where(["id" => $id, "user_type" => 2])->setField('user_status', 0);

            if ($result) {
                $user['id']=$id;
                $this->success('操作成功！');
            } else {
                $this->error('会员拉黑失败,会员不存在,或者是管理员！');
            }
        } else {
            $this->error('数据传入失败！');
        }
    }

    /**
     * 本站用户启用
     * @adminMenu(
     *     'name'   => '本站用户启用',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '本站用户启用',
     *     'param'  => ''
     * )
     */
    public function cancelBan()
    {
        $id = input('param.id', 0, 'intval');
        if ($id) {
            Db::name("user")->where(["id" => $id, "user_type" => 2])->setField('user_status', 1);
            $user['id']=$id;
            $message=Db::name("user_message")->where("id=2")->find();
            $this->success('操作成功！');

        } else {
            $this->error('数据传入失败！');
        }
    }
    //排序
    public function upd(){
        $param=request()->param();
        $param=request()->param();
        $data=array();
        $sert=array();
        foreach($param['listorders'] as $k=>$v){
            $status=Db::name("user_reference")->where("id=$k")->update(array('orderno'=>$v));
        }

        if($status){
            $this->success("排序成功");
        }else{
            $this->success("排序失败");
        }
    }
}
