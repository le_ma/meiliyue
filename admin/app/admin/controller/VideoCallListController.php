<?php
/**
 * Created by PhpStorm.
 * User: weipeng
 * Date: 2018/6/18
 * Time: 22:45
 */

namespace app\admin\controller;


use cmf\controller\AdminBaseController;

class VideoCallListController extends AdminBaseController
{

    public function index(){

        $list = db('video_call_record')
            ->order("create_time DESC")
            ->paginate(20);

        $data = $list->toArray();
        $page = $list->render();

        foreach ($data['data'] as &$v){
            $base_field = 'id,avatar,user_nickname,sex,level,coin';

            $emcee_info = db('user') -> field($base_field) -> find($v['anchor_id']);
            $v['emcee_info'] = $emcee_info;

            $user_id = $v['anchor_id'] == $v['user_id'] ? $v['call_be_user_id'] : $v['user_id'];
            $user_info = db('user') -> field($base_field) -> find($user_id);
            $v['user_info'] = $user_info;

        }

        $this->assign('list',$data['data']);
        $this->assign('page', $page);
        return $this -> fetch();
    }

    //查看视频
    public function select_video(){

        $id = input('param.id');
        $video = db('video_call_record')
            ->find($id);

        $config = load_cache('config');

        $this->assign('channel_id',$video['channel_id']);
        $this->assign('app_qgorq_key',$config['app_qgorq_key']);

        return $this -> fetch();
    }

    //关闭视频通话
    public function close(){
        $id = input('param.id');
        $video = db('video_call_record')
            ->find($id);

        $ext = array();
        $ext['type'] = 25;//type 25 关闭视频通话
        $ext['msg_content'] = '视频内容涉嫌违反平台或国家相关法律法规';

        $config = load_cache('config');
        require_once DOCUMENT_ROOT . '/system/im_common.php';
        $ser = open_one_im_push($config['tencent_identifier'],$video['anchor_id'],$ext);

        if($ser['ActionStatus'] =='OK'){
            $this->success('操作成功！');

        }else{
            $this->error('操作失败！');
        }
        exit;
    }
}