<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/7/14 0014
 * Time: 下午 14:47
 */
namespace app\api\controller;
use app\api\controller\Base;
use think\Db;

class DownloadApi extends Base{
    //分享下载页面
    public function index(){

        if(strpos($_SERVER['HTTP_USER_AGENT'], 'iPhone')||strpos($_SERVER['HTTP_USER_AGENT'], 'iPad')){
            $download=db("config")->where("code='ios_download_url'")->find();
        }else if(strpos($_SERVER['HTTP_USER_AGENT'], 'Android')){
            $download=db("config")->where("code='android_download_url'")->find();
        }else{
            $download=db("config")->where("code='android_download_url'")->find();
        }
        $this->assign('download',$download['val']);
        return $this->fetch();
    }
}