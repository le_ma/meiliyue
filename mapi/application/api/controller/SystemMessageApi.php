<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/2/23 0023
 * Time: 下午 14:53
 */
namespace app\api\controller;


use think\Db;
use cmf\controller\ApiController;

class SystemMessageApi extends Base
{

    //获取系统消息列表
    public function get_system_message(){

        $result = array('code' => 1,'msg' => '');
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));

        if($uid == 0 || empty($token)){
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid,$token);

        if(!$user_info){
            $result['code'] = 0;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }


        $user_list = db("user") -> where("id=$uid") -> field("last_remove_message_time") -> find();
        $last_time = $user_list['last_remove_message_time'];
        $where = "(touid=".$uid." or touid=0) and messageid !=0 and addtime > $last_time";
        $message_log = db("user_message_log") -> where($where) -> order('addtime desc')->select();
        $list = array();
        foreach($message_log as $k=>$v){

            $list[$k]['id'] = $v['id'];
            $id = $v['messageid'];
            if($v['type'] == 1){
                //后台管理员审核操作
                $message_list = db("user_message") -> where("id=$id") -> find();
                $list[$k]['title'] = $message_list['title'] . ':' . $v['messagetype'];
                $list[$k]['url'] = '';
            }else{
                //后台系统消息
                $message_list = Db::name("user_message_all") -> where("id=$id") -> find();
                $list[$k]['title'] = $message_list['title'];
                $list[$k]['url'] = $message_list['url'];
            }

            //内容
            $list[$k]['centent'] = $message_list['centent'];
            //时间
            $list[$k]['addtime'] = date('Y-m-d',$v['addtime']);

        }

        $result['list'] = $list;
        return_json_encode($result);

    }

    /**
     *    h5 页面我的消息
     */
    public function index(){
         $uid = intval(input("param.uid"));

        if($uid == 0){
            echo '页面访问错误';
            exit;
        }
        $time = Db::name("user")->where("id=$uid")->field("last_remove_message_time")->find();
        $last_time = $time['last_remove_message_time'];
        $where = "(touid=".$uid." or touid=0) and messageid !=0 and addtime > $last_time";
        $message_log = Db::name("user_message_log") -> where($where) -> order('addtime desc')->select();
        $data = array();
        foreach($message_log as $k=>$v){

            $data[$k]['id'] = $v['id'];
            $id = $v['messageid'];
            if($v['type'] == 1){
                //后台管理员审核操作
                $message_list = Db::name("user_message")->where("id=$id")->find();
                $data[$k]['title'] = $message_list['title'] . ':' . $v['messagetype'];
                $data[$k]['url'] = '';
            }else{
                //后台系统消息
                $message_list = Db::name("user_message_all")->where("id=$id")->find();
                $data[$k]['title'] = $message_list['title'];
                $data[$k]['url'] = $message_list['url'];
            }

            $data[$k]['centent'] = $message_list['centent'];         //内容
            $data[$k]['addtime'] = $v['addtime'];            //时间

        }
        //dump($data);

        $this->assign("message",$data);
        return $this->fetch();
    }



}