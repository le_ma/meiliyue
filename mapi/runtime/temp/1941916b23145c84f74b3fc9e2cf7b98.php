<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:88:"/www/wwwroot/uliao.bayuenet.com/mapi/public/../application/api/view/level_api/index.html";i:1535476197;}*/ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>等级详情</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/mapi/public/static/css/level.css" rel="stylesheet">
    <link href="/mapi/public/static/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <div class="col-xs-12 level creal">
        <img src="/mapi/public/static/image/male_banner.c430797.png">
    </div>
    <div class="col-xs-12 level-topb">
        <div class="col-xs-12 creal level-v">等级 ：
            <span class="font-sty"><?php if($name['sex'] == 1): ?>V<?php else: ?>M<?php endif; ?><?php echo $level_my['level_name']; ?> </span>
        </div>
        <!--<?php if($name['sex'] == 1): ?>-->
        <!--<div class="col-xs-12 creal level-v1">前六日和今日充值金额</div>-->
        <!--<?php else: ?>-->
        <!--<div class="col-xs-12 creal level-v1">前两日和今日赚取的总收益</div>-->
        <!--<?php endif; ?>-->
    </div>
    <div class="col-xs-12 level-dej">
        <div class="col-xs-2 level-dej1"><?php if($name['sex'] == 1): ?>V<?php else: ?>M<?php endif; ?><?php echo $level_my['level_name']; ?></div>
        <div class="col-xs-2 level-dej2 text-right col-xs-offset-8"><?php if($name['sex'] == 1): ?>V<?php else: ?>M<?php endif; ?><?php echo $level_my['down_name']; ?></div>
    </div>
    <div class="col-xs-12">
        <div class="col-xs-12 progress creal level-pro">
            <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" id="level">

            </div>
       </div>
    </div>
    <div class="col-xs-12">
        <div class="col-xs-6 text-right col-xs-offset-6">离 <span class="level-li1"> <?php if($name['sex'] == 1): ?>V<?php else: ?>M<?php endif; ?><?php echo $level_my['down_name']; ?> </span> 还差 <span class="level-li2"> <?php echo $level_my['spread']; ?> </span></div>
    </div>
    <!--<div class="col-xs-12 level-oil level-v1">-->
        <!--<?php if($name['sex'] == 1): ?>-->
        <!--<i class="glyphicon glyphicon-indent-left level-li1"></i> 前六日和今日充值金额 <span class="level-li1"><?php echo $level_my['msum']; ?></span>-->
        <!--<?php else: ?>-->
        <!--<i class="glyphicon glyphicon-indent-left level-li1"></i> 前两日和今日赚取的总收益 <span class="level-li1"><?php echo $level_my['msum']; ?></span>-->
        <!--<?php endif; ?>-->
     <!--</div>-->
    <!--<div class="col-xs-12 level-chong">-->
        <!--<a href="#">-->
        <!--<div class="col-xs-3">充 值</div>-->
        <!--<div class="col-xs-5 col-xs-offset-4 text-right">充 值 <span class="level-li1"> 升VIP </span> <i class="glyphicon glyphicon-chevron-right"></i></div>-->
        <!--</a>-->
    <!--</div>-->
    <div class="col-xs-12 level-yao">等级要求</div>
    <div class="col-xs-12">
        <table class="col-xs-12 table table-bordered">
            <tr class="active">
                <th class="text-center col-xs-2">等级</th>
                <th class="text-center">
                    <?php if($name['sex'] == 1): ?>
                        <div class="col-xs-12">充值金额</div>
                        <div class="col-xs-12 creal level-yang">前六日加当日之和</div>
                    <?php else: ?>
                        <div class="col-xs-12">收益数量</div>
                        <div class="col-xs-12 creal level-yang">前两日和今日赚取的总收益</div>
                    <?php endif; ?>

                 </th>
            </tr>
            <?php if(is_array($level) || $level instanceof \think\Collection || $level instanceof \think\Paginator): $i = 0; $__LIST__ = $level;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                <tr>
                    <td class="level-m">
                        <?php if($name['sex'] == 1): ?>V<?php else: ?>M<?php endif; ?><?php echo $vo['level_name']; ?>
                    </td>
                    <?php if($name['sex'] == 1): ?>
                        <td><?php echo $vo['level_up']; ?></td>
                        <?php else: ?>
                        <td><?php echo $vo['level_up_female']; ?></td>
                    <?php endif; ?>
                </tr>
            <?php endforeach; endif; else: echo "" ;endif; ?>

        </table>
    </div>
    <script src="/mapi/public/static/js/jquery-1.10.2.min.js"></script>
<script>
    $(document).ready(function(){
        var lei=<?php echo $level_my['progress']; ?>+"%";
        $("#level").css("width",lei);            //获取进度条
    })
</script>
</body>
</html>