<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/1/20 0020
 * Time: 上午 11:02
 */

namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;
use app\admin\model\AdminMenuModel;

class RefillController extends AdminBaseController
{
    /**
     * 充值列表
     */
    public function index()
    {
        $list = Db::name("user_charge_rule")->order("orderno asc")->select();
        $this->assign('list', $list);
        return $this->fetch();
    }

    /**
     * 充值添加
     */
    public function add()
    {
        $id = input('param.id');
        if ($id) {
            $name = Db::name("user_charge_rule")->where("id=$id")->find();
            $this->assign('rule', $name);
        } else {
            $this->assign('rule', array('type' => 0));
        }
        return $this->fetch();
    }

    public function addPost()
    {
        $param = $this->request->param();
        $id = $param['id'];
        $data = $param['post'];
        $data['addtime'] = time();
        if ($id) {
            $result = Db::name("user_charge_rule")->where("id=$id")->update($data);
        } else {
            $result = Db::name("user_charge_rule")->insert($data);
        }
        if ($result) {
            $this->success("保存成功", url('refill/index'));
        } else {
            $this->error("保存失败");
        }
    }

    //删除类型
    public function del()
    {
        $param = request()->param();
        $result = Db::name("user_charge_rule")->where("id=" . $param['id'])->delete();
        return $result ? '1' : '0';
    }

    //修改排序
    public function upd()
    {

        $param = request()->param();
        $data = '';
        foreach ($param['listorders'] as $k => $v) {
            $status = Db::name("user_charge_rule")->where("id=$k")->update(array('orderno' => $v));
            if ($status) {
                $data = $status;
            }
        }
        if ($data) {
            $this->success("排序成功");
        } else {
            $this->success("排序失败");
        }
    }

    //充值记录
    public function log_index()
    {

        $where = [];
        //dump($_REQUEST);
        if (!isset($_REQUEST['status']) || $_REQUEST['status'] == '-1') {
            $_REQUEST['status'] = '-1';
        } else {
            $where['c.status'] = $_REQUEST['status'];
        }

        if (isset($_REQUEST['end_time']) && $_REQUEST['end_time'] != '') {
            $where['c.end_time'] = ['<', $_REQUEST['end_time']];
        }

        if (isset($_REQUEST['start_time']) && $_REQUEST['start_time'] != '') {
            $where['c.start_time'] = ['>', $_REQUEST['start_time']];
        }

        if (isset($_REQUEST['uid']) && $_REQUEST['uid'] != '') {
            $where['c.uid'] = intval($_REQUEST['uid']);
        }

        if (isset($_REQUEST['order_id']) && $_REQUEST['order_id'] != '') {
            $where['c.order_id'] = intval($_REQUEST['order_id']);
        }

        $name = Db::name("user_charge_log")->alias('c')
            ->join('pay_menu p', 'c.pay_type_id=p.id')->field('c.*,p.pay_name')->order('c.addtime desc')->where($where)->paginate(20);
        //dump($where);exit;
        $result = array();
        foreach ($name as &$v) {
            $id = $v['id'];
            $user = Db::name("user")->where("id=$id")->find();
            $v['user_nickname'] = $user['user_nickname'];
            $result[] = $v;
        }
        //总充值
        $total_money = db('user_charge_log')->where('status', '=', 1)->sum('money');

        $this->assign('total_money', $total_money);
        $this->assign('refill', $_REQUEST);
        $this->assign('list', $result);
        $this->assign('page', $name->render());
        return $this->fetch();
    }

    //支付渠道列表
    public function pay_menu()
    {

        $list = db('pay_menu')->select();

        $this->assign('list', $list);
        return $this->fetch();
    }

    //添加充值渠道
    public function add_pay_menu()
    {

        return $this->fetch();
    }

    //添加充值渠道
    public function add_pay_menu_post()
    {

        $param = $this->request->param();
        $data = $param['post'];
        $result = Db::name("pay_menu")->insert($data);
        if ($result) {
            $this->success("保存成功", url('refill/pay_menu'));
        } else {
            $this->error("保存失败");
        }
    }

    //编辑充值渠道
    public function edit_pay_menu()
    {

        $id = input('param.id');

        $data = db('pay_menu')->find($id);
        $this->assign('data', $data);
        return $this->fetch();
    }

    //编辑支付渠道
    public function edit_pay_menu_post()
    {
        $param = $this->request->param();
        $id = $param['id'];
        $data = $param['post'];
        if ($id) {
            $result = Db::name("pay_menu")->where("id=$id")->update($data);
        } else {
            $result = Db::name("pay_menu")->insert($data);
        }
        if ($result) {
            $this->success("保存成功", url('refill/pay_menu'));
        } else {
            $this->error("保存失败");
        }
    }

    //删除类型
    public function del_pay_menu()
    {
        $param = request()->param();
        $result = Db::name("pay_menu")->where("id=" . $param['id'])->delete();
        return $result ? '1' : '0';
        exit;
    }
}
