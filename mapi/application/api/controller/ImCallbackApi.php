<?php
/**
 * Created by PhpStorm.
 * User: weipeng
 * Date: 2018/5/21
 * Time: 10:20
 */

namespace app\api\controller;

use BuguPush;
use UserOnlineStateRedis;
use VideoCallRedis;

header("Content-Type:text/html; charset=utf-8");

class ImCallbackApi extends Base
{

    public function callback()
    {

        require_once DOCUMENT_ROOT . '/system/umeng/BuguPush.php';

        $json = $GLOBALS['HTTP_RAW_POST_DATA'];
        $post = json_decode($json, true);

        file_put_contents('./im_callback.txt', $json);
        if ($post['CallbackCommand'] == 'C2C.CallbackAfterSendMsg') {
            if ($post['MsgBody'][0]['MsgType'] == 'TIMTextElem') {

                //普通私信消息
                $config = load_cache('config');
                $push = new BuguPush($config['umengapp_key'], $config['umeng_message_secret']);
                $custom = [
                    'action' => 1,
                    'user_id' => $post['From_Account']
                ];

//                if(!is_online($post['To_Account'],$config['heartbeat_interval'])){
//
//                    $push -> sendAndroidCustomizedcast('go_app',$post['To_Account'],'buguniao','私信消息','你有新的消息',$post['MsgBody'][0]['MsgContent']['Text'],json_encode($custom));
//                }
                $push->sendAndroidCustomizedcast('go_app', $post['To_Account'], 'buguniao', '私信消息', '你有新的消息', $post['MsgBody'][0]['MsgContent']['Text'], json_encode($custom));
                $push->sendIOSCustomizedcast('go_app', $post['To_Account'], 'buguniao', '私信消息', '你有新的消息', $post['MsgBody'][0]['MsgContent']['Text'], json_encode($custom));
            } else if ($post['MsgBody'][0]['MsgType'] == 'TIMCustomElem') {

                $data = $post['MsgBody'][0]['MsgContent']['Data'];
                $data = json_decode($data, true);

                $config = load_cache('config');

                $push = new BuguPush($config['umengapp_key'], $config['umeng_message_secret']);

                if($data['type'] == 23){//赠送礼物
                    $custom = [
                        'action' => 1,
                        'user_id' => $post['From_Account']
                    ];
                    $push->sendAndroidCustomizedcast('go_app', $post['To_Account'], 'buguniao', '礼物消息', '收到礼物打赏', $data['to_msg'], json_encode($custom));
                    $push->sendIOSCustomizedcast('go_app', $post['To_Account'], 'buguniao', '礼物消息', '收到礼物打赏', $data['to_msg'], json_encode($custom));

                }elseif ($data['type'] == 12){
                    $custom = [
                        'action' => 1,
                        'user_id' => $post['From_Account']
                    ];
                    $push->sendAndroidCustomizedcast('go_app', $post['To_Account'], 'buguniao', '新的通话消息', '新的通话消息，点击查看', '', json_encode($custom));
                    //$push->sendIOSCustomizedcast('go_app', $post['To_Account'], 'buguniao', '新的通话消息', '点击查看', $data['to_msg'], json_encode($custom));

                }

            }

        } else if ($post['CallbackCommand'] == 'State.StateChange') {

            $user_id = $post['Info']['To_Account'];
            $action = $post['Info']['Action'];


            if ($action == 'Logout') {
                db('video_call_record')->whereOr(['user_id' => $user_id])->whereOr(['call_be_user_id' => $user_id])->whereOr(['anchor_id' => $user_id])->delete();
            }

            require_once DOCUMENT_ROOT . '/system/redis/UserOnlineStateRedis.php';

            $user_online_redis = new UserOnlineStateRedis();
            $user_online_redis->change_state($user_id, $action);

            if($action == 'Logout'){

                $video_record = db("video_call_record")->whereOr('user_id', '=', $user_id)->whereOr('anchor_id', '=', $user_id) -> find();
                if($video_record){
                    db("video_call_record") -> delete($video_record['id']);

                    require_once DOCUMENT_ROOT . '/system/redis/VideoCallRedis.php';
                    $video_call_redis = new VideoCallRedis();
                    $video_call_redis->del_call($video_record['anchor_id']);
                    $video_call_redis->del_call($video_record['user_id']);

                }
            }
        }

        echo json_encode(['ActionStatus' => 'OK', 'ErrorCode' => 0, 'ErrorInfo' => '']);

    }

}