<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:60:"themes/admin_simpleboot3/admin/invite_manage/withdrawal.html";i:1536923216;s:43:"themes/admin_simpleboot3/public/header.html";i:1519953092;}*/ ?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="__TMPL__/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="__TMPL__/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="__STATIC__/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "__ROOT__/",
            WEB_ROOT: "__WEB_ROOT__/",
            JS_ROOT: "static/js/",
            APP: '<?php echo \think\Request::instance()->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="__TMPL__/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="__STATIC__/js/layer/layer.js" rel="stylesheet"></script>
    <script src="__STATIC__/js/wind.js"></script>
    <script src="__TMPL__/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip();
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
</head>
<body>
<div class="wrap js-check-wrap">
    <ul class="nav nav-tabs">
        <li class="active"><a href="javascript:;">充值记录</a></li>
    </ul>
    <form class="well form-inline margin-top-20" method="post" action="<?php echo url('InviteManage/withdrawal'); ?>">

        提现人ID:
        <input type="text" class="form-control" name="uid" style="width: 200px;"
               value="<?php echo (isset($request['uid']) && ($request['uid'] !== '')?$request['uid']:''); ?>" placeholder="请输入提现人ID...">
        状态：
        <select class="form-control" name="status" style="width: 140px;">
            <option value="-1">全部</option>
            <option value="0" <?php if($request['status'] == '0'): ?> selected='selected' <?php endif; ?> >未审核</option>
            <option value="1" <?php if($request['status'] == '1'): ?> selected='selected' <?php endif; ?> >已审核</option>
            <option value="2" <?php if($request['status'] == '2'): ?> selected='selected' <?php endif; ?> >拒绝</option>
        </select>
        时间:
        <input type="text" class="form-control js-bootstrap-datetime" name="start_time" value="<?php echo (isset($request['start_time']) && ($request['start_time'] !== '')?$request['start_time']:''); ?>" style="width: 140px;" autocomplete="off">-
        <input type="text" class="form-control js-bootstrap-datetime" name="end_time" value="<?php echo (isset($request['end_time']) && ($request['end_time'] !== '')?$request['end_time']:''); ?>" style="width: 140px;" autocomplete="off"> &nbsp; &nbsp;
        <input type="submit" class="btn btn-primary" value="搜索"/>
    </form>

    <form class="js-ajax-form" action="" method="post">
        <table class="table table-hover table-bordered table-list">
            <thead>
            <tr>
                <th>ID</th>
                <th>提现用户（ID）</th>
                <th>提现金额</th>
                <th>申请时间</th>
                <th>状态</th>
                <th>操作</th>
            </tr>
            </thead>
            <tfoot>
            <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): if( count($list)==0 ) : echo "" ;else: foreach($list as $key=>$vo): ?>
                <tr>
                    <td><?php echo $vo['id']; ?></td>
                    <td><?php echo $vo['user_nickname']; ?>(<?php echo $vo['uid']; ?>)</td>
                    <td><?php echo $vo['coin']; ?></td>
                    <td><?php echo date("Y-m-d H:i:s",$vo['addtime'] ); ?></td>
                    <td>
                        <?php if($vo['status'] == 1): ?>
                            已提现
                            <?php elseif($vo['status'] == 2): ?>
                            提现失败
                            <?php else: ?>
                            审核中
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if($vo['status'] == 0): ?>
                            <a href="javascript:void(0);" class="success_type" data-type="1" data-id="<?php echo $vo['id']; ?>">成功</a> |
                            <a href="javascript:void(0);" class="success_type" data-type="2" data-id="<?php echo $vo['id']; ?>">失败</a>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; endif; else: echo "" ;endif; ?>
            </tfoot>
        </table>
        <ul class="pagination"><?php echo (isset($page) && ($page !== '')?$page:''); ?></ul>
    </form>
</div>
<script src="__STATIC__/js/admin.js"></script>

<script>
    $(".success_type").click(function(){
        var type=$(this).attr("data-type");
        var id=$(this).attr("data-id");

        layer.confirm('确定要操作？', {
            btn: ['确定','取消'] //按钮
        }, function(){

            $.ajax({
                url: "<?php echo url('invite_manage/operation'); ?>",
                type: 'post',
                dataType: 'json',
                data:{id:id,type:type},
                success: function (data) {
                    layer.msg(data.msg,function(){
                        window.location.reload();
                    });

                }
            });

        });
    })
</script>
</body>
</html>