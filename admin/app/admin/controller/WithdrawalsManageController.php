<?php
/**
 * Created by PhpStorm.
 * User: weipeng
 * Date: 2018/3/18
 * Time: 23:06
 */

namespace app\admin\controller;


use cmf\controller\AdminBaseController;
class WithdrawalsManageController extends AdminBaseController
{

    public function index(){

        $where = [];
        if(isset($_REQUEST['mobile']) && $_REQUEST['mobile'] != ''){
            $where['u.mobile'] = $_REQUEST['mobile'];
        }else{

            $_REQUEST['mobile'] = '';
        }

        if(isset($_REQUEST['status']) && $_REQUEST['status'] != ''){
            $where['r.status'] = $_REQUEST['status'];
        }else{

            $_REQUEST['status'] = 0;
        }

        $list = db('user_cash_record')
            ->alias('r')
            ->join("user u","u.id=r.user_id")
            ->field('u.user_nickname,u.mobile,r.*')
            ->where($where)
            ->order("r.create_time DESC")
            ->paginate(20);
        //echo db() -> getLastSql();exit;

        $data = $list->toArray();

        $page = $list->render();
        //dump($data);exit;

        $config = load_cache('config');
        foreach ($data['data'] as &$v){
            $v['money'] = 0;
            if($v['income'] > 0){
                $v['money'] = number_format($v['income'] / $config['integral_withdrawal'],2);
            }
        }
        $this->assign('request',$_REQUEST);
        $this->assign('data',$data['data']);
        $this->assign('page', $page);
        return $this -> fetch();
    }

    //通过审核
    public function adopt_cash(){

        $id = input('param.id');

        db('user_cash_record') -> where('id','=',$id) -> setField('status',1);
        $this->success('操作成功');
    }

    //拒绝审核
    public function refuse_cash(){

        $id = input('param.id');

        $record = db('user_cash_record') -> where('id','=',$id) -> find();
        //返还提现金额
        db('user') -> where('id','=',$record['user_id']) -> setInc('income',$record['income']);
        //修改提现状态
        db('user_cash_record') -> where('id','=',$id) -> setField('status',2);
        $this->success('操作成功');
    }

    //删除
    public function del(){

        $id = input('param.id');

        db('user_cash_record') -> where('id','=',$id) -> delete();
        $this->success('操作成功');
    }
}