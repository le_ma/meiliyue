<?php

namespace app\api\controller;
use think\Db;
use think\config;
//引入七牛云的相关文件

class Base extends Common
{


    //判断传值是否是空
    public function empty_val($id){
        if(empty($id)){
            $data=array('msg'=>"传参错误！",'error'=>$id);
            save_log($data);
            $result['code'] = "0";
            $result['msg'] = "传参错误";
            $result['data'] = $id;
            return_json_encode($result);
        }
    }

}