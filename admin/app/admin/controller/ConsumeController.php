<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/2/8 0008
 * Time: 上午 10:55
 */

namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class ConsumeController extends AdminBaseController
{

    //消费记录
    public function index()
    {
        $where = [];
        if (!isset($_REQUEST['type']) || $_REQUEST['type'] == '-1' || $_REQUEST['type'] == 0) {
            $_REQUEST['type'] = '-1';
        } else {
            $where['a.type'] = $_REQUEST['type'];
        }

        if (isset($_REQUEST['uid']) && $_REQUEST['uid'] != '') {
            $where['a.user_id'] = intval($_REQUEST['uid']);
        }

        if (isset($_REQUEST['touid']) && $_REQUEST['touid'] != '') {
            $where['a.to_user_id'] = intval($_REQUEST['touid']);
        }

        $user = db("user_consume_log")
            ->alias("a")
            ->join("user b", "b.id=a.user_id")
            ->join("user c", "c.id=a.to_user_id")
            ->field("a.*,b.user_nickname as uname,c.user_nickname as toname")
            ->where($where)
            ->order('create_time desc')
            ->paginate(20);

        $lists = $user->toArray();
        $total = Db::name("user_consume_log")->sum('coin');

        $this->assign('total', $total);
        $this->assign('data', $lists['data']);
        $this->assign('request', $_REQUEST);
        $this->assign('page', $user->render());
        return $this->fetch();
    }

}