<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:52:"themes/admin_simpleboot3/user/admin_index/index.html";i:1534958727;s:43:"themes/admin_simpleboot3/public/header.html";i:1519953092;}*/ ?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="__TMPL__/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="__TMPL__/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="__STATIC__/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "__ROOT__/",
            WEB_ROOT: "__WEB_ROOT__/",
            JS_ROOT: "static/js/",
            APP: '<?php echo \think\Request::instance()->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="__TMPL__/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="__STATIC__/js/layer/layer.js" rel="stylesheet"></script>
    <script src="__STATIC__/js/wind.js"></script>
    <script src="__TMPL__/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip();
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
<style>
    #reference{    width: 100px;
        height: 32px;
        border-color: #dce4ec;
        color: #aeb5bb;}

    #user_status{    width: 100px;
        height: 32px;
        border-color: #dce4ec;
        color: #aeb5bb;}
    #sex{
        width: 100px;
        height: 32px;
        border-color: #dce4ec;
        color: #aeb5bb;}

    #order{
        width: 100px;
        height: 32px;
        border-color: #dce4ec;
        color: #aeb5bb;}
</style>
</head>
<body>
<div class="wrap">
    <ul class="nav nav-tabs">
        <li class="active"><a><?php echo lang('USER_INDEXADMIN_INDEX'); ?></a></li>
    </ul>
    <form class="well form-inline margin-top-20" method="post" action="<?php echo url('user/adminIndex/index'); ?>">
        用户ID：
        <input class="form-control" type="text" name="uid" style="width: 200px;" value="<?php echo input('request.uid'); ?>"
               placeholder="请输入用户ID">
        关键字：
        <input class="form-control" type="text" name="keyword" style="width: 200px;" value="<?php echo input('request.keyword'); ?>"
               placeholder="用户名/昵称/邮箱/手机号">

        账号状态：
        <select name="user_status" id="user_status">
            <option value="-1">全部</option>
            <option value="0" <?php if($request['user_status'] == 0): ?> selected="selected" <?php endif; ?>>禁用</option>
            <option value="1" <?php if($request['user_status'] == 1): ?> selected="selected" <?php endif; ?>>正常</option>
        </select>

        性别：
        <select name="sex" id="sex">
            <option value="-1">全部</option>
            <option value="1" <?php if($request['sex'] == 1): ?> selected="selected" <?php endif; ?>>男</option>
            <option value="2" <?php if($request['sex'] == 2): ?> selected="selected" <?php endif; ?>>女</option>
        </select>

        排序规则：
        <select name="order" id="order">
            <option value="-1">默认</option>
            <option value="1" <?php if($request['order'] == 1): ?> selected="selected" <?php endif; ?>>收益</option>
        </select>

        推荐状态：
        <select name="reference" id="reference">
            <option value="-1">全部</option>
            <option value="1" <?php if($request['reference'] == 1): ?> selected="selected" <?php endif; ?>>推荐</option>
            <option value="0" <?php if($request['reference'] == 0): ?> selected="selected" <?php endif; ?>>未推荐</option>
        </select>

        <input type="submit" class="btn btn-primary" value="搜索"/>
        <a class="btn btn-danger" href="<?php echo url('user/adminIndex/index'); ?>">清空</a>
    </form>
    <form method="post" class="js-ajax-form">
        <table class="table table-hover table-bordered">
            <thead>
            <tr>
                <th>ID</th>
                <th><?php echo lang('USERNAME'); ?></th>
                <th><?php echo lang('NICENAME'); ?></th>
                <th>性别</th>
                <th>自定义价格</th>
                <th><?php echo lang('AVATAR'); ?></th>
                <th>手机</th>
                <th>余额</th>
                <th>收益</th>
                <th>累计收益</th>
                <th><?php echo lang('REGISTRATION_TIME'); ?></th>
                <th><?php echo lang('LAST_LOGIN_TIME'); ?></th>
                <th><?php echo lang('LAST_LOGIN_IP'); ?></th>
                <th><?php echo lang('STATUS'); ?></th>
                <th><?php echo lang('ACTIONS'); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php 
                $user_statuses=array("0"=>lang('USER_STATUS_BLOCKED'),"1"=>lang('USER_STATUS_ACTIVATED'),"2"=>lang('USER_STATUS_ACTIVATED'));
             if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): if( count($list)==0 ) : echo "" ;else: foreach($list as $key=>$vo): ?>
                <tr>
                    <td><?php echo $vo['id']; ?></td>

                    <?php if(IS_TEST == 1): ?><td>测试模式，敏感数据不予显示！</td><?php else: ?><td><?php echo !empty($vo['user_login'])?$vo['user_login']:($vo['mobile']?$vo['mobile']:lang('THIRD_PARTY_USER')); ?></td><?php endif; ?>


                    <td><?php echo !empty($vo['user_nickname'])?$vo['user_nickname']:lang('NOT_FILLED'); ?></td>
                    <td><?php echo $vo['sex']==1?'男' : '女'; ?></td>
                    <td><?php echo $vo['custom_video_charging_coin']; ?></td>
                    <td><img width="25" height="25" src="<?php echo url('user/public/avatar',array('id'=>$vo['id'])); ?>"/></td>
                    <?php if(IS_TEST == 1): ?><td>测试模式，敏感数据不予显示！</td><?php else: ?><td><?php echo $vo['mobile']; ?></td><?php endif; ?>
                    <td><?php echo $vo['coin']; ?></td>
                    <td><?php echo $vo['income']; ?></td>
                    <td><?php echo $vo['income_total']; ?></td>
                    <td><?php echo date('Y-m-d H:i:s',$vo['create_time']); ?></td>
                    <td><?php echo date('Y-m-d H:i:s',$vo['last_login_time']); ?></td>
                    <td><?php echo $vo['last_login_ip']; ?></td>
                    <td><?php echo $user_statuses[$vo['user_status']]; ?></td>
                    <td>
                        <?php if($vo['id'] != '1'): if(empty($vo['user_status']) || (($vo['user_status'] instanceof \think\Collection || $vo['user_status'] instanceof \think\Paginator ) && $vo['user_status']->isEmpty())): ?>
                                <a href="<?php echo url('adminIndex/cancelban',array('id'=>$vo['id'])); ?>"
                                   class="js-ajax-dialog-btn"
                                   data-msg="<?php echo lang('ACTIVATE_USER_CONFIRM_MESSAGE'); ?>"><?php echo lang('ACTIVATE_USER'); ?></a>
                                <?php else: ?>
                                <a href="<?php echo url('adminIndex/ban',array('id'=>$vo['id'])); ?>" class="js-ajax-dialog-btn"
                                   data-msg="<?php echo lang('BLOCK_USER_CONFIRM_MESSAGE'); ?>"><?php echo lang('BLOCK_USER'); ?></a>
                            <?php endif; else: ?>
                            <a style="color: #ccc;"><?php echo lang('BLOCK_USER'); ?></a>
                        <?php endif; if($vo['id'] != 1): ?> |
                        <?php if($vo['reference'] == 0): ?>
                            <a href="<?php echo url('adminIndex/reference',array('id'=>$vo['id'],'type'=>1)); ?>">推 荐</a>
                        <?php else: ?>
                            <a href="<?php echo url('adminIndex/reference',array('id'=>$vo['id'],'type'=>0)); ?>">取 消 推 荐</a>
                        <?php endif; endif; ?>
                        <a href="#" onclick="account(<?php echo $vo['id']; ?>)">账户管理</a>
                        <a href="<?php echo url('admin_index/edit',array('id'=>$vo['id'])); ?>">编辑</a>
                    </td>
                </tr>
            <?php endforeach; endif; else: echo "" ;endif; ?>
            </tbody>
        </table>
        <div class="pagination"><?php echo $page; ?></div>
    </form>
</div>
<script src="__STATIC__/js/admin.js"></script>
<script src="__STATIC__/js/layer/layer.js" rel="stylesheet"></script>
<script type="text/javascript">
    
    function account(id) {
        layer.prompt({title: '请输入充值金额', formType: 0}, function(coin, index){
            $.ajax({
                url: "<?php echo url('admin_index/account'); ?>",
                type: 'get',
                dataType: 'json',
                data: {id: id,coin:coin},
                success: function (data) {
                    if(data['code'] == 1){
                        layer.msg("操作成功",{time: 2000, icon:1},function(){
                            window.location.reload();
                        });
                    }else{
                        layer.msg("操作失败",{time: 2000, icon:2});
                    }
                }
            });
            layer.close(index);
        });
    }


</script>
</body>
</html>