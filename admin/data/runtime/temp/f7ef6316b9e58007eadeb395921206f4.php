<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:62:"themes/admin_simpleboot3/admin/invite_manage/income_index.html";i:1532530226;s:43:"themes/admin_simpleboot3/public/header.html";i:1519953092;}*/ ?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="__TMPL__/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="__TMPL__/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="__STATIC__/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "__ROOT__/",
            WEB_ROOT: "__WEB_ROOT__/",
            JS_ROOT: "static/js/",
            APP: '<?php echo \think\Request::instance()->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="__TMPL__/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="__STATIC__/js/layer/layer.js" rel="stylesheet"></script>
    <script src="__STATIC__/js/wind.js"></script>
    <script src="__TMPL__/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip();
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
</head>
<body>
<div class="wrap js-check-wrap">
    <ul class="nav nav-tabs">
        <li class="active"><a href="javascript:;">充值记录</a></li>
    </ul>
    <form class="well form-inline margin-top-20" method="post" action="<?php echo url('InviteManage/income_index'); ?>">

        邀请人ID:
        <input type="text" class="form-control" name="user_id" style="width: 200px;"
               value="<?php echo (isset($request['user_id']) && ($request['user_id'] !== '')?$request['user_id']:''); ?>" placeholder="请输入被邀请人ID...">
        被邀请人ID:
        <input type="text" class="form-control" name="invite_user_id" style="width: 200px;"
               value="<?php echo (isset($request['invite_user_id']) && ($request['invite_user_id'] !== '')?$request['invite_user_id']:''); ?>" placeholder="请输入被邀请人ID...">
        邀请码:
        <input type="text" class="form-control" name="invite_code" style="width: 200px;"
               value="<?php echo (isset($request['invite_code']) && ($request['invite_code'] !== '')?$request['invite_code']:''); ?>" placeholder="请输入邀请码...">
        <input type="submit" class="btn btn-primary" value="搜索"/>
    </form>
    <form class="js-ajax-form" action="" method="post">
        <table class="table table-hover table-bordered table-list">
            <thead>
            <tr>
                <th>ID</th>
                <th>邀请用户（ID）</th>
                <th>被邀请用户（ID）</th>
                <th>收益数量</th>
                <th>邀请码</th>
                <th>收益时间</th>
            </tr>
            </thead>
            <tfoot>
            <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): if( count($list)==0 ) : echo "" ;else: foreach($list as $key=>$vo): ?>
                <tr>
                    <td><?php echo $vo['id']; ?></td>
                    <td><?php echo $vo['user_nickname']; ?>(<?php echo $vo['user_id']; ?>)</td>
                    <td><?php echo $vo['invite_user_nickname']; ?>(<?php echo $vo['invite_user_id']; ?>)</td>
                    <td><?php echo $vo['income']; ?></td>
                    <td><?php echo $vo['invite_code']; ?></td>
                    <td><?php echo date("Y-m-d H:i:s",$vo['create_time'] ); ?></td>

                </tr>
            <?php endforeach; endif; else: echo "" ;endif; ?>
            </tfoot>
        </table>
        <ul class="pagination"><?php echo (isset($page) && ($page !== '')?$page:''); ?></ul>
    </form>
</div>
<script src="__STATIC__/js/admin.js"></script>

</body>
</html>