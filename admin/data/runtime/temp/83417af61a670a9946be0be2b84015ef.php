<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:47:"themes/admin_simpleboot3/admin/agent/index.html";i:1532318374;s:43:"themes/admin_simpleboot3/public/header.html";i:1519953092;}*/ ?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="__TMPL__/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="__TMPL__/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="__STATIC__/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "__ROOT__/",
            WEB_ROOT: "__WEB_ROOT__/",
            JS_ROOT: "static/js/",
            APP: '<?php echo \think\Request::instance()->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="__TMPL__/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="__STATIC__/js/layer/layer.js" rel="stylesheet"></script>
    <script src="__STATIC__/js/wind.js"></script>
    <script src="__TMPL__/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip();
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
</head>
<body>
<div class="wrap js-check-wrap">
    <ul class="nav nav-tabs">
        <li class="active"><a href="<?php echo url('agent/index'); ?>">代理</a></li>
        <li><a href="<?php echo url('agent/add'); ?>">添加代理</a></li>
    </ul>
    <form class="well form-inline margin-top-20" method="post" action="<?php echo url('agent/index'); ?>">
        代理账号:
        <input type="text" class="form-control" name="agent_login" style="width: 120px;" value="<?php echo (isset($data['agent_login']) && ($data['agent_login'] !== '')?$data['agent_login']:''); ?>" placeholder="请输入账号">
        代理id:
        <input type="text" class="form-control" name="agent_id" style="width: 120px;" value="<?php echo (isset($data['agent_id']) && ($data['agent_id'] !== '')?$data['agent_id']:''); ?>" placeholder="请输入代理id">
        上级代理id:
        <input type="text" class="form-control" name="superior_id" style="width: 120px;" value="<?php echo (isset($data['superior_id']) && ($data['superior_id'] !== '')?$data['superior_id']:''); ?>" placeholder="请输入代理id">
        代理级别：
        <select class="form-control" name="agent_level" style="width: 140px;">
            <option value="0">全部</option>
            <option value="1"  <?php if($data['agent_level'] == 1): ?> selected='selected' <?php endif; ?>>一级</option>
            <option value="2"  <?php if($data['agent_level'] == 2): ?> selected='selected' <?php endif; ?>>二级</option>
            <option value="3"  <?php if($data['agent_level'] == 3): ?> selected='selected' <?php endif; ?>>三级</option>
        </select>
        <input type="submit" class="btn btn-primary" value="搜索" />
        <a class="btn btn-danger" href="<?php echo url('agent/index'); ?>">清空</a>
    </form>
    <table class="table table-hover table-bordered">
        <thead>
        <tr>
            <th width="50">ID</th>
            <th><?php echo lang('USERNAME'); ?></th>
            <th><?php echo lang('LAST_LOGIN_IP'); ?></th>
            <th><?php echo lang('LAST_LOGIN_TIME'); ?></th>
            <th>注册总数</th>
            <th>充值总金额</th>
            <th><?php echo lang('STATUS'); ?></th>
            <th>上级代理（ID）</th>
            <th width="130"><?php echo lang('ACTIONS'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php $user_statuses=array("0"=>'账号已封',"1"=>'正常'); if(is_array($users) || $users instanceof \think\Collection || $users instanceof \think\Paginator): if( count($users)==0 ) : echo "" ;else: foreach($users as $key=>$vo): ?>
            <tr>
                <td><?php echo $vo['id']; ?></td>
                <td><?php echo $vo['agent_login']; ?></td>
                <td><?php echo $vo['last_login_ip']; ?></td>
                <td>
                    <?php if($vo['last_login_time'] == 0): ?>
                        <?php echo lang('USER_HAVE_NOT_LOGIN'); else: ?>
                        <?php echo date('Y-m-d H:i:s',$vo['last_login_time']); endif; ?>
                </td>
                <td><?php echo (isset($vo['registered'] ) && ($vo['registered']  !== '')?$vo['registered'] :'0'); ?> 人</td>
                <td><?php echo (isset($vo['money'] ) && ($vo['money']  !== '')?$vo['money'] :'0'); ?> 元</td>
                <td><?php echo $user_statuses[$vo['status']]; ?></td>
                <td><?php echo $vo['agent_user']; ?> (<?php echo $vo['agent_id']; ?>)</td>
                <td>
                        <a href='<?php echo url("agent/edit",array("id"=>$vo["id"])); ?>'><?php echo lang('EDIT'); ?></a>
                        <a class="js-ajax-delete" href="<?php echo url('agent/delete',array('id'=>$vo['id'])); ?>"><?php echo lang('DELETE'); ?></a>
                        <?php if($vo['status'] == 1): ?>
                            <a href="<?php echo url('agent/ban',array('id'=>$vo['id'])); ?>" class="js-ajax-dialog-btn" data-msg="<?php echo lang('确定要封号'); ?>">封号</a>
                            <?php else: ?>
                            <a href="<?php echo url('agent/cancelban',array('id'=>$vo['id'])); ?>" class="js-ajax-dialog-btn" data-msg="<?php echo lang('确定解封号'); ?>">解封</a>
                        <?php endif; ?>
                         <a  href="<?php echo url('agent/index',array('superior_id'=>$vo['id'])); ?>">下级详情</a>
                        <a  href="<?php echo url('agent/registered',array('userid'=>$vo['id'])); ?>">注册详情</a>

                </td>
            </tr>
        <?php endforeach; endif; else: echo "" ;endif; ?>
        </tbody>
    </table>
    <div class="pagination"><?php echo $page; ?></div>
</div>
<script src="__STATIC__/js/admin.js"></script>
</body>
</html>