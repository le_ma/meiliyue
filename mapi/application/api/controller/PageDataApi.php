<?php
/**
 * Created by PhpStorm.
 * User: weipeng
 * Date: 2018/3/3
 * Time: 10:59
 */

namespace app\api\controller;


use think\helper\Time;
use UserOnlineStateRedis;

class PageDataApi
{

    //首页推荐用户
    public function recommend_user()
    {

        $result = array('code' => 1, 'msg' => '', 'data' => array());
        $page = intval(input('param.page'));
        $uid = intval(input('param.uid'));

        $user_info = get_user_base_info($uid);
        $sex = 1;
        if ($user_info['sex'] == 1) {
            $sex = 2;
        }

        $config = load_cache('config');
        $video = db('user')
            ->alias('a')
            ->field('a.id,a.sex,a.user_nickname,a.avatar,a.level')
            ->join('monitor m', 'a.id=m.user_id', 'left')
            ->where("a.user_status!=0 and a.sex=$sex and a.reference=1 and a.is_online=1")
            ->order("m.monitor_time desc")
            ->page($page)
            ->select();

        foreach ($video as &$v) {
            //好评百分比
            $v['evaluation'] = db('video_call_record_log')->where('anchor_id', '=', $v['id'])->where('is_fabulous', '=', 1)->count();
            $level = get_level($v['id']);
            $v['level_name'] = $level;
            //获取主播是否登录
            $v['is_online'] = is_online($v['id'], $config['heartbeat_interval']);
        }

        $result['online_emcee_count'] = 0;
        $result['online_emcee'] = [];
        //获取在线主播人数
//        if ($page == 1) {
//
//
//        }
        require_once DOCUMENT_ROOT . '/system/redis/UserOnlineStateRedis.php';
        $result['online_emcee'] = db('user')->alias('u')->field('u.id,u.avatar')->where('u.is_online', '=', 1)->where('u.sex', '=', 2)->limit(3)->select();
        $user_online_state_redis = new UserOnlineStateRedis();
        $result['online_emcee_count'] = $user_online_state_redis->get_female_online_count();
        $result['data'] = $video;
        return_json_encode($result);
    }

    //首页轮播图
    public function shuffling()
    {

        $result = array('code' => 0, 'msg' => '', 'data' => array());
        $shuffling = intval(input('param.shuffling'));

        $img = db('slide_item')->where("slide_id=$shuffling and status=1")->order("list_order desc")->field('id,image,title,url')->select();
        if ($img) {
            $result['code'] = '1';
            $result['data'] = $img;
        } else {
            $result['msg'] = "暂无图片";
        }

        return_json_encode($result);
    }


    //搜索
    public function request_search()
    {

        $result = array('code' => 1, 'msg' => '');
        $key_word = trim(input('param.key_word'));
        $uid = intval(input('param.uid'));

        $result['list'] = db('user')->field('avatar,id,user_nickname,address')
            ->where('id', '=', intval($key_word))->whereOr('user_nickname', '=', $key_word)->select();

        return_json_encode($result);

    }

    //获取评价标签列表
    public function request_get_evaluate_list()
    {
        $result = array('code' => 1, 'msg' => '', 'list' => array());
        $result['list'] = db('evaluate_label')->select();
        return_json_encode($result);
    }

    //获取我关注的主播
    public function request_get_follow_emcee_list()
    {

        $result = array('code' => 1, 'msg' => '', 'data' => array());
        $page = intval(input('param.page'));
        $uid = intval(input('param.uid'));

        //获取关注列表id
        $attention_user = db('user_attention')->where("uid=$uid")->page($page)->select();
        if (!$attention_user) {

            return_json_encode($result);
        }

        $attention_ids = array();
        array_walk($attention_user, function ($value, $key) use (&$attention_ids) {
            $attention_ids[] = $value['attention_uid'];
        });

        $user_list = db('user')
            ->alias('u')
            ->field('u.id,u.user_nickname,u.sex,u.user_status,u.avatar,u.address,u.is_online')
            //->where('u.sex', '=', 2)
            ->where('u.is_online', '=', 1)
            ->where('u.id', 'in', $attention_ids)
            ->order('u.income,u.coin desc')
            ->page($page)
            ->select();

        $result['data'] = emcee_complete($user_list);
        return_json_encode($result);

    }

    //获取星级主播列表
    public function request_get_star_emcee_list()
    {

        $result = array('code' => 1, 'msg' => '', 'data' => array());
        $level_id = intval(input('param.level_id'));
        $page = intval(input('param.page'));

        //查询等级
        $level = db('user_star_level')->find($level_id);

        $user_list = db('user')
            ->alias('u')
            ->field('u.id,u.user_nickname,u.sex,u.user_status,u.avatar,u.address')
            ->where('u.income_total', '>=', $level['min_income'])
            ->where('u.income_total', '<=', $level['max_income'])
            ->where('u.sex', '=', 2)
            ->order('u.income desc')
            ->page($page)
            ->select();

        $result['data'] = emcee_complete($user_list);
        return_json_encode($result);
    }

    //首页在线
    public function request_get_index_online()
    {

        $result = array('code' => 1, 'msg' => '', 'data' => array());
        $page = intval(input('param.page'));
        $uid = intval(input('param.uid'));

        $user_info = get_user_base_info($uid);
        $sex = 1;
        if ($user_info['sex'] == 1) {
            $sex = 2;
        }

        if ($sex == 1) {

            $user_list = db('user')
                ->alias('u')
                ->field('u.id,u.sex,u.user_nickname,u.avatar,u.level,u.is_online')
                ->where("u.user_status!=0 and u.is_online=1 and u.sex=$sex")
                ->order("u.level desc")
                ->page($page)
                ->select();
        } else {

            $user_list = db('user')
                ->alias('u')
                ->join('auth_form_record a', 'a.user_id=u.id')
                ->field('u.id,u.sex,u.user_nickname,u.avatar,u.level,u.is_online')
                ->where("u.user_status!=0 and u.is_online=1 and u.sex=$sex and a.status=1")
                ->order("u.level desc")
                ->page($page)
                ->select();
        }

        $result['data'] = emcee_complete($user_list);;
        return_json_encode($result);
    }

    //视频聊页面其他数据
    public function request_get_video_chat_page_data()
    {

        $result = array('code' => 1, 'msg' => '', 'data' => array());

        $img = db('slide_item')->where("slide_id=4 and status=1")->order("list_order desc")->field('id,image,title,url')->select();
        $result['slide'] = $img;

        return_json_encode($result);
    }


    //获取财富页相关信息
    public function get_wealth_page_info()
    {

        $result = array('code' => 1, 'msg' => '', 'data' => array());
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token, ['score', 'coin_system', 'income']);

        if (!$user_info) {
            $result['code'] = 10001;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        //用户聊币月
        $result['coin'] = $user_info['coin'];

        $config = load_cache('config');
        //分成比例
        $result['split'] = ($config['invite_income_ratio'] * 100);
        //总收益
        $result['income'] = $user_info['income'];

        return_json_encode($result);
    }

    //获取视频页面信息
    public function get_video_page_info()
    {

        $result = array('code' => 1, 'msg' => '', 'data' => array());
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));
        $video_id = intval(input('param.video_id'));

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token);

        if (!$user_info) {
            $result['code'] = 10001;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        $result['user_info'] = get_user_base_info($video_id);

        return_json_encode($result);
    }

    //获取礼物列表
    public function get_gift_list()
    {
        $result = array('code' => 1, 'msg' => '', 'data' => array());

        $gift_list = load_cache('gift');
        $result['list'] = $gift_list;
        return_json_encode($result);
    }


    //新人列表
    public function get_news_user_list()
    {

        $result = array('code' => 1, 'msg' => '', 'data' => array());
        $uid = intval(input('param.uid'));
        $page = intval(input('param.page'));

        $user_info = get_user_base_info($uid);

        $time = Time::dayToNow(6);

        if ($user_info['sex'] == 1) {
            $user_list = db('user')
                ->alias('u')
                ->join('auth_form_record a', 'a.user_id=u.id')
                ->where('u.create_time', '>', $time[0])
                ->where('u.sex', '=', 2)
                ->where('a.status', '=', 1)
                ->where('u.is_online', '=', 1)
                ->field('u.id,u.user_nickname,u.sex,u.avatar,u.address,a.status')
                ->order('u.create_time desc')
                ->page($page, 20)
                ->select();
        } else {
            $user_list = db('user')
                ->alias('u')
                ->where('u.create_time', '>', $time[0])
                ->where('u.sex', '=', 1)
                ->where('u.is_online', '=', 1)
                ->field('u.id,u.user_nickname,u.sex,u.avatar,u.address')
                ->order('u.create_time desc')
                ->page($page, 20)
                ->select();
        }

        $result['data'] = emcee_complete($user_list);

        return_json_encode($result);
    }


    //我的收益
    public function get_user_income_page_info()
    {

        $result = array('code' => 1, 'msg' => '');
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));
        $page = intval(input('param.page'));

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token, ['score', 'coin_system', 'income']);

        if (!$user_info) {
            $result['code'] = 10001;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        $config = load_cache('config');

        $result['income'] = $user_info['income'];
        $result['money'] = 0;
        if ($result['income'] > 0) {
            $result['money'] = number_format($result['income'] / $config['integral_withdrawal'], 2);
        }

        $result['list'] = db('user_cash_record')->where('user_id', '=', $uid)->page($page)->order('create_time desc')->select();

        foreach ($result['list'] as &$v) {
            $v['create_time'] = date('Y年m月d日', $v['create_time']);

            if ($v['status'] == 0) {
                $v['status'] = '审核中';
            } else if ($v['status'] == 1) {
                $v['status'] = '提现成功';
            } else {
                $v['status'] = '提现失败，请联系客服！';
            }
        }
        return_json_encode($result);
    }

    //举报类型
    public function get_report_type()
    {

        $result = array('code' => 1, 'msg' => '', 'list' => []);

        $result['list'] = db('user_report_type')->select();
        return_json_encode($result);

    }

    //加盟合作
    public function join_in()
    {

        $result = array('code' => 1, 'msg' => '');
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));
        $type = trim(input('param.type'));//加盟方式
        $contact = trim(input('param.contact'));//联系方式

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token, ['score', 'coin_system', 'income']);

        if (!$user_info) {
            $result['code'] = 10001;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        $is_exits = db('join_in')->where('user_id', '=', $uid)->find();
        if ($is_exits) {
            $result['msg'] = '已经提交,请耐心等待';
            $result['code'] = 0;
            return_json_encode($result);
        }

        $data = ['type' => $type, 'contact' => $contact, 'user_id' => $uid, 'create_time' => NOW_TIME];
        db('join_in')->insert($data);

        return_json_encode($result);

    }

    //黑名单
    public function black_list()
    {

        $result = array('code' => 1, 'msg' => '');
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));
        $p = intval(input('param.page'));

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token);
        if (!$user_info) {
            $result['code'] = 10001;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        $list = db('user_black')
            ->alias('b')
            ->field('u.user_nickname,u.id,u.avatar')
            ->join('user u', 'u.id=b.black_user_id')
            ->where('b.user_id', '=', $uid)
            ->page($p)
            ->select();

        $result['list'] = $list;

        return_json_encode($result);
    }


    //获取在线推荐用户
    public function get_online_user()
    {

        $result = array('code' => 1, 'msg' => '');
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token);
        if (!$user_info) {
            $result['code'] = 10001;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        //账号是否被禁用
        if ($user_info['user_status'] == 0) {
            $result['code'] = 0;
            $result['msg'] = "您因涉嫌违规，账号受限，请联系管理员!";
            return_json_encode($result);
        }

        $config = load_cache('config');
        //时间
        $time = NOW_TIME - $config['heartbeat_interval'] - 5;//偏移量5秒

        //查询在线的三个用户
        $online_user = db('monitor')
            ->alias('m')
            ->field('u.id,u.user_nickname,u.avatar,u.sex')
            ->join('user u', 'm.user_id=u.id')
            ->where('monitor_time', '>', $time)
            ->where('m.user_id', 'neq', $uid)
            ->where('u.sex', '=', 1)
            ->limit(3)
            ->select();

        foreach ($online_user as &$v) {
            $v['is_follow'] = 0;
            $focus = db('user_attention')->where("uid=$uid and attention_uid=" . $v['id'])->find();
            if ($focus) {
                $v['is_follow'] = 1;
            }
            $v['is_online'] = is_online($v['id'], $config['heartbeat_interval']);               //获取主播是否登录
        }

        $result['list'] = $online_user;
        $result['online_count'] = db('monitor')->where('monitor_time', '>', $time)->count();

        return_json_encode($result);

    }


//首页消息
    public function get_msg_user()
    {

        $result = array('code' => 1, 'msg' => '');
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token);
        if (!$user_info) {
            $result['code'] = 10001;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        //账号是否被禁用
        if ($user_info['user_status'] == 0) {
            $result['code'] = 0;
            $result['msg'] = "您因涉嫌违规，账号受限，请联系管理员!";
            return_json_encode($result);
        }

        $config = load_cache('config');
//        //时间
//        $time = NOW_TIME - $config['heartbeat_interval'] - 5;//偏移量5秒

        //1消息，2好友，3关注，4粉丝，5添加
        $type=intval(input('param.type'));

        if ($type==1){//消息
            //邀约消息
            $meeting_time=db('meeting')->where(['work_uid'=>$uid])->field('create_time')->order('create_time desc')->find();
            if (!empty($meeting_time)){
                $meeting_time_day=date('d',$meeting_time['create_time']);
                if ($meeting_time_day==date('d',time())){
                    $meeting_time['create_time']="今天"." ".date('H:i',$meeting_time['create_time']);
                }elseif ($meeting_time_day+1==date('d',time())){
                    $meeting_time['create_time']="昨天"." ".date('H:i',$meeting_time['create_time']);
                }else{
                    $meeting_time['create_time']=date('m/d H:i',$meeting_time['create_time']);
                }
            }

//            $result['msg']['invite']=empty($meeting_time)?array():$meeting_time;

            //系统消息
            $message_data=db('user_message')->where(['uid'=>$uid])->order('id desc')->find();

            if (!empty($message_data)){
                $message_day=date('d',$message_data['addtime']);

                if ($message_day==date('d',time())){
                    $message_data['addtime']="今天"." ".date('H:i',$message_data['addtime']);
                }elseif ($message_day+1==date('d',time())){
                    $message_data['addtime']="昨天"." ".date('H:i',$message_data['addtime']);
                }else{
                    $message_data['addtime']=date('m/d H:i',$message_data['addtime']);
                }
            }
            $arr=[
                'invite'=>empty($meeting_time)?array():$meeting_time,
                'site'=>empty($message_data)?array():$message_data
            ];

            $result['message']=$arr;
        }elseif ($type==2){//好友
            $user_friend=db('friend')->where(['uid'=>$uid,'status'=>1])->field('puid')->select();
            if (!empty($user_friend)){
                foreach ($user_friend as $key=>$val){
                    $user_info=db('user')->where(['id'=>$val['puid']])->field('id,user_nickname,avatar')->find();
                    $user_friend[$key]['user_nickname']=$user_info['user_nickname'];
                    $user_friend[$key]['avatar']=$user_info['avatar'];
                }
            }
            $result['friend']=empty($user_friend)?array():$user_friend;
        }elseif ($type==3){//关注
            $follow=db('user_attention')->where(['uid'=>$uid])->field('attention_uid')->select();
            if (!empty($follow)){
                foreach ($follow as $key=>$value){
                    $attention_info=db('user')->where(['id'=>$value['attention_uid']])->field('user_nickname,avatar')->find();
                    $follow[$key]['avatar']=$attention_info['avatar'];
                    $follow[$key]['user_nickname']=$attention_info['user_nickname'];
                }
            }
            $result['attention']=empty($follow)?array():$follow;
        }elseif ($type==4){//粉丝
            $attention=db('user_attention')->where(['attention_uid'=>$uid])->field('uid')->select();
            if (!empty($attention)){
                foreach ($attention as $key=>$value){
                    $attention_info=db('user')->where(['id'=>$value['uid']])->field('user_nickname,avatar')->find();
                    $attention[$key]['avatar']=$attention_info['avatar'];
                    $attention[$key]['user_nickname']=$attention_info['user_nickname'];
                }
            }
            $result['fans']=empty($attention)?array():$attention;
        }elseif ($type==5){//添加好友
            $value=$type=intval(input('param.value'));//搜索条件
//            $condition['user_nickname'] = $value;
//            $condition['id'] = $value;
//            $condition['_logic'] = 'OR';
            $user_data=db('user')->where("user_nickname='$value' OR id='$value'")->field('id,user_nickname,avatar')->select();

            $result['addlist']=empty($user_data)?array():$user_data;
        }

        return_json_encode($result);

    }

    //首页我的主页
    public function get_my_home()
    {

        $result = array('code' => 1, 'msg' => '');
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token);
        if (!$user_info) {
            $result['code'] = 10001;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        //账号是否被禁用
        if ($user_info['user_status'] == 0) {
            $result['code'] = 0;
            $result['msg'] = "您因涉嫌违规，账号受限，请联系管理员!";
            return_json_encode($result);
        }
        $user_data=db('user')->where(['id'=>$uid])->field('id,user_nickname,avatar')->find();
        $result['user_info']=$user_data;
        return_json_encode($result);

    }

    //首页发现
    public function get_find_info()
    {

        $result = array('code' => 1, 'msg' => '');
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));
        $type = trim(input('param.type'));
        $topic_type = trim(input('param.topic_type_id'));
        $p = trim(input('param.p'));
        $listRows = trim(input('param.listRows'));

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token);
        if (!$user_info) {
            $result['code'] = 10001;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        //账号是否被禁用
        if ($user_info['user_status'] == 0) {
            $result['code'] = 0;
            $result['msg'] = "您因涉嫌违规，账号受限，请联系管理员!";
            return_json_encode($result);
        }

        if ($type==1){//广场
            $guangchang="";
            $find_data=db('find')->where($guangchang)->limit(($p-1)*$listRows,$listRows)->select();
            if (!empty($find_data)){
                foreach ($find_data as &$val){
                    $user=db('user')->where(['id'=>$val['uid']])->find();
                    $val['avatar']=$user['avatar'];
                    $val['user_nickname']=$user['user_nickname'];
                    $val['sex']=$user['sex'];
                    $val['age']=date('Y',time())-date('Y',$user['birthday']);
                    $comment_count=db('find_comment')->where(['type'=>1,'work_id'=>$val['id']])->count();
                    $val['comment_count']=empty($comment_count)?0:$comment_count;
                    $flower_count=db('find_flower')->where(['work_id'=>$val['id']])->count();
                    $val['flower_count']=empty($flower_count)?0:$flower_count;
                }
                unset($val);
            }
            $result['date']=empty($find_data)?array():$find_data;
        }elseif ($type==2){//关注
            $follow=db('user_attention')->where(['uid'=>$uid])->field('attention_uid')->select();

            if (!empty($follow)){
                foreach ($follow as $vol){
                    $follow_user[]=$vol['attention_uid'];
                }
                $guanzhu_map['uid']=array('in',$follow_user);
                $find_data=db('find')->where($guanzhu_map)->limit(($p-1)*$listRows,$listRows)->select();
            }else{
                $find_data=db('find')->limit(($p-1)*$listRows,$listRows)->select();
            }


            if (!empty($find_data)){
                foreach ($find_data as &$val){
                    $user=db('user')->where(['id'=>$val['uid']])->find();
                    $val['avatar']=$user['avatar'];
                    $val['user_nickname']=$user['user_nickname'];
                    $val['sex']=$user['sex'];
                    $val['age']=date('Y',time())-date('Y',$user['birthday']);
                    $comment_count=db('find_comment')->where(['type'=>1,'work_id'=>$val['id']])->count();
                    $val['comment_count']=empty($comment_count)?0:$comment_count;
                    $flower_count=db('find_flower')->where(['work_id'=>$val['id']])->count();
                    $val['flower_count']=empty($flower_count)?0:$flower_count;
                }
                unset($val);
            }

            $result['date']=empty($find_data)?array():$find_data;
        }elseif ($type==3){//话题关注
            $user_topic_attention=db('topic_attention')->where(['uid'=>$uid])->select();
            if (empty($user_topic_attention)){
                $topic_attention=db('topic')->field('id,title')->order('id desc')->limit(($p-1)*$listRows,$listRows)->select();
            }else{
                foreach ($user_topic_attention as $vl){
                    $attention_ids[]=$vl['topic_id'];
                }
                $where['id']=array('in',$attention_ids);
                $topic_attention=db('topic')->field('id,title')->where($where)->limit(($p-1)*$listRows,$listRows)->select();
            }

            if (!empty($topic_attention)){
                foreach ($topic_attention as $k=>$val){
                    $count=db('topic_see')->where(['topic_id'=>$val['id']])->count();
                    $topic_attention[$k]['count']=empty($count)?0:$count;
                }
            }
            $result['date']=empty($topic_attention)?array():$topic_attention;
        }elseif ($type==4){//话题广场
            if (!empty($topic_type)){
                if ($topic_type=="newest"){
                    $order="create_time desc,id desc";

                    $topic_attention=db('topic')->field('id,title')->order($order)->limit(($p-1)*$listRows,$listRows)->select();

                    if (!empty($topic_attention)){
                        foreach ($topic_attention as $k=>$val){
                            $count=db('topic_see')->where(['topic_id'=>$val['id']])->count();
                            $topic_attention[$k]['count']=empty($count)?0:$count;
                        }
                    }
                    $result['date']=empty($topic_attention)?array():$topic_attention;

                }elseif ($topic_type=="hot"){
//                    $field="count(*) num,topic_id ids";
//                    $group="topic_id";
//                    $topic_see_data=db('topic_see')->field($field)->group($group)->order('num desc')->select();

                    $topic_attention=db('topic')->field('id,title')->order('commend_count desc')->limit(($p-1)*$listRows,$listRows)->select();

                    if (!empty($topic_attention)){
                        foreach ($topic_attention as $k=>$val){
                            $count=db('topic_see')->where(['topic_id'=>$val['id']])->count();
                            $topic_attention[$k]['count']=empty($count)?0:$count;
                        }
                    }
                    $result['date']=empty($topic_attention)?array():$topic_attention;

                }else{
                    $map['type']=$topic_type;

                    $topic_attention=db('topic')->where($map)->field('id,title')->order('id asc')->limit(($p-1)*$listRows,$listRows)->select();

                    if (!empty($topic_attention)){
                        foreach ($topic_attention as $k=>$val){
                            $count=db('topic_see')->where(['topic_id'=>$val['id']])->count();
                            $topic_attention[$k]['count']=empty($count)?0:$count;
                        }
                    }
                    $result['date']=empty($topic_attention)?array():$topic_attention;
                }
            }


        }

        return_json_encode($result);

    }

    //邀约首页分类
    public function get_topic_class()
    {

        $result = array('code' => 1, 'msg' => '');
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token);
        if (!$user_info) {
            $result['code'] = 10001;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        //账号是否被禁用
        if ($user_info['user_status'] == 0) {
            $result['code'] = 0;
            $result['msg'] = "您因涉嫌违规，账号受限，请联系管理员!";
            return_json_encode($result);
        }
        $user_data=db('topic_class')->where(['status'=>1])->field('id,title,status')->order('sort desc,id asc')->select();
        $result['topic_class']=$user_data;
        return_json_encode($result);

    }


    //邀约首页分类
    public function get_yy_class()
    {

        $result = array('code' => 1, 'msg' => '');
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token);
        if (!$user_info) {
            $result['code'] = 10001;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        //账号是否被禁用
        if ($user_info['user_status'] == 0) {
            $result['code'] = 0;
            $result['msg'] = "您因涉嫌违规，账号受限，请联系管理员!";
            return_json_encode($result);
        }
        $user_data=db('yy_class')->where(['status'=>1])->field('id,title,status')->order('sort desc,id asc')->select();
        $result['yy_class']=$user_data;
        return_json_encode($result);

    }

    //邀约首页
    public function get_yy_home()
    {

        $result = array('code' => 1, 'msg' => '');
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));
        $p = trim(input('param.p'));
        $listRows = trim(input('param.listRows'));

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token);
        if (!$user_info) {
            $result['code'] = 10001;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        //账号是否被禁用
        if ($user_info['user_status'] == 0) {
            $result['code'] = 0;
            $result['msg'] = "您因涉嫌违规，账号受限，请联系管理员!";
            return_json_encode($result);
        }

        //筛选条件
        $class=trim(input('param.class'));//1最新发布 2近期约会 3离我最近
        $sex=trim(input('param.sex'));//0全部 1男 2女
        $nation=trim(input('param.nation'));//国家
        $province=trim(input('param.province'));//省
        $area=trim(input('param.area'));//区/县
        $gift=trim(input('param.gift'));//1开启
        $video=trim(input('param.video'));//1开启

        if ($class==3){
            //满足条件的用户id组
            if (!empty($sex)){
                $user_map['sex']=$sex;
            }
            if ($video==1){
                $user_map['video_auth']=array('neq',0);
            }
            $user_map['user_status']=array('neq',0);
            //根据经纬度筛选
            $userData=db('user')->where(['id'=>$uid])->field('lat,lng')->find();
//            //半径默认10000m
//            $scope=calcScope($userData['lat'],$userData['lng'],10000);
//            $user_map['lat']=array(array('gt',$scope['minLat']),array('lt',$scope['maxLat']));
//            $user_map['lng']=array(array('gt',$scope['minLng']),array('lt',$scope['maxLng']));
            $user_data=db('user')->where($user_map)->field('id,lat,lng')->select();

//            var_dump($user_data);die();
            if (!empty($user_data)){
                foreach ($user_data as $value){
                    $ids[]=$value['id'];

//                    $jl=getDistance($userData['lng'],$userData['lat'],$value['lng'],$value['lat']);
//                    $value['juli']=$jl;
                }

                $yy_map['gift']=array('gt',0);
                $yy_map['uid']=array('in',$ids);

                $findData=db('invitation')->where($yy_map)->field('id,uid,theme,type,locale,content,me_pic,pic_link,mp3_link,yuji_time,see_time,create_time,tag,tags,game_name,beizhu,game_content,area,sport_name,destination,vocation,income,xueli,marriage_time,height,weight,dining_film,go_time')->select();


                if (!empty($findData)){
                    foreach ($findData as &$val){
                        $jingweidu=db('user')->where(['id'=>$val['uid']])->field('lat,lng')->find();
                        $val['lat']=$jingweidu['lat'];
                        $val['lng']=$jingweidu['lng'];
                        $val['juli']=getDistance($userData['lng'],$userData['lat'],$val['lng'],$val['lat']);
                    }
                    unset($val);
                    //冒泡算法按照距离重新排序

                    for($k=0;$k<=count($findData);$k++)
                    {
                        for($j=count($findData)-1;$j>$k;$j--){
                            if($findData[$j]['juli']<$findData[$j-1]['juli']){
                                $temp = $findData[$j];
                                $findData[$j] = $findData[$j-1];
                                $findData[$j-1] = $temp;
                            }
                        }
                    }

                    $invitation_data=$findData;
                }

            }
        }elseif($class==1){
            //满足条件的用户id组
            if (!empty($sex)){
                $user_map['sex']=$sex;
            }
            if (!empty($nation)){
                $user_map['nation']=$nation;
            }
            if (!empty($province)){
                $user_map['province']=$province;
            }
            if (!empty($area)){
                $user_map['area']=$area;
            }
            if ($video==1){
                $user_map['video_auth']=array('neq',0);
            }
            $user_id=db('user')->where($user_map)->field('id')->select();
            if (!empty($user_id)){
                foreach ($user_id as $vvl){
                    $user_ids[]=$vvl['id'];
                }
            }
//            var_dump($user_ids);die();
            $yy_map['uid']=array('in',empty($user_ids)?array():$user_ids);
            $yy_map['gift']=array('gt',0);

            $order="create_time desc";

            $invitation_data=db('invitation')->where($yy_map)->field('id,uid,theme,type,locale,content,me_pic,pic_link,mp3_link,yuji_time,see_time,create_time,tag,tags,game_name,beizhu,game_content,area,sport_name,destination,vocation,income,xueli,marriage_time,height,weight,dining_film,go_time')->order($order)->select();

        }else{
            //满足条件的用户id组
            if (!empty($sex)){
                $user_map['sex']=$sex;
            }
            if (!empty($nation)){
                $user_map['nation']=$nation;
            }
            if (!empty($province)){
                $user_map['province']=$province;
            }
            if (!empty($area)){
                $user_map['area']=$area;
            }
            if ($video==1){
                $user_map['video_auth']=array('neq',0);
            }
            $user_id=db('user')->where($user_map)->field('id')->select();
            if (!empty($user_id)){
                foreach ($user_id as $vvl){
                    $user_ids[]=$vvl['id'];
                }
            }
//            var_dump($user_ids);die();
            $yy_map['uid']=array('in',empty($user_ids)?array():$user_ids);
            $yy_map['gift']=array('gt',0);

            $order="";//近期约会排序待定

            $invitation_data=db('invitation')->where($yy_map)->field('id,uid,theme,type,locale,content,me_pic,pic_link,mp3_link,yuji_time,see_time,create_time,tag,tags,game_name,beizhu,game_content,area,sport_name,destination,vocation,income,xueli,marriage_time,height,weight,dining_film,go_time')->order($order)->select();
        }

        $topArr=[];

        //获取当前用户登录时的坐标
        $user_tab=db('user')->where(['id'=>$uid])->field("lat,lng")->find();
        $long1=$user_tab['lng'];
        $lat1=$user_tab['lat'];

        //type 1游戏，2旅游，3吃饭，4电影，5运动，6唱歌，7征婚，8其他
        if (!empty($invitation_data)){
            foreach ($invitation_data as &$val){
                $user_info=db('user')->where(['id'=>$val['uid']])->field('sex,birthday,user_nickname,avatar,lat,lng')->find();
                $long2=$user_info['lng'];
                $lat2=$user_info['lat'];
                $sex=$user_info['sex'];
                $age=date('Y',time())-date('Y',$user_info['birthday']);
                $juli=getDistance($long1,$lat1,$long2,$lat2);

                $topArr['user_nickname']=$user_info['user_nickname'];
                $topArr['avatar']=$user_info['avatar'];
                $topArr['sex']=$sex;
                $topArr['age']=$age;
                $topArr['juli']=$juli.'km';

                if ($val['type']==1){//游戏
                    $tags=db('game_tags')->where(['id'=>$val['tags']])->find();
                    $topArr['id']=$val['id'];
                    $topArr['top1']=$val['game_name'].'·'.$tags['title'];
                    $topArr['top2']=$val['beizhu'];
                    $topArr['top3']=$val['area'];
                    $topArr['pic']=$val['pic_link'];
                    $topArr['mp3']=$val['mp3_link'];
                    $topArr['type']=$val['type'];

                    $invitation_list[]=$topArr;
                }elseif ($val['type']==2){//旅游
                    //处理标签
                    $tag_data=explode(',',$val['tag']);
                    $tags=db('trip_tags')->where(['id'=>array('in',$tag_data),'status'=>1])->select();
                    $topArr['id']=$val['id'];
                    if (count($tag_data)>1){
                        $topArr['top1']=$val['destination'].'·'.$tags[0]['title'].'，'.$tags[1]['title'];
                    }else{
                        $topArr['top1']=$val['destination'].'·'.$tags[0]['title'];
                    }

                    $topArr['top2']=$val['content'];
                    $topArr['top3']=$val['go_time'];
                    $topArr['pic']=$val['pic_link'];
                    $topArr['mp3']=$val['mp3_link'];
                    $topArr['type']=$val['type'];

                    $invitation_list[]=$topArr;
                }elseif ($val['type']==3){//吃饭
                    $topArr['id']=$val['id'];
                    $topArr['top1']='约人吃饭'.'·'.$val['dining_film'];
                    $topArr['top2']=$val['content'];
                    $topArr['top3']=$val['see_time'];
                    $topArr['top4']=$val['locale'];
                    $topArr['pic']=$val['pic_link'];
                    $topArr['mp3']=$val['mp3_link'];
                    $topArr['type']=$val['type'];

                    $invitation_list[]=$topArr;
                }elseif ($val['type']==4){//电影
                    $topArr['id']=$val['id'];
                    $topArr['top1']='约人看电影'.'·'.$val['dining_film'];
                    $topArr['top2']=$val['content'];
                    $topArr['top3']=$val['see_time'];
                    $topArr['top4']=$val['locale'];
                    $topArr['pic']=$val['pic_link'];
                    $topArr['mp3']=$val['mp3_link'];
                    $topArr['type']=$val['type'];

                    $invitation_list[]=$topArr;
                }elseif ($val['type']==5){//运动
                    $tags=db('sport_tags')->where(['id'=>$val['tags']])->find();
                    $topArr['id']=$val['id'];
                    $topArr['top1']=$tags['title'];
                    $topArr['top2']=$val['content'];
                    $topArr['top3']=$val['see_time'];
                    $topArr['top4']=$val['locale'];
                    $topArr['pic']=$val['pic_link'];
                    $topArr['mp3']=$val['mp3_link'];
                    $topArr['type']=$val['type'];

                    $invitation_list[]=$topArr;
                }elseif ($val['type']==6){//唱歌
                    $topArr['id']=$val['id'];
                    $topArr['top1']='约人K歌';
                    $topArr['top2']=$val['content'];
                    $topArr['top3']=$val['see_time'];
                    $topArr['top4']=$val['locale'];
                    $topArr['pic']=$val['pic_link'];
                    $topArr['mp3']=$val['mp3_link'];
                    $topArr['type']=$val['type'];

                    $invitation_list[]=$topArr;
                }elseif ($val['type']==7){//征婚
                    $jiyu=db('marry_jiyu_tags')->where(['id'=>$val['tags']])->find();
                    $topArr['id']=$val['id'];
                    $topArr['top1']=$jiyu['title'];
                    $topArr['top2']=$val['marriage_time'];
                    $topArr['top3']=$val['vocation'].'·'.$val['xueli'];
                    $topArr['top4']=$val['height'].'cm'.'·'.$val['weight'].'kg';
                    $topArr['pic']=$val['me_pic'];
                    $topArr['type']=$val['type'];

                    $invitation_list[]=$topArr;
                }elseif ($val['type']==8){//其他
                    $topArr['id']=$val['id'];
                    $topArr['top1']=$val['theme'];
                    $topArr['top2']=$val['content'];
                    $topArr['top3']=$val['see_time'];
                    $topArr['top4']=$val['locale'];
                    $topArr['pic']=$val['pic_link'];
                    $topArr['mp3']=$val['mp3_link'];
                    $topArr['type']=$val['type'];

                    $invitation_list[]=$topArr;
                }
            }
        }
        $result['list']=empty($invitation_list)?array():$invitation_list;
        $result['page']=empty($invitation_list)?0:count($invitation_list);
        return_json_encode($result);

    }


    //邀约详情
    public function get_yy_info()
    {

        $result = array('code' => 1, 'msg' => '');
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));
        $invite_id = trim(input('param.invite_id'));

        if ($uid == 0 || empty($token)) {
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid, $token);
        if (!$user_info) {
            $result['code'] = 10001;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        //账号是否被禁用
        if ($user_info['user_status'] == 0) {
            $result['code'] = 0;
            $result['msg'] = "您因涉嫌违规，账号受限，请联系管理员!";
            return_json_encode($result);
        }
        $invite_info=db('invitation')->where(['id'=>$invite_id])->find();

        if (!empty($invite_info)){

            //获取登录用户信息
            $user_login=db('user')->where(['id'=>$uid])->field('lat,lng,video_auth')->find();
            //获取发布者信息
            $user_info=db('user')->where(['id'=>$invite_info['uid']])->field('sex,birthday,user_nickname,avatar,lat,lng')->find();

            //判断是否追加礼物
            if (!empty($invite_info['gift'])){
                $gift=db('gift')->where(['id'=>$invite_info['gift']])->field('name,coin,img')->find();
            }

            //type 1游戏，2旅游，3吃饭，4电影，5运动，6唱歌，7征婚，8其他
            if ($invite_info['type']==1){//游戏
                $tags=db('game_tags')->where(['id'=>$invite_info['tags']])->find();//游戏标签
                $result['list']=[
                    'type'=>$invite_info['type'],
                    'avatar'=>$user_info['avatar'],
                    'user_nickname'=>$user_info['user_nickname'],
                    'sex'=>$user_info['sex'],//1男 2女 0未知
                    'video_auth'=>empty($user_login['video_auth'])?0:1,//发布者是否已视频认证
                    'age'=>date('Y',time())-date('Y',$user_info['birthday']),
                    'juli'=>getDistance($user_login['lng'],$user_login['lat'],$user_info['lng'],$user_info['lat']),
                    'id'=>$invite_info['id'],
                    'subject'=>$invite_info['game_name'].'·'.$tags['title'],
                    'content'=>$invite_info['beizhu'],
                    'obj'=>$invite_info['sex'],//0不限1男2女
                    'gift'=>empty($gift)?array():$gift,
                    'pic'=>$invite_info['pic_link'],
                    'mp3'=>$invite_info['mp3_link'],
                    'area'=>$invite_info['area'],
                ];
            }elseif ($invite_info['type']==2){//旅游
                //处理标签
                $tag_data=explode(',',$invite_info['tag']);
                //旅行标签
                $tags=db('trip_tags')->where(['id'=>array('in',$tag_data),'status'=>1])->field('title')->select();
                if (count($tag_data)>1){
                    $topArr=$invite_info['destination'].'·'.$tags[0]['title'].'，'.$tags[1]['title'];
                }else{
                    $topArr=$invite_info['destination'].'·'.$tags[0]['title'];
                }

                //旅行希望与偏好标签
                if (!empty($invite_info['hope'])){
                    $hope_data=explode(',',$invite_info['hope']);
                    $hope_tags=db('hope_tags')->where(['id'=>array('in',$hope_data),'status'=>1])->field('title')->select();
                }

                $result_tags=empty($hope_tags)?$tags:array_merge($tags,$hope_tags);
                foreach ($result_tags as $val){
                    $resultTags[]=$val['title'];
                }
                $result['list']=[
                    'type'=>$invite_info['type'],
                    'avatar'=>$user_info['avatar'],
                    'user_nickname'=>$user_info['user_nickname'],
                    'sex'=>$user_info['sex'],//1男 2女 0未知
                    'video_auth'=>empty($user_login['video_auth'])?0:1,//发布者是否已视频认证
                    'age'=>date('Y',time())-date('Y',$user_info['birthday']),
                    'juli'=>getDistance($user_login['lng'],$user_login['lat'],$user_info['lng'],$user_info['lat']),

                    'subject'=>$topArr,
                    'content'=>$invite_info['content'],
                    'obj'=>$invite_info['sex'],//0不限1男2女
                    'go_time'=>$invite_info['go_time'],
                    'yuji_time'=>$invite_info['yuji_time'],
                    'go_way'=>$invite_info['go_way'],
                    'gift'=>empty($gift)?array():$gift,
                    'fee'=>$invite_info['fee'],
                    'tags'=>$resultTags
                ];
            }elseif ($invite_info['type']==3){//吃饭

                $result['list']=[
                    'type'=>$invite_info['type'],
                    'avatar'=>$user_info['avatar'],
                    'user_nickname'=>$user_info['user_nickname'],
                    'sex'=>$user_info['sex'],//1男 2女 0未知
                    'video_auth'=>empty($user_login['video_auth'])?0:1,//发布者是否已视频认证
                    'age'=>date('Y',time())-date('Y',$user_info['birthday']),
                    'juli'=>getDistance($user_login['lng'],$user_login['lat'],$user_info['lng'],$user_info['lat']),
                    'id'=>$invite_info['id'],

                    'subject'=>"约人吃饭".'·'.$invite_info['dining_film'],
                    'pay_way'=>$invite_info['pay_way'],
                    'content'=>$invite_info['content'],
                    'pic'=>$invite_info['pic_link'],
                    'mp3'=>$invite_info['mp3_link'],
                    'obj'=>$invite_info['sex'],//0不限1男2女
                    'jiesong'=>$invite_info['jiesong'],//是否接送0否1是
                    'gift'=>empty($gift)?array():$gift,
                    'see_time'=>$invite_info['see_time'],
                    'locale'=>$invite_info['locale']
                ];
            }elseif ($invite_info['type']==4){//电影

                $result['list']=[
                    'type'=>$invite_info['type'],
                    'avatar'=>$user_info['avatar'],
                    'user_nickname'=>$user_info['user_nickname'],
                    'sex'=>$user_info['sex'],//1男 2女 0未知
                    'video_auth'=>empty($user_login['video_auth'])?0:1,//发布者是否已视频认证
                    'age'=>date('Y',time())-date('Y',$user_info['birthday']),
                    'juli'=>getDistance($user_login['lng'],$user_login['lat'],$user_info['lng'],$user_info['lat']),
                    'id'=>$invite_info['id'],

                    'subject'=>"约人看电影".'·'.$invite_info['dining_film'],
                    'pay_way'=>$invite_info['pay_way'],
                    'content'=>$invite_info['content'],
                    'pic'=>$invite_info['pic_link'],
                    'mp3'=>$invite_info['mp3_link'],
                    'obj'=>$invite_info['sex'],//0不限1男2女
                    'jiesong'=>$invite_info['jiesong'],//是否接送0否1是
                    'gift'=>empty($gift)?array():$gift,
                    'see_time'=>$invite_info['see_time'],
                    'locale'=>$invite_info['locale']
                ];
            }elseif ($invite_info['type']==5){//运动
                $result['list']=[
                    'type'=>$invite_info['type'],
                    'avatar'=>$user_info['avatar'],
                    'user_nickname'=>$user_info['user_nickname'],
                    'sex'=>$user_info['sex'],//1男 2女 0未知
                    'video_auth'=>empty($user_login['video_auth'])?0:1,//发布者是否已视频认证
                    'age'=>date('Y',time())-date('Y',$user_info['birthday']),
                    'juli'=>getDistance($user_login['lng'],$user_login['lat'],$user_info['lng'],$user_info['lat']),
                    'id'=>$invite_info['id'],

                    'subject'=>$invite_info['sport_name'],
                    'pay_way'=>$invite_info['pay_way'],
                    'content'=>$invite_info['content'],
                    'pic'=>$invite_info['pic_link'],
                    'mp3'=>$invite_info['mp3_link'],
                    'obj'=>$invite_info['sex'],//0不限1男2女
                    'jiesong'=>$invite_info['jiesong'],//是否接送0否1是
                    'gift'=>empty($gift)?array():$gift,
                    'see_time'=>$invite_info['see_time'],
                    'locale'=>$invite_info['locale']
                ];
            }elseif ($invite_info['type']==6){//唱歌
                $result['list']=[
                    'type'=>$invite_info['type'],
                    'avatar'=>$user_info['avatar'],
                    'user_nickname'=>$user_info['user_nickname'],
                    'sex'=>$user_info['sex'],//1男 2女 0未知
                    'video_auth'=>empty($user_login['video_auth'])?0:1,//发布者是否已视频认证
                    'age'=>date('Y',time())-date('Y',$user_info['birthday']),
                    'juli'=>getDistance($user_login['lng'],$user_login['lat'],$user_info['lng'],$user_info['lat']),
                    'id'=>$invite_info['id'],

                    'subject'=>"约人K歌",
                    'pay_way'=>$invite_info['pay_way'],
                    'content'=>$invite_info['content'],
                    'pic'=>$invite_info['pic_link'],
                    'mp3'=>$invite_info['mp3_link'],
                    'obj'=>$invite_info['sex'],//0不限1男2女
                    'jiesong'=>$invite_info['jiesong'],//是否接送0否1是
                    'gift'=>empty($gift)?array():$gift,
                    'see_time'=>$invite_info['see_time'],
                    'locale'=>$invite_info['locale']
                ];
            }elseif ($invite_info['type']==7){//征婚
                $result['list']=[
                    'type'=>$invite_info['type'],
                    'avatar'=>$user_info['avatar'],
                    'user_nickname'=>$user_info['user_nickname'],
                    'sex'=>$user_info['sex'],//1男 2女 0未知
                    'video_auth'=>empty($user_login['video_auth'])?0:1,//发布者是否已视频认证
                    'age'=>date('Y',time())-date('Y',$user_info['birthday']),
                    'juli'=>getDistance($user_login['lng'],$user_login['lat'],$user_info['lng'],$user_info['lat']),
                    'id'=>$invite_info['id'],

                    'content'=>$invite_info['content'],//寄语
                    'marriage_time'=>$invite_info['marriage_time'],//何时结婚
                    'parents'=>$invite_info['parents'],//父母是否健在
                    'pic'=>$invite_info['pic_link'],
                    'mp3'=>$invite_info['mp3_link'],
                    'the_one'=>$invite_info['the_one'],//是否独生子女
                    'birth_location'=>$invite_info['birth_location'],//户籍所在地
                    'now_location'=>$invite_info['now_location'],
                    'height'=>$invite_info['height'],
                    'weight'=>$invite_info['weight'],
                    'vocation'=>$invite_info['vocation'],
                    'income'=>$invite_info['income'],
                    'xueli'=>$invite_info['xueli'],
                    'pass_love'=>$invite_info['pass_love'],
                    'is_marriage'=>$invite_info['is_marriage'],
                    'is_kid'=>$invite_info['is_kid'],
                    'drink'=>$invite_info['drink'],
                    'self_evaluation'=>$invite_info['self_evaluation'],
                    'shuxiang'=>$invite_info['shuxiang'],
                    'xingzuo'=>$invite_info['xingzuo'],
                    'xuexing'=>$invite_info['xuexing'],
                    'nation'=>$invite_info['nation'],
                    'belief'=>$invite_info['belief'],
                    'school'=>$invite_info['school'],
                    'driving_license'=>$invite_info['driving_license'],
                    'smoking'=>$invite_info['smoking'],
                    'disposition'=>$invite_info['disposition'],
                    'hereditary'=>$invite_info['hereditary'],
                    'sport'=>$invite_info['sport'],
                    'tourism'=>$invite_info['tourism'],
                    'aihao'=>$invite_info['aihao'],
                    'house'=>$invite_info['house'],
                    'loan'=>$invite_info['loan'],
                    'bazi'=>$invite_info['bazi'],
                    'car_no'=>$invite_info['car_no'],
                    'other_assets'=>$invite_info['other_assets'],
                    'your_birth_location'=>$invite_info['your_birth_location'],
                    'your_income'=>$invite_info['your_income'],
                    'your_educational'=>$invite_info['your_educational'],
                    'your_marriage'=>$invite_info['your_marriage'],
                    'your_house'=>$invite_info['your_house'],
                    'your_kid'=>$invite_info['your_kid'],
                    'your_shuxiang'=>$invite_info['your_shuxiang'],
                    'your_height'=>$invite_info['your_height'],
                    'your_weight'=>$invite_info['your_weight'],
                    'gift'=>empty($gift)?array():$gift,
                    'see_time'=>$invite_info['see_time'],
                    'locale'=>$invite_info['locale']
                ];
            }elseif ($invite_info['type']==8){//其他
                $result['list']=[
                    'type'=>$invite_info['type'],
                    'avatar'=>$user_info['avatar'],
                    'user_nickname'=>$user_info['user_nickname'],
                    'sex'=>$user_info['sex'],//1男 2女 0未知
                    'video_auth'=>empty($user_login['video_auth'])?0:1,//发布者是否已视频认证
                    'age'=>date('Y',time())-date('Y',$user_info['birthday']),
                    'juli'=>getDistance($user_login['lng'],$user_login['lat'],$user_info['lng'],$user_info['lat']),
                    'id'=>$invite_info['id'],

                    'subject'=>$invite_info['theme'],
                    'content'=>$invite_info['content'],
                    'pic'=>$invite_info['pic_link'],
                    'mp3'=>$invite_info['mp3_link'],
                    'obj'=>$invite_info['sex'],//0不限1男2女
                    'jiesong'=>$invite_info['jiesong'],//是否接送0否1是
                    'gift'=>empty($gift)?array():$gift,
                    'see_time'=>$invite_info['see_time'],
                    'locale'=>$invite_info['locale']
                ];
            }
        }else{
            $result['code']=10004;
            $result['msg']="数据不存在";
            $result['list']=array();
        }
        return_json_encode($result);

    }


}