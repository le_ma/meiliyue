<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件
use Qiniu\Auth;
use Qiniu\Storage\UploadManager;
use think\Config;
use think\Db;
use think\helper\Time;


//获取是否关注
function get_attention($uid, $to_user_id)
{
    $is_attention = db('user_attention')->where("uid=$uid")->where('attention_uid', '=', $to_user_id)->find();
    return $is_attention ? 1 : 0;
}

//获取是否拉黑
function get_is_black($uid, $to_user_id)
{
    $is_black = db('user_black')->where('user_id', '=', $uid)->where('black_user_id', '=', $to_user_id)->find();
    return $is_black ? 1 : 0;
}


//获取鉴权视频链接
function get_sign_video_url($key, $video_url)
{

    $parse_url_arr = parse_url($video_url);
    $url_dir = substr($parse_url_arr['path'], 0, strrpos($parse_url_arr['path'], '/') + 1);
    $t = dechex(time() + 60 * 60 * 24);
    $us = rand_str();
    $sign = md5($key . $url_dir . $t . $us);

    $sign_video_url = $video_url . '?t=' . $t . '&us=' . $us . '&sign=' . $sign;

    return $sign_video_url;
}

//根据条件获取主播列表
function emcee_complete($list)
{

    $config = load_cache('config');
    foreach ($list as &$v) {
        $v['evaluation'] = db('video_call_record_log')->where('anchor_id', '=', $v['id'])->where('is_fabulous', '=', 1)->count();             //好评百分比
        $level = get_level($v['id']);
        $v['level'] = $level;
        $v['is_online'] = is_online($v['id'], $config['heartbeat_interval']);               //获取主播是否登录
    }

    return $list;
}

//注册邀请业务处理
function reg_invite_service($uid, $invite_code)
{

    $invite_data['user_id'] = 0;
    $invite_data['invite_code'] = '';

    if (!empty($invite_code)) {

        $invite_code = db('invite_code')->where('invite_code', '=', $invite_code)->find();
        if ($invite_code && $invite_code['user_id'] != $uid) {
            $invite_data['user_id'] = $invite_code['user_id'];
            $invite_data['invite_code'] = $invite_code['invite_code'];
            $invite_data['invite_user_id'] = $uid;
            $invite_data['create_time'] = NOW_TIME;
        }
    }

    //添加邀请记录
    $res = db('invite_record')->insert($invite_data);

}

//完善资料查询邀请人进行奖励
function reg_invite_perfect_info_service($uid, $sex)
{

    $config = load_cache('config');
    $invite_record = db('invite_record')->where('invite_user_id', '=', $uid)->find();
    $reward = $sex == 1 ? $config['invite_reg_reward_man'] : $config['invite_reg_reward_female'];
    if ($invite_record) {

        $record = [
            'user_id' => $invite_record['user_id'],
            'invite_user_id' => $uid,
            'c_id' => 6,
            'income' => $reward,
            'invite_code' => $invite_record['invite_code'],
            'create_time' => NOW_TIME,
        ];

        db('invite_profit_record')->insert($record);
        db('user')->where('id', '=', $invite_record['user_id'])->inc('invitation_coin', $reward)->update();
    }

}

//随机获取一个空闲主播
function get_rand_emcee($uid, $max_count = 5)
{
    static $count = 0;
    if ($max_count == $count) {
        return 0;
    }
    $monitor = db('user')
        ->where('is_open_do_not_disturb', 'neq', 1)
        ->where('sex', '=', 2)
        ->where('is_online', '=', 1)
        ->where('id', '<>', $uid)
        ->limit(1)
        ->order('rand()')
        ->find();
    if (!$monitor) {
        return 0;
    }
    $is_call = db('video_call_record')->where('anchor_id', '=', $monitor['id'])->find();
    if ($is_call) {
        get_rand_emcee();
    } else {
        return $monitor['id'];
    }

    $count++;
}


/**
 * 记录错误日志
 * @param 日志内容 $res
 */
function save_log($res)
{

    $err_date = date("Ym", time());

    $err_info['msg'] = "错误：" . $res['msg'];
    $err_info['error'] = $res['error'];
    $err_info['post'] = $_POST;
    $err_info['get'] = $_GET;

    $request_url = 'http://' . $_SERVER['SERVER_NAME'] . ':' . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
    $err_info['url'] = $request_url;

    //$address = '/var/log/error';
    $address = 'public/errorlog';
    if (!is_dir($address)) {
        mkdir($address, 0700, true);
    }
    $address = $address . '/' . $err_date . '_error.log';

    $error_date = date("Y-m-d H:i:s", time());
    if (!empty($_SERVER['HTTP_REFERER'])) {
        $file = $_SERVER['HTTP_REFERER'];
    } else {
        $file = $_SERVER['REQUEST_URI'];
    }

    $res_real = "$error_date\t$file\t\n";
    error_log($res_real, 3, $address);
    $res = var_export($err_info, true);
    $res = $res . "\r\n";
    error_log($res, 3, $address);
}

//传参错误
function intvals($id)
{
    $result['code'] = 0;
    $result['msg'] = "传参错误：" . $id;
    $data = array('msg' => "传参错误", 'error' => $id);

    return_json_encode($result);

}

//封装json_encode()
function return_json_encode($result)
{
    json($result)->send();
    //echo json_encode($result);
    exit;
}

//获取配置信息
function get_config()
{

    $config_res = Db::name('config')->select();
    //var_dump($config_res);exit;
    $config = array_reduce($config_res, function (&$config, $v) {
        $config[$v['code']] = $v;
        return $config;
    });

    return $config;
}

//检测token
function check_token($user_id, $token, $field = [])
{

    $base_field = 'user_nickname,id,coin,sex,avatar,user_status,level,user_status';
    if (is_array($field) && count($field) > 0) {
        $base_field .= ',' . implode(',', $field);
    }

    $user_info = db('user')
        ->field($base_field)
        ->where(['id' => $user_id, 'token' => $token])
        ->find();

    return $user_info;
}


/**
 *      把秒数转换为时分秒的格式
 * @param Int $times 时间，单位 秒
 * @return String
 */
function secToTime($secs)
{
    $r = '';
    if ($secs >= 3600) {
        $hours = floor($secs / 3600);
        $secs = $secs % 3600;
        $r = $hours . ' 时';
        if ($hours <> 1) {
            $r .= 's';
        }
        if ($secs > 0) {
            $r .= ', ';
        }
    }
    if ($secs >= 60) {
        $minutes = floor($secs / 60);
        $secs = $secs % 60;
        $r .= $minutes . ' 分';
        if ($minutes <> 1) {
            $r .= '';
        }
        if ($secs > 0) {
            $r .= '';
        }
    }
    $r .= $secs;
    if ($secs <> 1) {
        $r .= ' 秒';
    }
    return $r;
}


//添加消费总记录
function add_charging_log($user_id, $to_user_id, $type, $coin, $table_id, $content = '')
{

    $data = [
        'user_id' => $user_id,
        'to_user_id' => $to_user_id,
        'coin' => $coin,
        'table_id' => $table_id,
        'type' => $type,
        'create_time' => time(),
        'profit' => $coin
    ];

    if ($type == 0) {
        $data['content'] = $content;
    } elseif ($type == 1) {
        $data['content'] = '视频购买消费';
    } elseif ($type == 2) {
        $data['content'] = '私照够买消费';
    } elseif ($type == 3) {
        $data['content'] = '赠送礼物消费';
    } elseif ($type == 4) {
        $data['content'] = '通话计时消费';
    } elseif ($type == 5) {
        $data['content'] = '私信消息付费';
    }

    return db('user_consume_log')->insert($data);
}

//获取用户基本信息
function get_user_base_info($user_id, $field = array(), $cache = 0)
{

//    $base_field = 'id,avatar,user_nickname,sex,level,coin,user_status';
//    if(is_array($field) && count($field) > 0){
//        $base_field .= ',' . implode(',',$field);
//    }
//    $user_info = db('user') -> field($base_field) -> find($user_id);

    $user_info = load_cache('user_info', ['user_id' => $user_id, 'field' => $field, 'cache' => $cache]);
    return $user_info;
}

/**
 * @dw 获取用户认证信息
 * @return 成功返回认证状态,未提交返回-1
 * */
function get_user_auth_status($user_id)
{
    //是否提交认证
    $auth_record = db('auth_form_record')->where('user_id', '=', $user_id)->find();

    return $auth_record ? $auth_record['status'] : -1;
}

/**
 * @dw 获取用户认证信息
 * @return 成功返回认证状态,未提交返回-1
 * */
function get_user_video_auth_status($user_id)
{
    //是否提交认证
    $auth_record = db('user_auth_video')->where('user_id', '=', $user_id)->find();

    return $auth_record ? $auth_record['status'] : -1;
}


//获取等级
function get_level($user_id)
{

    $user_info = get_user_base_info($user_id);
    //男性用户
    if ($user_info['sex'] == 1) {
        $time = Time::dayToNow(6);
        $where = "user_id=" . $user_id . " and create_time > " . $time[0];
        $where2 = "uid=" . $user_id . " and addtime > " . $time[0];
        //充值
        $charge_coin = db('user_charge_log')->where($where2)->sum("coin");
        //获取消费记录
        $charging_coin = db('user_consume_log')->where($where)->sum("coin");
        $total = $charge_coin + $charging_coin;

        $level = db('level')->where("level_up", '>', $total)->order("level_up asc")->limit(2)->select();
        if (count($level) > 0 && $level[0]['level_name'] != $user_info['level']) {
            db("user")->where("id=" . $user_id)->update(array('level' => $level[0]['level_name']));
            $user_level = $level[0]['level_name'];
        } else {
            $user_level = $user_info['level'];
        }
    } else {

        $time = Time::dayToNow(2);
        $where = "to_user_id=" . $user_id . " and create_time > " . $time[0];
        //获取收益记录
        $total = db('user_consume_log')->where($where)->sum("profit");
        $level = db('level')->where("level_up_female >" . $total)->order("level_up_female asc")->limit(2)->select();

        if (count($level) > 0 && $level[0]['level_name'] > $user_info['level']) {
            db("user")->where("id=" . $user_id)->update(array('level' => $level[0]['level_name']));
            $user_level = $level[0]['level_name'];
        } else {
            $user_level = $user_info['level'];
        }

    }
    return $user_level;
}

//获取用户等级
function get_grade_level($user_id)
{

    $user_info = get_user_base_info($user_id);
    //男性用户
    if ($user_info['sex'] == 1) {
        $time = Time::dayToNow(6);
        $where = "user_id=" . $user_id . " and create_time > " . $time[0];
        $where2 = "uid=" . $user_id . " and addtime > " . $time[0];
        //充值
        $charge_coin = db('user_charge_log')->where($where2)->sum("coin");
        //获取消费记录
        $charging_coin = db('user_consume_log')->where($where)->sum("coin");
        $total = $charge_coin + $charging_coin;
        $level = db('level')->where("level_up >" . $total)->order("level_up asc")->limit(2)->select();

        if (count($level) > 0 && $level[0]['level_name'] != $user_info['level']) {
            db("user")->where("id=" . $user_id)->update(array('level' => $level[0]['level_name']));
            $data['level_name'] = $level[0]['level_name'];
            $data['split'] = $level[0]['split'];
        } else {
            $data['level_name'] = $user_info['level'];

            $data['split'] = 0;
        }

        //获取充值金币和消费金币总数
        $data['msum'] = $total;                                                          // 获取提成比例

        if (count($level) > 1) {
            $data['down_name'] = $level[1]['level_name'];                                     // 获取下一个级别
            $data['progress'] = round(100 * ($total / $level[1]['level_up']));      // 进度 单位%
            $data['spread'] = $level[1]['level_up'] - $total;
        } else {
            $data['down_name'] = '99999';
            $data['progress'] = '0%';
            $data['spread'] = 0;
        }
    } else {

        $time = Time::dayToNow(2);
        $where = "to_user_id=" . $user_id . " and create_time > " . $time[0];
        //获取消费记录
        $total = db('user_consume_log')->where($where)->sum("profit");
        $level = db('level')->where("level_up_female >" . $total)->order("level_up_female asc")->limit(2)->select();

        if (count($level) > 0 && $level[0]['level_name'] != $user_info['level']) {
            db("user")->where("id=" . $user_id)->update(array('level' => $level[0]['level_name']));
            $data['level_name'] = $level[0]['level_name'];

            // 获取提成比例
            $data['split'] = $level[0]['split'];
        } else {
            $data['level_name'] = $user_info['level'];
            $data['split'] = 0;
        }
        //获取充值金币和消费金币总数
        $data['msum'] = $total;
        if (count($level) > 1) {
            // 获取下一个级别
            $data['down_name'] = $level[1]['level_name'];
            // 进度 单位%
            $data['progress'] = round(100 * ($total / $level[1]['level_up_female']));
            $data['spread'] = $level[1]['level_up_female'] - $total;
        } else {
            $data['down_name'] = '99999';
            $data['progress'] = '0%';
            $data['spread'] = 0;
        }

    }
    return $data;
}


//用户是否在线
function is_online($user_id, $heartbeat_interval)
{
//    $key = 'online:'.$user_id;
//    $time = $GLOBALS['redis'] -> get($key);
//
//    $type = 0;
//    if($time){
//        $invite_time = NOW_TIME - $time;
//        if($invite_time < $heartbeat_interval){
//            $type = 1;                      //在线
//        }
//    }
    include_once DOCUMENT_ROOT . '/meiliyue/system/redis/UserOnlineStateRedis.php';

    $user_redis = new UserOnlineStateRedis();
    $res = $user_redis->is_online($user_id);
    return $res ? 1 : 0;
}

//更新心跳时间
function update_heartbeat($user_id)
{

    $key = 'online:' . $user_id;

    $time = $GLOBALS['redis']->set($key, NOW_TIME);
    $data = [
        'user_id' => $user_id,
        'monitor_time' => NOW_TIME,
    ];

    $monitor = db('monitor')->where('user_id', '=', $user_id)->find();
    if ($monitor) {
        db('monitor')->where('user_id', '=', $user_id)->update(['monitor_time' => NOW_TIME]);
    } else {
        //增加记录
        db('monitor')->insert($data);
    }

}

//生成邀请码
function create_invite_code_0910($user_id)
{
    //获取邀请码
    $invite_code = db('invite_code')->where('user_id', '=', $user_id)->find();

    if (!$invite_code) {
        //生成邀请码;
        db('invite_code')->insert(['user_id' => $user_id, 'invite_code' => $user_id]);
        $invite_code = $user_id;
    }else{
        $invite_code = $invite_code['invite_code'];
    }

    return $invite_code;
}


//生成邀请码
function create_invite_code()
{

    $code = rand_str(6);
    $res = db('invite_code')->where('invite_code', '=', $code)->find();
    if ($res) {
        create_invite_code();
    } else {
        return $code;
    }
}

//邀请消费分成
//function invite_back_now($total_coin,$uid,$log_id,$invite_ratio){
//
//    //增加邀请分成
//    $invite_record = db('invite_record') -> where('invite_user_id','=',$uid) -> find();
//
//    if($invite_record){
//        //分成比例
//        $invite_income = $total_coin * $invite_ratio;
//
//        if($invite_income > 0){
//            $record = [
//                'user_id' => $invite_record['user_id'],
//                'invite_user_id' => $uid,
//                'c_id' => $log_id,
//                'income' => $invite_income,
//                'create_time' => NOW_TIME,
//            ];
//            db('invite_profit_record') -> insert($record);
//        }
//        //增加邀请人收益
//        db('user') -> where('id','=',$invite_record['user_id']) -> inc('income',$invite_income) -> inc('income_total',$invite_income) ->update();
//    }
//
//}

//邀请收益分成
function invite_back_now($total_coin, $uid, $log_id)
{
    //增加收益分成
    $invite_record = db('invite_record')->where('invite_user_id', '=', $uid)->find();

    if ($invite_record) {
        //分成比例
        //获取邀请人的性别
        $user = get_user_base_info($invite_record['invite_user_id'], 'sex');

        $config = load_cache('config');

        $invite_ratio = $user['sex'] == 1 ? $config['invite_income_ratio'] : $config['invite_income_ratio_female'];
        $invite_income = $total_coin * $invite_ratio;

        if ($invite_income > 0) {
            $record = [
                'user_id' => $invite_record['user_id'],
                'invite_user_id' => $uid,
                'c_id' => $log_id,
                'income' => $invite_income,
                'invite_code' => $invite_record['invite_code'],
                'create_time' => NOW_TIME,
            ];
            db('invite_profit_record')->insert($record);
        }

        //增加邀请人收益
        $money = number_format($invite_income / $config['invitation_exchange'], 2);
        db('user')->where('id', '=', $invite_record['user_id'])->inc('invitation_coin', $money)->update();
    }
}

//七牛删除图片
function oss_del_list($data)
{

    require_once DOCUMENT_ROOT . '/system/qiniu/autoload.php';
    // 需要填写你的 Access Key 和 Secret Key
    $accessKey = Config::get('qiniu.accessKey');
    $secretKey = Config::get('qiniu.secretKey');
    // 要上传的空间
    $bucket = Config::get('qiniu.bucket');
    // 构建鉴权对象
    $auth = new Auth($accessKey, $secretKey);
    $config = new \Qiniu\Config();
    $bucketManager = new \Qiniu\Storage\BucketManager($auth, $config);

    //每次最多不能超过1000个
    $ops = $bucketManager->buildBatchDelete($bucket, $data);
    list($ret, $err) = $bucketManager->batch($ops);

    if ($err !== null) {
        return false;
    } else {

        //返回图片的完整URL
        return $ret;
    }
}

//七牛批量删除
function oss_del_file($data)
{
    require_once DOCUMENT_ROOT . '/system/qiniu/autoload.php';
    // 需要填写你的 Access Key 和 Secret Key
    $accessKey = Config::get('qiniu.accessKey');
    $secretKey = Config::get('qiniu.secretKey');
    // 要上传的空间
    $bucket = Config::get('qiniu.bucket');
    // 构建鉴权对象
    $auth = new Auth($accessKey, $secretKey);
    $config = new \Qiniu\Config();
    $bucketManager = new \Qiniu\Storage\BucketManager($auth, $config);
    $result = $bucketManager->delete($bucket, $data);

    //var_dump($result);exit;
    if (!$result) {
        return true;
    } else {
        return false;
    }

}

//七牛上传 $file文件
function oss_upload($file)
{

    // 要上传图片的本地路径
    $filePath = $file->getRealPath();
    $ext = pathinfo($file->getInfo('name'), PATHINFO_EXTENSION);  //后缀
    // 上传到七牛后保存的文件名
    $key = substr(md5($file->getRealPath()), 0, 5) . date('YmdHis') . rand(0, 9999) . '.' . $ext;
    require_once DOCUMENT_ROOT . '/system/qiniu/autoload.php';
    // 需要填写你的 Access Key 和 Secret Key
    $accessKey = Config::get('qiniu.accessKey');
    $secretKey = Config::get('qiniu.secretKey');
    // 构建鉴权对象
    $auth = new Auth($accessKey, $secretKey);
    // 要上传的空间
    $bucket = Config::get('qiniu.bucket');
    $domain = Config::get('qiniu.DOMAIN');
    $token = $auth->uploadToken($bucket);

    // 初始化 UploadManager 对象并进行文件的上传
    $uploadMgr = new UploadManager();

    // 调用 UploadManager 的 putFile 方法进行文件的上传
    list($ret, $err) = $uploadMgr->putFile($token, $key, $filePath);
    if ($err !== null) {

        return false;
    } else {
        $url = $domain . '/' . $ret['key'];
        //返回图片的完整URL
        return $url;
    }
}

//定时清理离线用户
function crontab_do_end_live()
{

    $config = load_cache('config');

    //时间
    $time = NOW_TIME - $config['heartbeat_interval'] - 10;//偏移量5秒
    $time_out_user = db('monitor')->where('monitor_time', '<', $time)->select();

    $out_id_array = [];
    foreach ($time_out_user as $v) {

        $out_id_array[] = $v['user_id'];
        //删除心跳
        $key = 'online:' . $v['user_id'];
        $GLOBALS['redis']->del('del', $key);
    }

    if (count($out_id_array) > 0) {

        $ids = implode(',', $out_id_array);
        //删除所有超时心跳
        db('monitor')->where('user_id', 'in', $ids)->delete();
    }

    db('video_live_list')->where('last_heart_time', '<', $time)->delete();

}

//定时清理超时电话
function crontab_do_end_call()
{

    $config = load_cache('config');
    //查询
    $list = db('video_call_record')->where(['status' => 0])->select();
    foreach ($list as $v) {
        $time = NOW_TIME - $v['create_time'];
        if ($time > $config['video_call_time_out']) {
            //删除超时电话记录
            db('video_call_record')->delete($v['id']);
        }
    }

    //查询
    $list = db('video_call_record')->where(['status' => 1])->select();
    foreach ($list as $v) {
        $time = NOW_TIME - $v['create_time'];
        if ($time > 60 * 60 * 5) {
            //删除超时电话记录
            db('video_call_record')->delete($v['id']);
        }
    }

}

function log_result($word)
{
    if (is_array($word)) $word = var_export($word, true);
    $file = DOCUMENT_ROOT . "/public/notify_url.log";
    $fp = fopen($file, "a");
    flock($fp, LOCK_EX);
    fwrite($fp, "执行日期：" . strftime("%Y-%m-%d-%H：%M：%S", time()) . "\n" . $word . "\n\n");
    flock($fp, LOCK_UN);
    fclose($fp);
}


//支付通用回调发方法
function pay_call_service($notice_sn)
{

    //订单信息
    $order_info = db('user_charge_log')->where('order_id', '=', $notice_sn)->where('status', '=', 0)->find();

    if ($order_info) {
        $rule = db('user_charge_rule')->find($order_info['refillid']);
        if ($rule) {
            $coin = $rule['coin'] + $rule['give'];
            //增加用户钻石
            db('user')->where('id', '=', $order_info['uid'])->setInc('coin', $coin);
            db('user_charge_log')->where('order_id', '=', $notice_sn)->where('status', '=', 0)->setField('status', 1);
            //增加回调信息
            notify_log($notice_sn, $order_info['uid'], '充值回调成功,success:增加钻石成功');
        } else {
            //充值规则不存在
            db('user_charge_log')->where('order_id', '=', $notice_sn)->where('status', '=', 0)->setField('status', 2);
            notify_log($notice_sn, $order_info['uid'], '充值回调成功,error:充值规则不存在');
        }
    } else {
        //订单信息不存在

        notify_log($notice_sn, 0, '充值回调成功,error:订单信息不存在');
    }

}

function notify_log($order_id, $user_id, $content)
{

    db('pay_notify_log')->insert(['order_id' => $order_id, 'user_id' => $user_id, 'content' => $content, 'create_time' => NOW_TIME]);
}

function post($curlPost, $url)
{
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_NOBODY, true);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $curlPost);
    $return_str = curl_exec($curl);
    curl_close($curl);
    return $return_str;
}

//xml解析
function xml_to_array($xml)
{
    $reg = "/<(\w+)[^>]*>([\\x00-\\xFF]*)<\\/\\1>/";
    if (preg_match_all($reg, $xml, $matches)) {
        $count = count($matches[0]);
        for ($i = 0; $i < $count; $i++) {
            $subxml = $matches[2][$i];
            $key = $matches[1][$i];
            if (preg_match($reg, $subxml)) {
                $arr[$key] = xml_to_array($subxml);
            } else {
                $arr[$key] = $subxml;
            }
        }
    }
    return $arr;
}

//获取经纬度
function returnSquarePoint($lng, $lat, $distance = 0.5)
{
    define('EARTH_RADIUS', '6371');//地球半径，平均半径为6371km
    $dlng = 2 * asin(sin($distance / (2 * EARTH_RADIUS)) / cos(deg2rad($lat)));
    $dlng = rad2deg($dlng);

    $dlat = $distance / EARTH_RADIUS;
    $dlat = rad2deg($dlat);

    return array(
        'left-top' => array('lat' => $lat + $dlat, 'lng' => $lng - $dlng),
        'right-top' => array('lat' => $lat + $dlat, 'lng' => $lng + $dlng),
        'left-bottom' => array('lat' => $lat - $dlat, 'lng' => $lng - $dlng),
        'right-bottom' => array('lat' => $lat - $dlat, 'lng' => $lng + $dlng)
    );
}

//获取验证码数字
function get_verification_code($account, $length = 6)
{
    if (empty($account)) return false;
    $verificationCodeQuery = db('verification_code');
    $num = Db::name('config')->where("code='system_sms_sum'")->field('val')->find();
    $currentTime = time();
    $maxCount = $num['val'];
    $findVerificationCode = $verificationCodeQuery->where("'account'=" . $account)->find();
    $result = false;
    if (empty($findVerificationCode)) {
        $result = true;
    } else {
        $sendTime = $findVerificationCode['send_time'];
        $todayStartTime = strtotime(date('Y-m-d', $currentTime));
        if ($sendTime < $todayStartTime) {
            $result = true;
        } else if ($findVerificationCode['count'] < $maxCount) {
            $result = true;
        }
    }

    if ($result) {
        switch ($length) {
            case 4:
                $result = rand(1000, 9999);
                break;
            case 6:
                $result = rand(100000, 999999);
                break;
            case 8:
                $result = rand(10000000, 99999999);
                break;
            default:
                $result = rand(100000, 999999);
        }
    }

    return $result;
}

/**
 * 更新手机或邮箱验证码发送日志
 * @param string $account 手机或邮箱
 * @param string $code 验证码
 * @param int $expireTime 过期时间
 * @return boolean
 */
function verification_code_log($account, $code, $expireTime = 0)
{
    $currentTime = NOW_TIME;
    $expireTime = $expireTime > $currentTime ? $expireTime : $currentTime + 30 * 60;
    $verificationCodeQuery = db('verification_code');
    $findVerificationCode = $verificationCodeQuery->where('account', $account)->find();
    if ($findVerificationCode) {
        $todayStartTime = strtotime(date("Y-m-d"));//当天0点
        if ($findVerificationCode['send_time'] <= $todayStartTime) {
            $count = 1;
        } else {
            $count = ['exp', 'count+1'];
        }
        $result = $verificationCodeQuery
            ->where('account', $account)
            ->update([
                'send_time' => $currentTime,
                'expire_time' => $expireTime,
                'code' => $code,
                'count' => $count
            ]);
    } else {
        $result = $verificationCodeQuery
            ->insert([
                'account' => $account,
                'send_time' => $currentTime,
                'code' => $code,
                'count' => 1,
                'expire_time' => $expireTime
            ]);
    }

    return $result;
}




/**
 * 根据经纬度和半径计算出范围
 * @param string $lat 纬度
 * @param String $lng 经度
 * @param float $radius 半径/单位m
 * @return Array 范围数组
 */
function calcScope($lat, $lng, $radius)
{
    $degree = (24901 * 1609) / 360.0;
    $dpmLat = 1 / $degree;
    $PI = 3.1415926;

    $radiusLat = $dpmLat * $radius;
    $minLat = $lat - $radiusLat;       // 最小纬度
    $maxLat = $lat + $radiusLat;       // 最大纬度

    $mpdLng = $degree * cos($lat * ($PI / 180));
    $dpmLng = 1 / $mpdLng;
    $radiusLng = $dpmLng * $radius;
    $minLng = $lng - $radiusLng;      // 最小经度
    $maxLng = $lng + $radiusLng;      // 最大经度

    /** 返回范围数组 */
    $scope = array(
        'minLat' => $minLat,
        'maxLat' => $maxLat,
        'minLng' => $minLng,
        'maxLng' => $maxLng
    );
    return $scope;
}

/**
 * 计算两点地理坐标之间的距离
 * @param  Decimal $longitude1 起点经度
 * @param  Decimal $latitude1  起点纬度
 * @param  Decimal $longitude2 终点经度
 * @param  Decimal $latitude2  终点纬度
 * @param  Int     $unit       单位 1:米 2:公里
 * @param  Int     $decimal    精度 保留小数位数
 * @return Decimal
 */
function getDistance($longitude1, $latitude1, $longitude2, $latitude2, $unit=2, $decimal=2){

    $EARTH_RADIUS = 6370.996; // 地球半径系数
    $PI = 3.1415926;

    $radLat1 = $latitude1 * $PI / 180.0;
    $radLat2 = $latitude2 * $PI / 180.0;

    $radLng1 = $longitude1 * $PI / 180.0;
    $radLng2 = $longitude2 * $PI /180.0;

    $a = $radLat1 - $radLat2;
    $b = $radLng1 - $radLng2;

    $distance = 2 * asin(sqrt(pow(sin($a/2),2) + cos($radLat1) * cos($radLat2) * pow(sin($b/2),2)));
    $distance = $distance * $EARTH_RADIUS * 1000;

    if($unit==2){
        $distance = $distance / 1000;
    }

    return round($distance, $decimal);

}


