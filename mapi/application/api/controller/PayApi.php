<?php
/**
 * Created by PhpStorm.
 * User: weipeng
 * Date: 2018/3/19
 * Time: 11:23
 */

namespace app\api\controller;


class PayApi extends Base
{

    //充值金币
    public function pay(){

        $result = array('code' => 1,'msg' => '');
        $uid = intval(input('param.uid'));
        $token = trim(input('param.token'));
        $pid = intval(input('param.pid'));
        $rule_id = intval(input('param.rid'));//充值规则ID

        if($uid == 0 || empty($token)){
            $result['code'] = 0;
            $result['msg'] = '参数错误';
            return_json_encode($result);
        }

        $user_info = check_token($uid,$token);
        if(!$user_info){
            $result['code'] = 10001;
            $result['msg'] = '登录信息失效';
            return_json_encode($result);
        }

        //充值渠道
        $pay_type = db('pay_menu') -> find($pid);

        if(!$pay_type){
            $result['code'] = 0;
            $result['msg'] = '充值类型不存在';
            return_json_encode($result);
        }

        //充值规则
        $rule = db('user_charge_rule') -> find($rule_id);
        if(!$rule){
            $result['code'] = 0;
            $result['msg'] = '充值规则不存在';
            return_json_encode($result);
        }

        $notice_id = NOW_TIME . $uid;//订单号码
        $order_info = [
            'uid' => $uid,
            'money' => $rule['money'],
            'coin' => $rule['coin'],
            'refillid' => $rule_id,
            'addtime' => NOW_TIME,
            'status' => 0,
            'type' => $pid,
            'order_id' => $notice_id,
            'pay_type_id' => $pid,
        ];

        //增加订单记录
        db('user_charge_log') -> insert($order_info);

        $class_name = $pay_type['class_name'];
        //echo $class_name;exit;
        //echo DOCUMENT_ROOT."/system/pay_class/".$class_name."_menu.php";exit;
        bugu_request_file(DOCUMENT_ROOT."/system/pay_class/".$class_name."_menu.php");
        $o = new $class_name;
        $pay= $o->get_payment_code($pay_type,$rule,$notice_id);

        $result['pay'] = $pay;

        return_json_encode($result);
    }

    public function pay_display_html(){

        $order_id = $_REQUEST['order_id'];
        $class_name = $_REQUEST['class_name'];

        bugu_request_file(DOCUMENT_ROOT."/system/pay_class/".$class_name."_menu.php");
        $o = new $class_name;
        $o->display_html($order_id);
    }
}