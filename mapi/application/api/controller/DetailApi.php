<?php
/**
 * Created by PhpStorm.
 * User: weipeng
 * Date: 2018/3/4
 * Time: 20:48
 */

namespace app\api\controller;


class DetailApi extends Base
{
    //我的明细
    public function defaults()
    {
        $uid = input("param.uid");

        if (empty($uid)) {
            echo '用户uid传参错误';
            exit;
        }

        //总收益 积分：接受的是积分 ；师傅是：男的奖励聊币女的奖励积分
        $where = 'to_user_id=' . $uid;
        $profit = db("user_consume_log")->where($where)->sum("profit");

        //总消费 聊币：送礼物打电话的是聊币
        $where2 = 'user_id=' . $uid;
        $coin = db("user_consume_log")->where($where2)->sum("coin");

        $user = Db("user")->field("coin,user_nickname")->where("id=" . $uid)->find();

        $data = array(
            'user_nickname' => $user['user_nickname'],
            'coin' => $user['coin'],
            'income' => $profit,
            'spending' => $coin,
            'uid' => $uid
        );
        $this->assign('list', $data);

        return $this->fetch();
    }

    //我的明细
    public function index()
    {
        $uid = input("param.uid");
        $type = intval(input('param.type'));

        if (empty($uid)) {
            echo '用户uid传参错误';
            exit;
        }

        $record_list = $this->getListData(0, $type, $uid);
        $this->assign('p', 0);
        $this->assign('uid', $uid);
        $this->assign('data', $record_list);
        $this->assign('type', $type);
        return $this->fetch();
    }

    //分页
    public function pages()
    {
        $page = input("param.page");

        $p = ($page + 1) * 20;
        $uid = input("param.uid");
        $record_list = $this->getListData($p, session("detail"), $uid);

        echo json_encode($record_list);
        exit;

    }

    public function getListData($page, $type, $uid)
    {

        session("detail", $type);

        //判断所有还是聊币还是积分
        if ($type == 1) {          //聊币：送礼物打电话的是聊币
            $where = 'user_id=' . $uid;
        } else {     //积分：接受的是积分 ；师傅是：男的奖励聊币女的奖励积分
            $where = 'to_user_id=' . $uid;
        }

        //查询本用户的所有记录
        $record_list = db("user_consume_log")->where($where)
            ->field("sum(coin) as coin,sum(profit) as profit,content,create_time,table_id,type,user_id,to_user_id")
            ->order("create_time desc")
            ->limit($page, 20)
            ->group("table_id,type")
            ->select();

        foreach ($record_list as &$v) {

            if ($v['type'] == 4) {      //视频通话
                $name = "视频通话";

            } elseif ($v['type'] == 3) {      //礼物
                $name = '礼物赠送';

            } elseif ($v['type'] == 2) {              //私照
                $name = "私照观看";

            } elseif ($v['type'] == 1) {              //视频
                $name = "小视频观看";

            } else {                             //系统赠送
                $name = $v['content'];

            }
            $v['name'] = $name;
            $v['create_time'] = date('Y-m-d H:i:s', $v['create_time']);

            $where_name['id'] = $type == 1 ? $v['to_user_id'] : $v['user_id'];

            $user = Db("user")->field("user_nickname")->where($where_name)->find();
            $v['user_nickname'] = $user['user_nickname'];

        }

        return $record_list;
    }

    //扣费和收益明细
    public function particulars()
    {

        $uid = input("param.uid");
        $typeid = input("param.typeid");
        $type = input("param.type");
        $table = input("param.table");
        //查询本用户的所有记录
        //判断所有还是聊币还是积分

        if ($type == 1) {          //聊币：送礼物打电话的是聊币
            $where = 'user_id=' . $uid;
        } else {     //积分：接受的是积分 ；师傅是：男的奖励聊币女的奖励积分
            $where = 'to_user_id=' . $uid;
        }
        $where .= " and type=" . $table . " and table_id=" . $typeid;
        $record_list = db("user_consume_log")->where($where)
            ->field("*")
            ->order("create_time desc")
            ->select();

        foreach ($record_list as &$v) {
            $v['create_time'] = date('Y-m-d H:i:s', $v['create_time']);

            $where_name['id'] = $type == 1 ? $v['to_user_id'] : $v['user_id'];

            $user = Db("user")->field("user_nickname")->where($where_name)->find();
            $v['user_nickname'] = $user['user_nickname'];
        }


        $this->assign('uid', $uid);
        $this->assign('data', $record_list);
        return $this->fetch();
    }

}