<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:48:"themes/admin_simpleboot3/admin/audit/photos.html";i:1519953092;s:43:"themes/admin_simpleboot3/public/header.html";i:1519953092;}*/ ?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="__TMPL__/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="__TMPL__/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="__STATIC__/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "__ROOT__/",
            WEB_ROOT: "__WEB_ROOT__/",
            JS_ROOT: "static/js/",
            APP: '<?php echo \think\Request::instance()->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="__TMPL__/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="__STATIC__/js/layer/layer.js" rel="stylesheet"></script>
    <script src="__STATIC__/js/wind.js"></script>
    <script src="__TMPL__/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip();
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
<style>
    .gift-img{width:50px;height:50px}
    .gift-img img{width:100%;height:100%;}
    .gift-in input{width:25px;}
    .js-ajax-form{margin-top:30px;}
    .identity img{width:30px;height:30px;border-radius: 50%;}
    .audit img{width:40px;height:40px;}
    .details{cursor: pointer;}
    .layui-layer-demo .layui-layer-title{
        background: #e0e0e0!important;
    }
    #status{    width: 100px;
        height: 32px;
        border-color: #dce4ec;
        color: #aeb5bb;}

</style>
</head>
<body>
<div class="wrap js-check-wrap">
    <ul class="nav nav-tabs">
        <li class="active"><a href="javascript:;">视频认证列表</a></li>
    </ul>
    <form class="well form-inline margin-top-20" method="post" action="<?php echo url('audit/photos'); ?>">
        用户ID：
        <input class="form-control" type="text" name="uid" style="width: 200px;" value="<?php echo input('request.uid'); ?>"
               placeholder="请输入用户ID">
        状态：
        <select name="status" id="status">
            <option value="">未审核</option>
            <option value="1" <?php if($request['status'] == 1): ?> selected="selected" <?php endif; ?>>已审核</option>
            <option value="2" <?php if($request['status'] == 2): ?> selected="selected" <?php endif; ?>>已拒绝</option>
        </select>
        <input type="submit" class="btn btn-primary" value="搜索"/>
        <a class="btn btn-danger" href="<?php echo url('audit/photos'); ?>">清空</a>
    </form>
    <form class="js-ajax-form" action="<?php echo url('audit/photos_upd'); ?>" method="post">

        <table class="table table-hover table-bordered table-list">
            <thead>
            <tr>
                <th>ID</th>
                <th>用户头像</th>
                <th>用户名称</th>
                <th>私照图片</th>
                <th>申请时间</th>
                <th>状态</th>
                <th>操作</th>
            </tr>
            </thead>
            <tfoot>
            <?php if(is_array($user) || $user instanceof \think\Collection || $user instanceof \think\Paginator): if( count($user)==0 ) : echo "" ;else: foreach($user as $key=>$vo): ?>
                <tr>
                    <td><?php echo $vo['uid']; ?></td>
                    <td class="identity"><img src="<?php echo $vo['avatar']; ?>" alt=""></td>
                    <td><?php echo $vo['user_nickname']; ?></td>
                    <td class="audit"><img src="<?php echo $vo['img']; ?>" alt=""></td>
                    <td><?php echo date("Y-m-d H:i:s",$vo['addtime'] ); ?></td>
                    <td>
                        <?php if($vo['status'] == '0'): ?>
                            正在审核中
                            <?php elseif($vo['status'] == '2'): ?>
                            已拒绝
                            <?php else: ?>
                            已审核
                        <?php endif; ?>
                    </td>
                    <td>
                        <a class="details" data-url="<?php echo $vo['img']; ?>">查看详情</a>
                        <?php if($vo['status'] == '0'): ?>
                            | <a href="<?php echo url('audit/photos_upd',array('uid'=>$vo['uid'],'id'=>$vo['id'],'type'=>1)); ?>">通过</a>
                            | <a href="<?php echo url('audit/photos_upd',array('uid'=>$vo['uid'],'id'=>$vo['id'],'type'=>2)); ?>">拒绝</a>
                        <?php endif; ?>

                    </td>
                </tr>
            <?php endforeach; endif; else: echo "" ;endif; ?>
            </tfoot>
        </table>
        <ul class="pagination"><?php echo $page; ?></ul>

    </form>

</div>
<script src="__STATIC__/js/layer/layer.js" rel="stylesheet"></script>
<script>
    $(".details").click(function(){
        var url=$(this).attr("data-url");
        //图片显示
        layer.open({
            type: 1,
            title: false,
            closeBtn: 0,
            area: ['300px', '300px'],
            skin: 'layui-layer-nobg', //没有背景色
            shadeClose: true,
            content: "<img src='"+url+"' style='width:100%;height:100%;'/>"
        });

    })

</script>
</body>
</html>