<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:52:"themes/admin_simpleboot3/admin/agent/withdrawal.html";i:1532312036;s:43:"themes/admin_simpleboot3/public/header.html";i:1519953092;}*/ ?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="__TMPL__/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="__TMPL__/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="__STATIC__/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "__ROOT__/",
            WEB_ROOT: "__WEB_ROOT__/",
            JS_ROOT: "static/js/",
            APP: '<?php echo \think\Request::instance()->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="__TMPL__/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="__STATIC__/js/layer/layer.js" rel="stylesheet"></script>
    <script src="__STATIC__/js/wind.js"></script>
    <script src="__TMPL__/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip();
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
</head>
<body>
<div class="wrap js-check-wrap">
    <ul class="nav nav-tabs">
        <li class="active"><a href="<?php echo url('agent/index'); ?>">代理</a></li>
        <li><a href="<?php echo url('agent/add'); ?>">添加代理</a></li>
    </ul>
    <form class="well form-inline margin-top-20" method="post" action="<?php echo url('agent/withdrawal'); ?>">

        代理id:
        <input type="text" class="form-control" name="agent_id" style="width: 120px;" value="<?php echo (isset($data['agent_id']) && ($data['agent_id'] !== '')?$data['agent_id']:''); ?>" placeholder="请输入代理id">

        代理级别：
        <select class="form-control" name="agent_level" style="width: 140px;">
            <option value="1" <?php if($data['agent_level'] == 1): ?> selected='selected' <?php endif; ?>>一级</option>
            <option value="2" <?php if($data['agent_level'] == 2): ?> selected='selected' <?php endif; ?> >二级</option>
            <option value="3" <?php if($data['agent_level'] == 3): ?> selected='selected' <?php endif; ?>>三级</option>
        </select>
        提现状态：
        <select class="form-control" name="status" style="width: 140px;">
            <option value="-1">全部</option>
            <option value="0" <?php if($data['status'] == '0'): ?> selected='selected' <?php endif; ?>>审核中</option>
            <option value="1" <?php if($data['status'] == 1): ?> selected='selected' <?php endif; ?>>提现成功</option>
            <option value="2" <?php if($data['status'] == 2): ?> selected='selected' <?php endif; ?>>拒绝提现</option>
        </select>
        &nbsp;&nbsp;
        时间:
        <input type="text" class="form-control js-bootstrap-datetime" name="start_time" value="<?php echo (isset($data['start_time']) && ($data['start_time'] !== '')?$data['start_time']:''); ?>" style="width: 140px;" autocomplete="off">-
        <input type="text" class="form-control js-bootstrap-datetime" name="end_time" value="<?php echo (isset($data['end_time']) && ($data['end_time'] !== '')?$data['end_time']:''); ?>" style="width: 140px;" autocomplete="off"> &nbsp; &nbsp;

        <input type="submit" class="btn btn-primary" value="搜索" />
        <a class="btn btn-danger" href="<?php echo url('agent/withdrawal'); ?>">清空</a>

        <div style="margin-top:20px;">提现的总金额 :  <?php echo $sum; ?> 元</div>
    </form>
    <table class="table table-hover table-bordered">
        <thead>
        <tr>
            <th width="50">#</th>
            <th><?php echo lang('USERNAME'); ?>(ID)</th>

            <th>提现金额</th>
            <th>申请时间</th>
            <th><?php echo lang('STATUS'); ?></th>

            <th width="130"><?php echo lang('ACTIONS'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php $user_statuses=array("0"=>'审核中',"1"=>'已提现','2'=>'拒绝提现'); if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): if( count($list)==0 ) : echo "" ;else: foreach($list as $key=>$vo): ?>
            <tr>
                <td><?php echo $key +1; ?></td>
                <td><?php echo $vo['agent_login']; ?>(<?php echo $vo['agent_id']; ?>)</td>

                <td><?php echo (isset($vo['money'] ) && ($vo['money']  !== '')?$vo['money'] :'0'); ?> 元</td>
                <td><?php echo date('Y-m-d H:i',$vo['addtime'] ); ?></td>
                <td><?php echo $user_statuses[$vo['status']]; ?></td>

                <td>
                     <?php if(($vo['status'] == 0) and ($vo['agent_level'] == 1)): ?>
                        <a href="<?php echo url('agent/addwithdrawal',array('id'=>$vo['id'],'status'=>1)); ?>" class="js-ajax-dialog-btn" data-msg="<?php echo lang('确定支付提现金额'); ?>">提现</a>
                        <a href="<?php echo url('agent/addwithdrawal',array('id'=>$vo['id'],'status'=>2)); ?>" class="js-ajax-dialog-btn" data-msg="<?php echo lang('确定拒绝提现金额'); ?>">拒绝提现</a>
                    <?php endif; ?>

                </td>
            </tr>
        <?php endforeach; endif; else: echo "" ;endif; ?>
        </tbody>
    </table>
    <div class="pagination"><?php echo $page; ?></div>
</div>
<script src="__STATIC__/js/admin.js"></script>
</body>
</html>