<?php
/**
 * Created by PhpStorm.
 * User: weipeng  kj
 * Date: 2018/3/4
 * Time: 22:14
 */

function load_cache($key,$param = array(),$is_real = true){

    $file =  DOCUMENT_ROOT."/meiliyue/system/cache/".$key.".auto_cache.php";
//    $file =  DOCUMENT_ROOT."/system/cache/".$key.".auto_cache.php";
    require_once $file;
    $class = $key."_auto_cache";

    $obj = new $class;
    $result = $obj->load($param,$is_real);
    return $result;
}


//PHP把秒转换成小时数和分钟 ：时间转换
function secs_to_str($secs) {
    $r='';
    if($secs>=3600){
        $hours=floor($secs/3600);
        $secs=$secs%3600;
        $r=$hours.' 时';
        if($hours<>1){$r.='s';}
        if($secs>0){$r.=', ';}
    }
    if($secs>=60){
        $minutes=floor($secs/60);
        $secs=$secs%60;
        $r.=$minutes.' 分';
        if($minutes<>1){$r.='';}
        if($secs>0){$r.='';}
    }
    $r.=$secs;
    if($secs<>1)
    {$r.='秒';
    }
    return $r;
}

function get_oss_file_path($path){
    $file_name = parse_url($path)['path'];
    $file_name = substr($file_name,1,strlen($file_name));
    return $file_name;
}

function bugu_request_file($path){

    require_once $path;
}


//生成随机字符串
function rand_str($len = 8){
    $chars    = [
        "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
        "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
        "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G",
        "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R",
        "S", "T", "U", "V", "W", "X", "Y", "Z", "0", "1", "2",
        "3", "4", "5", "6", "7", "8", "9"
    ];
    $charsLen = count($chars) - 1;
    shuffle($chars);    // 将数组打乱
    $output = "";
    for ($i = 0; $i < $len; $i++) {
        $output .= $chars[mt_rand(0, $charsLen)];
    }
    return $output;
}

